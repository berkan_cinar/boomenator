<?php
namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\Container;

/**
 * REST client for Eloqua's API.
 */
class EloquaRequest
{
    private $ch;
    public $baseUrl;
    public $responseInfo;

	public function __construct($doctrine, $credString, $baseUrl=null)
	{
		if($credString == ''){
			$credResults = $doctrine->getRepository('AppBundle:Credentials')
	            ->findAll();

	        $selected = reset($credResults);
	        $credString = $selected->getCredString();
	    }

		$creds = explode('|||',$credString);
		// basic authentication credentials
		$credentials = $creds[0] . '\\' . $creds[1] . ':' . $creds[2];

		// set the base URL for the API endpoint
		$this->baseUrl = ($baseUrl===null)?'https://secure.p01.eloqua.com/API/REST/2.0':$baseUrl;		

		// initialize the cURL resource
		$this->ch = curl_init();

		// set cURL and credential options
		curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl);
		curl_setopt($this->ch, CURLOPT_USERPWD, $credentials); 

		// return transfer as string
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, FALSE);
	    curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	}

	public function __destruct()
	{
		curl_close($this->ch);
	}

	public function get($url)
	{
		return $this->executeRequest($url, 'GET');
	}

	public function post($url, $data, $image=null)
	{
		return $this->executeRequest($url, 'POST', $data, $image);
	}

	public function put($url, $data)
	{
		return $this->executeRequest($url, 'PUT', $data);
	}

	public function delete($url)
	{
		return $this->executeRequest($url, 'DELETE');	
	}
	
	public function executeRequest($url, $method, $data=null, $image=null)
	{
		set_time_limit(0);
		// set the full URL for the request
		curl_setopt($this->ch, CURLOPT_URL, $this->baseUrl . '/' . $url);

		// escape data and set headers
		$headers = array('Content-type: application/json');		
		if($image){
			$headers = array("Content-Length: ".strlen($data),"Content-Type: multipart/form-data; boundary=ELOQUA_BOUNDARY");
		}
		if($data !== null && $image !== true){
			if(is_object($data)||is_array($data)){$data = json_encode($data);}
			$data = preg_replace('/,\s*"[^"]+":null|"[^"]+":null,?/', '', $data);
		}
		
		curl_setopt($this->ch, CURLOPT_HTTPHEADER, $headers);

		switch ($method) {
			case 'GET':
				curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
				break;
			case 'POST':
				curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
				break;
			case 'PUT':
				curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);
				break;
			case 'DELETE':
				curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
				break;
			default:
				break;
		}

        // execute the request
        $response = curl_exec($this->ch);

        // store the response info including the HTTP status
        // 400 and 500 status codes indicate an error
        $responseInfo = $this->responseInfo = curl_getinfo($this->ch);
        $httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        
        if ($httpCode >= 400) 
        {            
            return(array($responseInfo, $response));            
        }
        	
        return json_decode($response);
	}
}

?>
