<?php
namespace AppBundle\Twig;

class UcFirst extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('ucfirst', array($this, 'UcFirstFilter')),
        );
    }

    public function UcFirstFilter($string)
    {
        $res = ucfirst($string);

        return $res;
    }

    public function getName()
    {
        return 'ucfirst';
    }
}