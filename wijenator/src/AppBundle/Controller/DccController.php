<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Oneup\UploaderBundle\OneupUploaderBundle;

use AppBundle\Service\EloquaRequest;
use AppBundle\Helper\Email;
use AppBundle\Helper\Image;
use AppBundle\Helper\Segment;

use ZipArchive;

class DccController extends Controller
{
    private $parsedFolders;
    private $elqCredentials;

    /**
     * @Route("/uploaddcc", name="uploadDCC")
     */
    public function dccUploadAction(Request $request)
    {
        $fs = new Filesystem();
        $dir = $this->get('kernel')->getRootDir().'\..\web\uploads\dcc';
        if(!$fs->exists($dir)){$fs->mkdir($dir);}
        $finder = new Finder();
        $finder->files()->in($dir)->name('*.*');
        foreach($finder as $file){
            $fs->remove($file);
        }
        $fs->remove($dir."\\images");

        return $this->render('tools/dccupload.html.twig', [
            'title' => 'Upload DCC zip file'
        ]);
    }

    /**
     * @Route("/parsedcc", name="parseDCC")
     */
    public function dccParseAction(Request $request)
    {
        $fs = new Filesystem();
        $dir = $this->get('kernel')->getRootDir().'\..\web\uploads\dcc';
        if(!$fs->exists($dir)){
            return $this->redirectToRoute('uploadDCC');
        }

        $finder = new Finder();
        $finder->files()->in($dir)->name('*.zip');
        if($finder->count() < 1){
            return $this->redirectToRoute('uploadDCC');
        }

        $iterator = $finder->getIterator();
        $iterator->rewind();
        $zipFile = $iterator->current();

        $zip = new ZipArchive();
        $zip->open($zipFile);

        $dataSet;
        $emails = array();
        $images = array();
        
        for($i=0; $i<$zip->numFiles;$i++){ 
            $file = $zip->getNameIndex($i); 
            $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION)); 
            $fol = explode('/',$file)[0];
            if($ext == "json"){
                $dataSet = json_decode($zip->getFromName($file));
            }elseif($ext == "html"){ 
                $newName = preg_replace('/[\[\$\&\+\,\:\;\=\?\@\#\|\'\<\>\^\*\(\)\%\!\-\]]/', '', $file);
                $zip->renameName($file, $newName); 
                if($newName != "crm_briefing.html"){
                    array_push($emails, $newName);
                }
            }elseif($fol == "images"){
                array_push($images, $file);
            }
        }
        $zip->close();
       
        $zip2 = new ZipArchive();
        $zip2->open($zipFile);
        $zip2->extractTo($dir);

        $prepData = self::prepareDataAction($dataSet);

        $campaignName = $dataSet->campaign->campaign_name;
        $campaignType = $dataSet->campaign->campaign_type;
        $country = $dataSet->campaign->country;

        $segmentForm = null;
        $namingTemplate = null;
        $bg = null;
        $cat = null;

         if($campaignType == "B2C"){
            //Get correct credentials
            $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => 'PhilipsConsumerLifestyle'));
            if($cred === null){return new Response('You don\'t have credentials for the B2C instance PhilipsConsumerLifestyle, and the uploaded DCC request is for B2C.');}
            $this->elqCredentials = $cred->getCredString();

            //Naming convention template
            $namingDate = str_replace('-','',substr($dataSet->briefing[2]->fields->preferred_broadcast_date,0,10));

            $sectorResult = $this->getDoctrine()->getRepository('AppBundle:ValueMaps')->findOneBy(array('type' => 'sector', 'name' => $dataSet->briefing[1]->fields->tracking_sector));
            $sector = $sectorResult->getValue();

            $bgResult = $this->getDoctrine()->getRepository('AppBundle:ValueMaps')->findOneBy(array('type' => 'bg', 'name' => $dataSet->briefing[1]->fields->tracking_business_group));
            $bg = $bgResult->getValue();

            $cat = $dataSet->briefing[1]->fields->tracking_category;

            $subtype = $dataSet->briefing[1]->fields->tracking_campaign_sub_type;

            $country = ($country == "MyShop")?"NL":$country;
            $namingTemplate = $sector.' '.$country.' '.$namingDate.' '.$bg.$cat.' EM <span style="color:red;">CAMPAIGNTYPE</span> '.$subtype.' <span style="color:red;">NAME #TICKET</span>';

            $segmentFolders = null; //self::fetchFoldersAction('segment', $this->elqCredentials, $request);

            $segmentForm = null; //self::dccSegmentAction($segmentFolders, $campaignType, $bg, $cat, $request)->createView();
        }elseif($campaignType == "B2B"){
            //Get correct credentials
            $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => 'PhilipsHealthcareProd'));
            if($cred === null){return new Response('You don\'t have credentials for the B2B instance PhilipsHealthcareProd, and the uploaded DCC request is for B2B.');}
            $this->elqCredentials = $cred->getCredString();            
        } 
        
        $emailFolders = self::fetchFoldersAction('email', $this->elqCredentials, $request);

        $emailForm = self::dccEmailAction($this->elqCredentials, $emailFolders, $emails, $campaignName, $campaignType, $country, strtolower($bg.$cat), $request)->createView();

        $zip2->close();

        return $this->render('tools/parsedcc.html.twig', array(
            'namingTemplate' => $namingTemplate,
            'segmentForm' => $segmentForm,
            'emailForm' => $emailForm,
            'prepData' => $prepData,
            'title' => 'DCC uploader'
        ));
    }

    public function dccSegmentAction($folders = null, $campaignType = null, $bg = null, $cat = null, Request $request)
    {
        $segmentCreation = [];

        $segmentForm = $this->get('form.factory')->createNamedBuilder('dccSegmentForm', 'Symfony\Component\Form\Extension\Core\Type\FormType', $segmentCreation, array('attr' => array('class' => 'dccSegmentForm')))
            ->add('folder', ChoiceType::class, array('choices' => $folders, 'label' => 'Select the folder to create the segment in.', 'attr' => array('class' => 'segmentFolders', 'size' => '15')))
            ->add('name', TextType::class, array('label' => 'Segment Name'))
            ->add('campaignType', HiddenType::class, array('data' => $campaignType))
            ->add('bg', HiddenType::class, array('data' => $bg))
            ->add('cat', HiddenType::class, array('data' => $cat))
            ->add('submit', SubmitType::class, array('label' => 'Create Segment'))
            ->setAction($this->generateUrl('dcc_segment_form'))
            ->getForm();

        $segmentForm->handleRequest($request);

        return $segmentForm;
    }

    /**
     * @Route("/dccsegmentform", name="dcc_segment_form")
     */
    public function dccSegmentFormAction(Request $request)
    {
        $instance = ($request->request->get('campaignType') == "B2C")?'PhilipsConsumerLifestyle':'PhilipsHealthcareProd';
        $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => $instance));
        $this->elqCredentials = $cred->getCredString();

        $settings = [];
        $settings['folderId'] = $request->request->get('folder');
        $settings['name'] = $request->request->get('name');
        $settings['campaignType'] = $request->request->get('campaignType');
        $settings['bg'] = $request->request->get('bg');
        $settings['cat'] = $request->request->get('cat');

        $uploadResponse = $this->forward('AppBundle:DccUpload:segmentUpload', array(
            'request'  => $request,
            'settings' => $settings,
            'elqCredentials' => $this->elqCredentials
        ));

        return $uploadResponse;
    }

    public function dccEmailAction($elqCredentials, $folders = null, $emails = null, $campaignName = null, $campaignType = null, $country = null, $bgcat = null, Request $request)
    {
        $emailCreation = [];

        $emailFormBuilder = $this->get('form.factory')->createNamedBuilder('dccEmailForm', 'Symfony\Component\Form\Extension\Core\Type\FormType', $emailCreation, array('attr' => array('class' => 'dccEmailForm')))
            ->add('folder', ChoiceType::class, array('choices' => $folders, 'label' => 'Select the folder to create the emails in.', 'attr' => array('class' => 'emailFolders', 'size' => '15')))
            ->add('country', HiddenType::class, array('data' => $country));

        $fileNames = array();
        if($emails !== null){
            foreach($emails as $k => $em){
                $emName = preg_replace('/[^\d\w\-\:]/', '', $em);
                $emailFormBuilder->add($emName, TextType::class, array('label' => $em.': Eloqua name', 'attr' => array('class' => 'emailNameDcc')));
                $fileNames[$em] = $emName;
            }
        }
            
        $emailFormBuilder->add('submit', SubmitType::class, array('label' => 'Create Emails'))
            ->add('campaignType', HiddenType::class, array('data' => $campaignType))
            ->add('campaignName', HiddenType::class, array('data' => $campaignName))
            ->add('bgcat', HiddenType::class, array('data' => $bgcat))
            ->add('fileNames', HiddenType::class, array('data' => str_replace("\"","'",json_encode($fileNames))))
            ->setAction($this->generateUrl('dcc_email_form'));

        $emailForm = $emailFormBuilder->getForm();

        $emailForm->handleRequest($request);

        return $emailForm;
    }

    /**
     * @Route("/dccemailform", name="dcc_email_form")
     */
    public function dccEmailFormAction(Request $request)
    {   
        $instance = ($request->request->get('campaignType') == "B2C")?'PhilipsConsumerLifestyle':'PhilipsHealthcareProd';
        $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => $instance));
        $this->elqCredentials = $cred->getCredString();

        $fileNames = json_decode(str_replace("'","\"",$request->request->get('fileNames')));
        $names = json_decode($request->request->get('names'));
        $eloquaNames = array();
        foreach($fileNames as $htmlName => $fileName){
            if(isset($names->$fileName)){
                $eloquaNames[$htmlName] = $names->$fileName;
            }
        }

        $settings = [];
        $settings['folder'] = $request->request->get('folder');
        $settings['eloquaNames'] = $eloquaNames;
        $settings['country'] = $request->request->get('country');
        $settings['campaignName'] = $request->request->get('campaignName');
        $settings['campaignType'] = $request->request->get('campaignType');
        $settings['bgcat'] = $request->request->get('bgcat');

        $uploadResponse = $this->forward('AppBundle:DccUpload:emailUpload', array(
            'request'  => $request,
            'settings' => $settings,
            'elqCredentials' => $this->elqCredentials
        ));

        return $uploadResponse;
    }

    private function prepareDataAction($dataSet)
    {
        $prepData = array();
        foreach($dataSet as $k => $v){
            if($k == 'campaign'){
                $row = array();
                $row['name'] = 'DCC Details';
                $fields = array();
                $anyFields = false;
                foreach($v as $k1 => $v1){
                    $k1 = ucfirst(str_replace("_"," ",$k1));
                    $f = array('name' => $k1, 'value' => $v1);
                    array_push($fields, $f);
                    $anyFields = true;
                }
                $row['fields'] = $fields;
                if($anyFields){
                    array_push($prepData, $row);
                }
            }elseif($k == 'briefing'){
                foreach($v as $subset){
                    if($subset->briefingcategory == "Briefing"){
                        $counter = 1;
                        foreach($subset->fields as $k2 => $v2){
                            $row = array();
                            $fields = array();
                            $row['name'] = "Audience ".$counter;
                            $segmentToArray = (array) $v2;
                            $anySegments = false;
                            foreach($segmentToArray as $k3 => $v3){
                                $k3 = ucfirst(str_replace("_"," ",$k3));
                                $f = array('name' => $k3, 'value' => $v3);
                                array_push($fields, $f);
                                $anySegment = true;
                            }
                            $row['fields'] = $fields;
                            if($anySegment){
                                array_push($prepData, $row);
                            }
                            $counter++;
                        }
                    }else{
                        $row = array();
                        $row['name'] = $subset->briefingcategory;
                        $fields = array();
                        $anyFields = false;
                        foreach($subset->fields as $k2 => $v2){
                            $k2 = ucfirst(str_replace("_"," ",$k2));
                            $f = array('name' => $k2, 'value' => $v2);
                            array_push($fields, $f);
                            $anyFields = true;
                        }
                        $row['fields'] = $fields;
                        if($anyFields){
                            array_push($prepData, $row);
                        }
                    }
                }
            }
        }
        return $prepData;
    }

    private function fetchFoldersAction($type, $elqCredentials, Request $request)
    {
        $elq = new EloquaRequest($this->getDoctrine(), $elqCredentials);
        $query = '';
        if($type == "email"){
            $query = 'assets/email/folders?depth=minimal&count=1000&page=';
        }elseif($type == "segment"){
            $query = 'assets/contact/segment/folders?depth=minimal&count=1000&page=';
        }

        $i = 1;
        $data = $elq->get($query.$i);
        while (isset($data->elements) && count($data->elements) > 0) {
            foreach ($data->elements as $f) {
                $folders[$f->id] = $f;
            }

            $i++;
            $data = $elq->get($query.$i); 
        }

        //remove the root folder as it has no parent folderID
        foreach($folders as $k=>$f){
            if(!isset($f->folderId)){
                $root = $f;
                unset($folders[$k]);
            }
        }

        $this->parsedFolders = Array();
        $folderStructure = self::orderFoldersAction($root->id, 0, $folders);
        self::parseFolderNamesAction($folderStructure, 0);
        return $this->parsedFolders;
    }

    private function orderFoldersAction($parent, $rootLevel, $folders)
    {
        $structure = null;
        $folder = null;
        usort($folders, function($a, $b){
            return ($a->name < $b->name) ? -1 : 1;
        });
        foreach ($folders as $k1 => $f1){
            if($f1->folderId == $parent){
                unset($folders[$parent]);
                if($rootLevel){
                    $structure[$f1->id]['name'] = $f1->name;
                    $children = self::orderFoldersAction($f1->id, false, $folders);
                    if($children != null){
                        $structure[$f1->id]['child'] = $children;
                    }
                }else{
                    $folder[$f1->id]['name'] = $f1->name;
                    $children = self::orderFoldersAction($f1->id, false, $folders);
                    if($children != null){
                        $folder[$f1->id]['child'] = $children;
                    }
                }
            }
        }
        if($rootLevel){
            return $structure;
        }else{
            return $folder;
        }
    }

    private function parseFolderNamesAction($structure, $depth)
    {
        foreach($structure as $key => $folder){
            $name = '';
            for($i=0;$i<$depth;$i++){
                $name .= ($i==0)?'&nbsp;':'|';
                $name .= '&nbsp;&nbsp;&nbsp;';
            }
            if($depth != 0){
                $name .= '|_';
            }
            $name .= ' '.$folder['name'];
            $this->parsedFolders[$name] = $key;
            //run function for child folders (if any)
            if(isset($folder['child'])){
                $newDepth = $depth+1;
                self::parseFolderNamesAction($folder['child'],$newDepth);
            }
        }
        return true;
    }

}
?>