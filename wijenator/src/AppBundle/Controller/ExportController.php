<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\ExportSettings;
use AppBundle\Entity\ExportFields;
use AppBundle\Service\EloquaRequest;

class ExportController extends Controller
{
	/**
     * @Route("/export", name="export")
     */
    public function ExportAction(Request $request)
    {
    	$creds = explode('|||',$request->getSession()->get('selectedCredential'));
    	$username = $creds[1];

    	$unrestrictedUsers = $this->getDoctrine()
                ->getRepository('AppBundle:UnrestrictedUsers')
                ->findAll();

        $restricted = true;
        foreach($unrestrictedUsers as $user){
            if(strtolower($user->getUsername()) == strtolower($username)){
                $restricted = false;
            }
        }
        if(!$restricted){
	    	$exports = $this->getDoctrine()->getRepository('AppBundle:ExportSettings')->findBy(array('enabled' => true),array('type' => 'ASC'));
	    	return $this->render('tools/export.html.twig', array(
	            'exports' => $exports,
	            'title' => 'CSV exports'
	        ));
	    }else{
	    	return new Response('You don\'t have access to this part of the tool.');
	    }
    }

	/**
     * @Route("/export/{type}", name="exportType")
     */
    public function generateCsvAction(Request $request, $type)
    {
    	set_time_limit(6000);
    	$elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));

    	$settings = $this->getDoctrine()->getRepository('AppBundle:ExportSettings')->findOneBy(array('type' => $type));
    	$sslField = $this->getDoctrine()->getRepository('AppBundle:ExportFields')->findOneBy(array('name' => 'SSL Secured'))->getSystemName();
    	$msField = $this->getDoctrine()->getRepository('AppBundle:ExportFields')->findOneBy(array('name' => 'Microsite name'))->getSystemName();

    	$userRequired = false;
    	$micrositeRequired = false;
    	$optionListRequired = false;
    	$users = [];
    	$microsites = [];
    	$optionLists = [];
    	$fields = $settings->getFields();
    	$fieldNames = [];
    	foreach ($fields as $field){
    		$name = ($field->getName() == "ID" || $field->getName() == "Name")?ucfirst($type).' '.$field->getName():$field->getName();
    		array_push($fieldNames,$name);
    		if($field->getType() == 'user'){$userRequired = true;}
    		if($field->getType() == 'microsite'){$micrositeRequired = true;}
    		if($field->getType() == 'optionlist'){$optionListRequired = true;}
    	}

    	if ($micrositeRequired){
    		$micrositeData = $elq->get('assets/microsites?depth=partial&count=10');
    		foreach($micrositeData->elements as $ms){
				if(isset($ms->domains[0])){
					$microsites[$ms->id] = array('name' => $ms->name, 'domain' => end($ms->domains), 'prefix' => ($ms->$sslField == 1)?'https://':'http://');
				}else{
					$microsites[$ms->id] = array('name' => $ms->name, 'domain' => "not set/unknown", 'prefix' => ($ms->$sslField == 1)?'https://':'http://');
				}
			}
    	}

    	if ($optionListRequired){
    		$i=1;
    		$optionListData = $elq->get('assets/optionLists?depth=minimal&count=1000&page='.$i);
    		while (count($optionListData->elements) > 0) {
				$i++;
		    		foreach ($optionListData->elements as $ol) {
						if(isset($ol->name)){
							$optionLists[$ol->id] = $ol->name;
						}else{
							$optionLists[$ol->id] = "Undefined name";
						}
					}
				$optionListData = $elq->get('assets/optionLists?depth=minimal&count=1000&page='.$i);
			}
    	}

    	if ($userRequired){
	    	$userData = $elq->get('system/users?depth=minimal&count=1000');
			foreach ($userData->elements as $user) {
				$users[$user->id] = $user->name;
			}
			for ($i = 0; $i <= 10; $i++){
				if(!isset($users[$i])){
					$users[$i] = "Oracle system user";
				}
			}
		}

    	$callback = function() use($fieldNames, $elq, $users, $microsites, $settings, $fields, $msField, $optionLists) {
	        $handle = fopen('php://output', 'w+');

	        fputcsv($handle, $fieldNames,',');

	        $query = $settings->getEndpoint().'?depth='.$settings->getDepth().'&count='.$settings->getCount().'&orderBy=id&page=';
    		$i=1;
	        $data = $elq->get($query.$i);

	        if(!isset($data->elements)){dump($data);die();}

	        while (count($data->elements) > 0) {
				$i++;

				foreach ($data->elements as $record) {
					$recordData = [];

					foreach ($fields as $field){
						$systemName = $field->getSystemName();

						switch($field->getType()){
							case 'text':
								array_push($recordData,$record->$systemName);
								break;

							case 'user':
								$alternative = "unknown";
								if($systemName == 'runAsUserId'){
									$alternative = "not activated";
								}
								$user = (isset($record->$systemName))?$users[$record->$systemName]:$alternative;
								array_push($recordData,$user);
								break;

							case 'date':
								if(property_exists($record, $systemName)){
									array_push($recordData,date("Y-m-d H:i:s",(int)$record->$systemName));
								}
								break;

							case 'url':
								$url = '';
								if(isset($record->$systemName)){
									$url = ($microsites[$record->$msField]['domain']!=="not set/unknown")?$microsites[$record->$msField]['prefix'].$microsites[$record->$msField]['domain'].$record->$systemName:$record->$systemName;
								}else{
									if(isset($record->$msField)){
										$url = ($microsites[$record->$msField]['domain']!=="not set/unknown")?$microsites[$record->$msField]['prefix'].$microsites[$record->$msField]['domain']."/LP=".$record->id:"";
									}else{
										$url="";
									}
								}
								array_push($recordData,$url);
								break;

							case 'microsite':
								$micrositeName = (isset($record->$systemName))?$microsites[$record->$systemName]['name']:'not set';
								array_push($recordData,$micrositeName);
								break;

							case 'optionlist':
								$optionList = (isset($record->$systemName))?$optionLists[$record->$systemName]:'';
								array_push($recordData,$optionList);
								break;
							case 'campaign cloud app':
								$cloudapps = '';
								foreach ($record->elements as $step){
									if($step->type == "CampaignCloudAction"){
										$cloudapps .= $step->$systemName.' | ';
									}
								}
								array_push($recordData,$cloudapps);
								break;
							case 'submissiondate':
								$formId = $record->id;
								$submissionDate = $elq->get('data/form/'.$formId.'?count=1');
								if(count($submissionDate->elements) === 0){
									array_push($recordData,'');
								}else{
									array_push($recordData,date("Y-m-d H:i:s",$submissionDate->elements[0]->$systemName));
								}
								break;
							case 'elementcount':
								if(isset($record->elements)){
									array_push($recordData, count($record->elements));
								}else{
									array_push($recordData, 'Unknown');
								}
								break;
						}
					}
					fputcsv($handle,$recordData,',');
				}
				$data = $elq->get($query.$i);
				if(!isset($data->elements)){dump($data);die();}
			}
	        fclose($handle);
	    };

	    $response = new StreamedResponse($callback, 200, array(
	    	'Content-Type' => 'text/csv; charset=utf-8',
	    	'Content-Disposition' => 'attachment; filename="'.date("Ymd_His").'_'.ucfirst($settings->getType()).'_export.csv"'
	    ));

	    return $response;
    }
}