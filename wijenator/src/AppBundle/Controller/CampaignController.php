<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Service\EloquaRequest;

class CampaignController extends Controller
{
    /**
     * @Route("/campaignchecker/{type}", name="campaigns")
     */
    public function campaignAction(Request $request, $type)
    {
        $searchRequest = [];
        $campaignForm = null;
        $credString = "";
        if ($type == 'b2c'){
            $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => 'PhilipsConsumerLifestyle'));
            if($cred === null){return new Response('You don\'t have credentials for the B2C instance PhilipsConsumerLifestyle, please enter your credentials for B2C to be able to use the B2C Boomenator.');}
            $credString = $cred->getCredString();
            $twigType = 'tools/b2cCampaign.html.twig';
        } elseif ($type == 'b2b'){
            $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => 'PhilipsHealthcareProd'));
            if($cred === null){return new Response('You don\'t have credentials for the B2B instance PhilipsHealthcareProd, please enter your credentials for B2B to be able to use the B2B Boomenator.');}
            $credString = $cred->getCredString();
            $twigType = 'tools/b2bCampaign.html.twig';
        }
        $elq = new EloquaRequest($this->getDoctrine(),$credString);
        $this->session = $request->getSession();
        $this->session->set('selectedCredential',$credString);

        $campaigns = null;
        $campaignResult = null;
        $results = null;

        if($type == 'b2b'){
            $searchForm = $this->get('form.factory')->createNamedBuilder('campaignSearch', 'Symfony\Component\Form\Extension\Core\Type\FormType', $searchRequest)
                ->add('query', TextType::class, array('label' => 'Enter campaign name', 'required' => true))
                ->add('submitSearch', SubmitType::class, array('label' => 'Check campaign'))
                ->setAction($this->generateUrl('campaigns', array('type' => $type)))
                ->getForm();
        }else{
            $searchForm = $this->get('form.factory')->createNamedBuilder('campaignSearch', 'Symfony\Component\Form\Extension\Core\Type\FormType', $searchRequest)
                ->add('query', TextType::class, array('label' => 'Enter campaign name', 'required' => true))
                ->add('submitSearch', SubmitType::class, array('label' => 'Check campaign'))
                ->setAction($this->generateUrl('campaigns', array('type' => $type)))
                ->getForm();
        }

        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchRequest = $searchForm->getData();

            $campaigns = $elq->get('assets/campaigns?depth=minimal&orderBy=name&search=\''.urlencode(str_replace("'","*",$searchRequest['query'])).'\'');
            if(is_object($campaigns)) {
                foreach ($campaigns->elements as $campaign){
                    $campaignResult[$campaign->name] = $campaign->id;
                }

                if($campaignResult !== NULL){
                    if(count($campaignResult) == 1 ){
                       if($type == 'b2b'){
                            return $this->forward('AppBundle:Checker:'.$type, array(
                                'request'  => $request,
                                'campaignId' => reset($campaignResult),
                                'renderInfo' => array(
                                    'searchForm' => $searchForm->createView(),
                                    'title' => 'Check '.strtoupper($type).' Campaign',
                                    'campaignForm' => null
                                    )
                            ));
                        }else{
                            return $this->forward('AppBundle:Checker:'.$type, array(
                                'request'  => $request,
                                'campaignId' => reset($campaignResult),
                                'renderInfo' => array(
                                    'searchForm' => $searchForm->createView(),
                                    'title' => 'Check '.strtoupper($type).' Campaign',
                                    'campaignForm' => null
                                    )
                            ));
                        }
                    }elseif(count($campaignResult) > 1){
                        $campaignRequest = [];

                        $campaignForm = $this->get('form.factory')->createNamedBuilder('CampaignSelect', 'Symfony\Component\Form\Extension\Core\Type\FormType', $campaignRequest)
                            ->add('campaigns', ChoiceType::class, array('choices' => $campaignResult, 'expanded' => true, 'required' => true))
                            ->add('submitCampaign', SubmitType::class, array('label' => 'Check campaign'))
                            ->setAction($this->generateUrl('campaigns', array('type' => $type)))
                            ->getForm();

                        $campaignForm->handleRequest($request);

                        if ($campaignForm->isSubmitted() && $campaignForm->isValid()) {
                            if($type == 'b2b'){
                                return $this->forward('AppBundle:Checker:'.$type, array(
                                    'request'  => $request,
                                    'campaignId' => reset($campaignResult),
                                    'renderInfo' => array(
                                        'searchForm' => $searchForm->createView(),
                                        'title' => 'Check '.strtoupper($type).' Campaign',
                                        'campaignForm' => $campaignForm->createView()
                                        ),
                                    'lcp' => $searchRequest['lcp']
                                ));
                            }else{
                                return $this->forward('AppBundle:Checker:'.$type, array(
                                    'request'  => $request,
                                    'campaignId' => reset($campaignResult),
                                    'renderInfo' => array(
                                        'searchForm' => $searchForm->createView(),
                                        'title' => 'Check '.strtoupper($type).' Campaign',
                                        'campaignForm' => $campaignForm->createView()
                                        )
                                ));
                            }
                        }
                    }
                }
            }else{
                $response = new \stdClass();
                $response->failed = 'The Eloqua API currently isn\'t responding. Please try to reset your password first as that sometimes solves it, otherwise the Eloqua API is just encountering some issues, and we unfortunately have to try again later.';
                return $this->render($twigType, array(
                    'searchForm' => $searchForm->createView(),
                    'title' => 'Check '.strtoupper($type).' Campaign',
                    'campaignForm' => null,
                    'results' => $response,
                    'manualCheckForm' => null
                ));
            }
        }

        $campaignFormRendered = ($campaignForm == null)?null:$campaignForm->createView();

        return $this->render($twigType, array(
            'searchForm' => $searchForm->createView(),
            'title' => 'Check '.strtoupper($type).' Campaign',
            'campaignForm' => $campaignFormRendered,
            'results' => $results,
            'manualCheckForm' => null
        ));
    }
}