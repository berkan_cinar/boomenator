<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Service\EloquaRequest;
use AppBundle\Helper\Contact;
use AppBundle\Helper\Deployment;

class ProofController extends Controller
{
    /**
     * @Route("/proofs/{handleCampaign}", name="proofs")
     */
    public function proofAction(Request $request, $handleCampaign = false)
    {
        $searchRequest = [];
        $campaignForm = null;
        $emailForm = null;
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));
        $campaigns = null;
        $campaignResult = null;

        $searchForm = $this->get('form.factory')->createNamedBuilder('proofSearch', 'Symfony\Component\Form\Extension\Core\Type\FormType', $searchRequest)
            ->add('query', TextType::class, array('label' => 'Search by name', 'required' => true))
            ->add('type', ChoiceType::class, array('choices' => array('Email based search' => 'email','Campaign based search' => 'campaign'), 'expanded' => true, 'required' => true))
            ->add('submitSearch', SubmitType::class, array('label' => 'Search'))
            ->setAction($this->generateUrl('proofs'))
            ->getForm();

        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchRequest = $searchForm->getData();

            if($searchRequest['type'] == 'campaign'){
                $campaigns = $elq->get('assets/campaigns?depth=minimal&orderBy=name&search=\''.urlencode($searchRequest['query']).'\'');
                foreach ($campaigns->elements as $campaign){
                    $campaignResult[$campaign->name] = $campaign->id;
                }
                $handleCampaign = true;                
            }elseif($searchRequest['type'] == 'email'){
                $emails = $elq->get('assets/emails?depth=minimal&orderBy=name&search=\''.urlencode($searchRequest['query']).'\'');
                $emailResult = [];
                foreach ($emails->elements as $email){
                    $emailResult[$email->name] = $email->id;
                }
                $emailForm = self::emailFormAction($emailResult, null,$request);
            }
        }

        if($handleCampaign){
            
            $campaignRequest = [];

            if($campaignResult == null){
                $campaignResult = (array) json_decode($request->getSession()->get('campaignSet'));
            }

            $request->getSession()->set('campaignSet',json_encode($campaignResult));
            $request->getSession()->save();

            $campaignForm = $this->get('form.factory')->createNamedBuilder('proofCampaignSelect', 'Symfony\Component\Form\Extension\Core\Type\FormType', $campaignRequest)
                ->add('campaigns', ChoiceType::class, array('choices' => $campaignResult, 'expanded' => true, 'required' => true))
                ->add('submitCampaign', SubmitType::class, array('label' => 'Fetch emails'))
                ->setAction($this->generateUrl('proofs', array('handleCampaign' => true)))
                ->getForm();

            $campaignForm->handleRequest($request);

            if ($campaignForm->isSubmitted() && $campaignForm->isValid()) {
                $emailSet = [];
                $campaignRequest = $campaignForm->getData();
                
                $emailForm = self::emailFormAction(null, $campaignRequest['campaigns'], $request);
            }else{
                if(count($campaignResult) == 1){
                    $emailForm = self::emailFormAction(null, reset($campaignResult), $request);
                }
            }
        }

        $campaignFormRendered = ($campaignForm == null)?null:$campaignForm->createView();
        $emailFormRendered = ($emailForm == null)?null:$emailForm->createView();

        return $this->render('tools/proofs.html.twig', array(
            'searchForm' => $searchForm->createView(),
            'title' => 'Send proofs',
            'campaignForm' => $campaignFormRendered,
            'emailForm' => $emailFormRendered
        ));
    }

    /**
     * @Route("/proofemails", name="proof_emails")
     */
    public function emailFormAction($emailResult = null, $campaignID = null, Request $request)
    {
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));

        if($campaignID !== null){
            $data = $elq->get('/assets/campaign/'.$campaignID.'?depth=complete');
            foreach ($data->elements as $step) {
                if($step->type=="CampaignEmail"){
                    $email = $elq->get('/assets/email/'.$step->emailId.'?depth=minimal');
                    $emailResult[$email->name] = $email->id;
                }
            }
            if(count($emailResult) < 1){
                $emailResult['No emails available'] = 0;
            }
        }

        $userEmail = $elq->get('system/user/current')->emailAddress;
        $emailRequest = [];

        if($emailResult == null){
            $emailResult = (array) json_decode($request->getSession()->get('emailSet'));
        }

        $request->getSession()->set('emailSet',json_encode($emailResult));
        $request->getSession()->save();

        $emailForm = $this->get('form.factory')->createNamedBuilder('proofEmailSelect', 'Symfony\Component\Form\Extension\Core\Type\FormType', $emailRequest, array('attr' => array('class' => 'emailProofForm')))
            ->add('emails', ChoiceType::class, array('choices' => $emailResult, 'expanded' => true, 'multiple' => true, 'required' => true))
            ->add('emailaddress', TextType::class, array('label' => 'Email address', 'data' => $userEmail))
            ->add('submit', SubmitType::class, array('label' => 'Send proof(s)'))
            ->setAction($this->generateUrl('proof_emails'))
            ->getForm();

        $emailForm->handleRequest($request);

        if ($request->request->get('emailID') != '' && $request->request->get('emailaddress') != '') {
            $email = $request->request->get('emailaddress');

            $proofResponse = $this->forward('AppBundle:Proof:sendProof', array(
                'emailId'  => $request->request->get('emailID'),
                'emailAddress' => $request->request->get('emailaddress'),
                'request' => $request
            ));

            return $proofResponse;
        }

        return $emailForm;
    }


    public function sendProofAction($emailId, $emailAddress, $request)
    {
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));
        $r = $elq->get("/data/contacts?search='".$emailAddress."'&depth=partial");
            
        if ($r->elements[0]->name == $emailAddress) {
            $contact = $r->elements[0];
            if ($r->elements[0]->isSubscribed != "true") {
                $c = new Contact;
                $c->setId($r->elements[0]->id);
                $c->setEmailAddress($r->elements[0]->emailAddress);
                $c->setIsSubscribed("true");
                $sub = $elq->put("/data/contact/".$r->elements[0]->id,$c);
            }
            $emaildata = $elq->get("/assets/email/".$emailId);
            $emaildata->subject = "* TEST * ".$emaildata->subject." * TEST *";
            
            $deployment = new Deployment;
            $deployment->setContactId($contact->id);
            $deployment->setEmail($emaildata);
            
            $proof = $elq->post("/assets/email/deployment", $deployment);

            if (isset($proof->failedSendCount) && $proof->failedSendCount == "0"){
                return new Response("SUCCESS");
            } else {
                foreach ($proof as $id => $obj) {
                    $str[] = $obj->type." : ".$obj->property." > ".$obj->requirement->type;
                }
                return new Response(implode("\n",$str));
            }        
        } else {
            return new Response("Contact doesn't exist");
        } 
    }
}