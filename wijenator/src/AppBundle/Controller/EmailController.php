<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use AppBundle\Service\EloquaRequest;

class EmailController extends Controller
{
    /**
     * @Route("/emailchecker/{type}", name="emailcheck")
     */
    public function emailAction(Request $request, $type)
    {
        $searchRequest = [];
        $emailForm = null;
        $manualCheckFormRendered = null;
        $credString = "";

        if ($type == 'b2c'){
            $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => 'PhilipsConsumerLifestyle'));
            if($cred === null){return new Response('You don\'t have credentials for the B2C instance PhilipsConsumerLifestyle, please enter your credentials for B2C to be able to use the B2C email checker.');}
            $credString = $cred->getCredString();
        } elseif ($type == 'b2b'){
            $cred = $this->getDoctrine()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => 'PhilipsHealthcareProd'));
            if($cred === null){return new Response('You don\'t have credentials for the B2B instance PhilipsHealthcareProd, please enter your credentials for B2B to be able to use the B2B email checker.');}
            $credString = $cred->getCredString();
        }
        

        $elq = new EloquaRequest($this->getDoctrine(),$credString);
        $this->session = $request->getSession();
        $this->session->set('selectedCredential',$credString);

        $emails = null;
        $emailResult = null;
        $results = null;
        $rr = null;

        if ($type == 'b2c'){
            $searchForm = $this->get('form.factory')->createNamedBuilder('emailSearch', 'Symfony\Component\Form\Extension\Core\Type\FormType', $searchRequest)
                ->add('query', TextType::class, array('label' => 'Enter email name', 'required' => true))
                ->add('rrEmail', CheckboxType::class, array('label' => 'R&R email', 'required' => false))
                ->add('submitSearch', SubmitType::class, array('label' => 'Check email'))
                ->setAction($this->generateUrl('emailcheck', array('type' => $type)))
                ->getForm();
        } elseif ($type == 'b2b'){
            $searchForm = $this->get('form.factory')->createNamedBuilder('emailSearch', 'Symfony\Component\Form\Extension\Core\Type\FormType', $searchRequest)
                ->add('query', TextType::class, array('label' => 'Enter email name', 'required' => true))
                ->add('rrEmail', HiddenType::class, array('data' => 'NA'))
                ->add('submitSearch', SubmitType::class, array('label' => 'Check email'))
                ->setAction($this->generateUrl('emailcheck', array('type' => $type)))
                ->getForm();
        }

        $searchForm->handleRequest($request);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchRequest = $searchForm->getData();

            $rr = ($searchRequest['rrEmail'] == 1)?true:false;

            $emails = $elq->get('assets/emails?depth=minimal&orderBy=name&search=\''.urlencode(str_replace("'","*",$searchRequest['query'])).'\'');
            foreach ($emails->elements as $email){
                $emailResult[$email->name] = $email->id;
            }

            if($emailResult !== NULL){
                $emailForm = null;
                $forwardToChecker = false;
                if(count($emailResult) == 1 ){
                    $forwardToChecker = true;
                }elseif(count($emailResult) > 1){
                    $emailRequest = [];

                    $emailForm = $this->get('form.factory')->createNamedBuilder('EmailSelect', 'Symfony\Component\Form\Extension\Core\Type\FormType', $emailRequest)
                        ->add('emails', ChoiceType::class, array('choices' => $emailResult, 'expanded' => true, 'required' => true))
                        ->add('submitEmail', SubmitType::class, array('label' => 'Check email'))
                        ->setAction($this->generateUrl('emailcheck', array('type' => $type)))
                        ->getForm();

                    $emailForm->handleRequest($request);

                    if ($emailForm->isSubmitted() && $emailForm->isValid()) {
                        $forwardToChecker = true;
                    }
                }

                if($forwardToChecker){
                    reset($emailResult);
        
                    $emailResults = new \stdClass();

                    $draftErrors = $elq->get('assets/email/'.reset($emailResult).'/active/validationErrors');
                    if($draftErrors !== null){
                        $emailResults->failed = "There are draft errors on the campaign. Please fix these before using the Boomenator.";
                        $renderInfo = array();
                        $renderInfo['searchForm'] = $searchForm->createView();
                        $renderInfo['title'] = 'Check '.strtoupper($type).' Email';
                        $renderInfo['emailForm'] = null;
                        $renderInfo['manualCheckForm'] = null;
                        $renderInfo['results'] = $emailResults;
                        return $this->render('tools/EmailChecker.html.twig', $renderInfo);
                    }
                    if($type == 'b2c'){
                        $nameResponse =  $this->forward('AppBundle:Checker:b2cNamingCheck', array(
                            'name'  => key($emailResult),
                            'assetType' => 'EM',
                            'external' => true
                        ));
                    } elseif ($type == 'b2b'){
                        $nameResponse =  $this->forward('AppBundle:Checker:b2bNamingCheck', array(
                            'name'  => key($emailResult),
                            'assetType' => 'email',
                            'external' => true
                        ));
                    }

                    $nameResults = json_decode($nameResponse->getContent());

                    if($nameResults->country === null){
                        $emailResults->failed = "Email name is missing country. The tool will not work without this in the naming.";
                        $renderInfo['results'] = $emailResults;
                        return $this->render('tools/b2cEmailChecker.html.twig', $renderInfo);
                    }

                    $userEmail = $elq->get('system/user/current')->emailAddress;

                    if($type == 'b2c'){
                        $regionMap = $this->getDoctrine()->getRepository('AppBundle:RegionMapping')->findOneBy(array('country' => $nameResults->country));
                        if($regionMap === null){
                            $emailResults->failed = "This country's details have not been included in the Boomenator yet.<br>Please send an email to Bram with the following information:<ul><li>Country code (".$nameResults->country.")</li><li>Region (EMEA/APAC/AMER)</li><li>Timezone (for example: (UTC) Casablanca)</li></ul>";
                            $renderInfo['results'] = $emailResults;
                            return $this->render('tools/b2cEmailChecker.html.twig', $renderInfo);
                        }
                    }elseif($type == 'b2b'){
                        $regionMap = $this->getDoctrine()->getRepository('AppBundle:RegionMapping')->findOneBy(array('type' => 'B2B'));
                    }
                    $regionRes = $regionMap->getRegion();
                    $region = $this->getDoctrine()->getRepository('AppBundle:RegionSettings')->findOneBy(array('id' => $regionRes->getId()));

                    $emailFormRendered = ($emailForm == null)?null:$emailForm->createView();

                    $emailResponse =  $this->forward('AppBundle:Checker:emailCheck', array(
                        'request'  => $request,
                        'emailId' => reset($emailResult),
                        'type' => $type,
                        'region' => $region,
                        'userEmail' => $userEmail,
                        'country' => $nameResults->country,
                        'external' => true,
                        'rr' => $rr
                    ));

                    $results = new \stdClass();
                    $results->failed = null;
                    $results->email = new \stdClass();

                    list($results->email->name, $results->email->eloqua, $results->email->nameErrors, $results->email->errors, $results->email->links, $results->email->images, $results->email->sizepx, $results->email->ptags, $results->email->dcs, $results->email->dupOmniture, $results->email->outlookImages, $results->email->comments, $results->email->styles, $results->email->dccTags, $results->email->bgGradients, $results->email->brokenFieldMerge, $results->email->doubleQuotes, $results->email->doubleStyleTags, $nameResults) = json_decode($emailResponse->getContent());

                    $manualCheckResults = [];
                    if($type == "b2c")
                    {
                        $manualChecks = array(
                            "Checked if all links work" => "1",
                            "Link tracking is enabled for all links (settings > manage links)" => "2",
                            "Buttons have correct width set for outlook" => "3",
                            "Email is responsive/looks good on mobile" => "4",
                            "Tags like [first name] replaced with dynamic content" => "5",
                            "Emails checked via litmus. Check the major email clients (outook desktop 15, android, iOS, gmail)" => "6",
                            "If code changes were made; brand approved and checked against all clients in litmus" => "7"
                        );
                        if($rr){
                             $rrManualChecks = array(
                                "Dynamic links exist" => "8",
                                "Dynamic image exists" => "9",
                                "R&R initial or reminder blind form is used" => "10"
                            );
                            $manualChecks = array_merge($manualChecks,$rrManualChecks);
                        }
                        if($nameResults->sector == "HC"){
                            $healthManualChecks = array(
                                "For gated email: Check if correct footer is used, should contain mailto: link to privacy" => "11",
                                "For gated email: check if the url's are working" => "12"
                            );
                            $manualChecks = array_merge($manualChecks,$healthManualChecks);
                        }
                        if($nameResults->country == "DE" || $nameResults->country == "AT" || $nameResults->country == "CH" || $nameResults->country == "US"){
                            $countryManualChecks = array(
                                "Email approved by requester" => "13"
                            );
                            $manualChecks = array_merge($manualChecks,$countryManualChecks);
                        }
                    }elseif($type == "b2b"){
                        $manualChecks = array(
                            "Checked if all links work" => "1",
                            "Link tracking is enabled for all links (settings > manage links)" => "2",
                            "Buttons have correct width set for outlook" => "3",
                            "Email is responsive/looks good on mobile" => "4",
                            "Tags like [first name] replaced with dynamic content" => "5",
                            "Emails checked via litmus. Check the major email clients (outook desktop 15, android, iOS, gmail)" => "6",
                            "If code changes were made; brand approved and checked against all clients in litmus" => "7"
                        );
                        if($nameResults->country == "DE" || $nameResults->country == "AT" || $nameResults->country == "CH" || $nameResults->country == "US"){
                            $countryChecks = array(
                                "Email approved by requester" => "8"
                            );
                            $manualChecks = array_merge($manualChecks,$countryManualChecks);
                        }
                    }
                    $manualCheckForm = $this->get('form.factory')->createNamedBuilder('ManualCheck', 'Symfony\Component\Form\Extension\Core\Type\FormType', $manualCheckResults)
                        ->add('manualChecks', ChoiceType::class, array('choices' => $manualChecks, 'expanded' => true, 'required' => true, 'multiple' => true))
                        ->getForm();
                    $manualCheckForm->handleRequest($request);
                    $manualCheckFormRendered = $manualCheckForm->createView();
                }
            }
        }

        $emailFormRendered = ($emailForm == null)?null:$emailForm->createView();
        return $this->render('tools/EmailChecker.html.twig', array(
            'searchForm' => $searchForm->createView(),
            'title' => 'Check '.strtoupper($type).' Email',
            'emailForm' => $emailFormRendered,
            'manualCheckForm' => $manualCheckFormRendered,
            'results' => $results
        ));
    }
}