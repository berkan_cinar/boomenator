<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class MenuCredentialsController extends Controller
{
    protected $session;

    /**
     * @Route("/menucreds", name="menu_credentials")
     */
    public function getCredentialsAction(Request $request)
    {
        $this->session = $request->getSession();
        $creds = $this->getDoctrine()
            ->getRepository('AppBundle:Credentials')
            ->findAll();

        $credOptions = array();
        foreach($creds as $cred){
           $credOptions[$cred->getInstance()] = $cred->getCredString();
        }

        $selected = reset($credOptions);
        if($this->session->get('selectedCredential') !== null && $this->session->get('selectedCredential') !== false){
            $selected = $this->session->get('selectedCredential');
        }else{
            $this->session->set('selectedCredential',$selected);
            $this->session->save();
        }

        $form = $this->get('form.factory')->createNamedBuilder('menuCredentialsForm', 'Symfony\Component\Form\Extension\Core\Type\FormType', null, array('attr' => array('class' => 'menuCredentialsForm')))
            ->add('credential', ChoiceType::class, array('choices' => $credOptions, 'label' => false, 'data' => $selected, 'attr' => array('class' => 'credForm')))
            ->setAction($this->generateUrl('menu_credentials'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $selectedCredential = $form->getData();
            $this->session->set('selectedCredential',$selectedCredential['credential']);
            $this->session->save();
        }

        return $this->render('credentials/credentials.html.twig', array(
            'form' => $form->createView()
        ));
    }
}