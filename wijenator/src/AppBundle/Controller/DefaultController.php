<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $allCredentials = $this->getDoctrine()
            ->getRepository('AppBundle:Credentials')
            ->findAll();
        if(!$allCredentials){
            $response = $this->forward('AppBundle:Credentials:setCredentials', array(
                'request'  => $request
            ));

            return $response;
        }

        $menuItems = $this->getDoctrine()
            ->getRepository('AppBundle:MenuLinks')
            ->findAll();

        $creds = explode('|||',$request->getSession()->get('selectedCredential'));
        $username = "";

        if(isset($creds[1])){
            $username = $creds[1];
        }else{
            $credQuery = $this->getDoctrine()
                ->getRepository('AppBundle:Credentials')
                ->findAll();

            $username = $credQuery[0]->getUsername();
        }

        $unrestrictedUsers = $this->getDoctrine()
                ->getRepository('AppBundle:UnrestrictedUsers')
                ->findAll();

        $restricted = true;
        foreach($unrestrictedUsers as $user){
            if(strtolower($user->getUsername()) == strtolower($username)){
                $restricted = false;
            }
        }

        if($restricted){
            foreach($menuItems as $key => $item){
                if($item->getRestricted()){
                    array_splice($menuItems,$key,1);
                }
            }
        }


        return $this->render('default/index.html.twig', [
            'menuItems' => $menuItems,
            'title' => 'Home',
            'user' => strtolower($username)
        ]);
    }
}
