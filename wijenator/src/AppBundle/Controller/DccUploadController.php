<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Finder\Finder;
use Oneup\UploaderBundle\OneupUploaderBundle;

use AppBundle\Service\EloquaRequest;
use AppBundle\Helper\Email;
use AppBundle\Helper\HtmlContent;
use AppBundle\Helper\ImageFolder;
use AppBundle\Helper\ImageFile;
use AppBundle\Helper\Segment\Segment;
use AppBundle\Helper\Segment\ContactFilterSegment;
use AppBundle\Helper\Segment\SubscriptionCriterion;
use AppBundle\Helper\Segment\UnsubscriptionCriterion;
use AppBundle\Helper\Segment\HardBouncebackCriterion;
use AppBundle\Helper\Segment\ActivityCriterion;
use AppBundle\Helper\Segment\ContactFieldCriterion;
use AppBundle\Helper\Segment\LinkedCustomObjectCriterion;
use AppBundle\Helper\Segment\cdoFieldCondition;
use AppBundle\Helper\Segment\activityRestriction;
use AppBundle\Entity\UploadSettings;
use AppBundle\Entity\RegionMapping;

use ZipArchive;

class DccUploadController extends Controller
{
	private $elq;
	private $zip;
	private $options;

	/*******************
	* Segment creation *
	*******************/
	public function segmentUploadAction(Request $request, $settings, $elqCredentials)
    {
    	$dir = $this->get('kernel')->getRootDir().'\..\web\uploads\dcc';
    	$finder = new Finder();
        $finder->files()->in($dir)->name('*.zip');
        $iterator = $finder->getIterator();
        $iterator->rewind();
        $zipFile = $iterator->current();
        $zip = new ZipArchive();
        $zip->open($zipFile);
        $dataSet = json_decode($zip->getFromName('briefing.json'));

        $this->options = $dataSet->briefing[0]->fields;
        $this->options->country = substr($dataSet->campaign->country,0,-3);
        $this->options->end_date = $dataSet->briefing[2]->fields->end_date;

    	$elq = new EloquaRequest($this->getDoctrine(), $elqCredentials);

        $elements = array();

        //create filter with segmentation options

        $criteria = array();
        $emailGroup = new SubscriptionCriterion();
        if(strtolower($bg.$cat) == "hwmcc"){
            //Set email group HW - MCC
            $emailGroup->setEmailGroups(array(475));
        }else{
            //Set email group General Newsletter
            $emailGroup->setEmailGroups(array(474));
        }
        
        array_push($criteria,$emailGroup);
        $statement = $emailGroup->getId();
        $country = new ContactFieldCriterion(100012);
        $country->setTextCondition('exactly',true,$this->options->country);
        array_push($criteria,$country);
        $statement .= $country->getId();

        if(isset($this->options->gender)){
            $gen = $this->options->gender;
            $gender = new ContactFieldCriterion(100192);
            if($gen == "male and female"){
                $gender->setTextCondition('exactly',true,1);
                array_push($criteria,$gender);
                $statement .= ' AND ('.$gender->getId();
                $gender2 = new ContactFieldCriterion(100192);
                $gender2->setTextCondition('exactly',true,2);
                array_push($criteria,$gender2);
                $statement .= ' OR '.$gender2->getId().')';
            }elseif($gen == 'male' || $gen == 'female'){
                $val = ($gen=='male')?1:2;
                $gender->setTextCondition('exactly',true,$val);
                array_push($criteria,$gender);
                $statement .= ' AND '.$gender->getId();
            }elseif($gen == 'unknown'){
                $gender->setTextCondition('exactly',false,1);
                array_push($criteria,$gender);
                $statement .= ' AND '.$gender->getId();
                $gender2 = new ContactFieldCriterion(100192);
                $gender2->setTextCondition('exactly',false,2);
                array_push($criteria,$gender2);
                $statement .= ' AND '.$gender2->getId();
            }
        }

        //age
        $fromValue = $this->options->audience_group_from;
        $toValue = $this->options->audience_group_to;
        if(isset($fromValue) && isset($toValue) && $fromValue != '' && $fromValue != 'N/A' && $toValue != '' && $toValue != 'N/A'){
            if(!(intval($fromValue) == 14 && $this->options->country != 'NL') && !(intval($fromValue) <= 16 && $this->options->country == 'NL')){
                $fromAge = new ContactFieldCriterion(100193);
                $months = $fromValue*12;
                $fromAge->setRelativeDateCondition('within last',false,$months,'month');
                array_push($criteria,$fromAge);
                $toAge = new ContactFieldCriterion(100193);
                $months = $toValue*12;
                $toAge->setRelativeDateCondition('within last',true,$months,'month');
                array_push($criteria,$toAge);
                $statement .= ' AND '.$fromAge->getId().' AND '.$toAge->getId();
            }
        }elseif((isset($fromValue) && isset($toValue) && $fromValue != '' && $fromValue != 'N/A' && ($toValue == '' || $toValue == 'N/A')) || (isset($fromValue) && !isset($toValue) && $fromValue != '' && $fromValue != 'N/A')){
            if(!(intval($fromValue) == 14 && $this->options->country != 'NL') && !(intval($fromValue) <= 16 && $this->options->country == 'NL')){
                $fromAge = new ContactFieldCriterion(100193);
                $months = $fromValue*12;
                $fromAge->setRelativeDateCondition('within last',false,$months,'month');
                array_push($criteria,$fromAge);
                $blankAge = new ContactFieldCriterion(100193);
                $blankAge->setBlankDate(true);
                array_push($criteria,$blankAge);
                $statement .= ' AND ('.$fromAge->getId().' OR '.$blankAge.')';
            }
        }elseif((isset($fromValue) && isset($toValue) && ($fromValue == '' || $fromValue == 'N/A') && $toValue != '' && $toValue != 'N/A') || (!isset($fromValue) && isset($toValue) && $fromValue != '' && $fromValue != 'N/A')){
            $toAge = new ContactFieldCriterion(100193);
            $months = $toValue*12;
            $toAge->setRelativeDateCondition('within last',true,$months,'month');
            array_push($criteria,$toAge);
            $blankAge = new ContactFieldCriterion(100193);
            $blankAge->setBlankDate(true);
            array_push($criteria,$blankAge);
            $statement .= ' AND ('.$toAge->getId().' OR '.$blankAge.')';
        }


        //TODO: ADD ALL TEH THINGS
    }
 
    /*****************
    * Email creation *
    *****************/
    public function emailUploadAction(Request $request, $settings, $elqCredentials)
    {
        if($settings['campaignType'] == "B2C"){
            $country = ($settings['country'] == "MyShop")?"NL":$settings['country'];
            $uploadValues = $this->getDoctrine()->getRepository('AppBundle:UploadSettings')->findOneBy(array('type' => 'b2cdcc'));
            $regionMap = $this->getDoctrine()->getRepository('AppBundle:RegionMapping')->findOneBy(array('country' => $country));
        }elseif($settings['campaignType'] == "B2B"){
            $uploadValues = $this->getDoctrine()->getRepository('AppBundle:UploadSettings')->findOneBy(array('type' => 'b2bdcc'));
            $regionMap = $this->getDoctrine()->getRepository('AppBundle:RegionMapping')->findOneBy(array('type' => 'B2B'));
        }

        $region = $regionMap->getRegion();

        $response = array();
        $elq = new EloquaRequest($this->getDoctrine(), $elqCredentials);
        
        //get zipfile
        $dir = $this->get('kernel')->getRootDir().'\..\web\uploads\dcc';

        foreach($settings['eloquaNames'] as $htmlName => $eloquaName){
            $finder = new Finder();
            $finder->files()->in($dir)->name($htmlName);
            $iterator = $finder->getIterator();
            $iterator->rewind();
            $file = $iterator->current()->getContents();

            $searchName = str_replace(' ','*',$settings['campaignName']);
            $searchName = str_replace("'",'*',$searchName);
            $imgFolderRes = $elq->get("/assets/image/folders?search=name='".$searchName."'");
            $folderExists = false;
            $folderId;
            foreach ($imgFolderRes->elements as $imgFolder){
                if($imgFolder->folderId == $uploadValues->getImageFolder()){
                    $folderExists = true;
                    $folderId = $imgFolder->id;
                    break;
                }
            }
            if(!$folderExists){
                $fol = new ImageFolder();
                $fol->setName($settings['campaignName']);
                $folRes = $elq->post('/assets/image/folder',$fol);
                $folderId = $folRes->id;
            }

            //max-width:none issue
            $file = preg_replace('/(\.outlook.*?{.*?max-width):none(.*?})/i','$1:999px$2',$file);
            //center CTA issue
            $file = preg_replace('/(<a.*?href=".*?".*?style=".*?)padding: 0px 12px;(.*?".*?>)\s*(<p.*?<\/p>)\s*<\/a>/im','$1$2<table border="0" cellspacing="0" cellpadding="0" align="center"><tr><td width="6%"><img class="spacerOneByOne" src="http://images.consumerproducts.philips.com/EloquaImages/clients/PhilipsConsumerLifestyle/%7B8c1da8b0-2efd-47d3-8c64-41b21960f2fb%7D_Blank_1x1.png" width="15" height="1" alt=""></td><td>$3</td><td width="6%"><img class="spacerOneByOne" src="http://images.consumerproducts.philips.com/EloquaImages/clients/PhilipsConsumerLifestyle/%7B8c1da8b0-2efd-47d3-8c64-41b21960f2fb%7D_Blank_1x1.png" width="15" height="1" alt=""></td></tr></table></a>',$file);

            $htmlContent = new HtmlContent();
            $htmlContent->setHtml(self::replaceImages($file, $folderId, $dir, $elq));

            $email = new Email();
            $email->setName($eloquaName);
            $email->setSubject(self::pageTitle($file));
            $email->setFolderId($settings['folder']);
            $email->setHtmlContent($htmlContent);
            $email->setReplyToName($region->getReplyToName());
            $email->setReplyToEmail($region->getReplyToEmail());
            $email->setSenderName($region->getSenderName());
            $email->setSenderEmail($region->getSenderEmail());
            $email->setBounceBackEmail($region->getBounceBackEmail());
            $email->setEmailHeaderId($uploadValues->getEmailHeaderId());
            $email->setEmailFooterId($uploadValues->getEmailFooterId());
            $email->setVirtualMTAId($region->getVirtualMtaId());
            if($settings['campaignType'] == "B2C"){
                if($settings['bgcat'] == "hwmcc"){
                    //Set email group HW - MCC
                    $email->setEmailGroupId(475);
                }elseif($settings['bgcat'] == "hwohc"){
                    //Set email group HW - OHC
                    $email->setEmailGroupId(476);
                }else{
                    //Set email group General Newsletter
                    $email->setEmailGroupId(474);
                }
            }

            $emailRes = $elq->post('/assets/email',$email);

            $response[] = array('emailName'=>$eloquaName,'emailId'=>$emailRes->id);

            $file = null;
        }

        return new Response(json_encode($response));
    }

    private function replaceImages($file, $folderId, $dir, $elq)
    {
        $matchedImages = array();

        preg_match_all("#(src=|background\-image\:url\(|background=)[\'\"](?P<image>images\/.*?\.(?:jpe?g|gif|png))(\"|\'\))#is",$file,$matches);
        foreach($matches['image'] as $match){
            if (0 !== strpos($match, 'http')) {
                if(!isset($matchedImages[$match])){
                    $filename = $dir.'\\'.$match;
                    $img_info = getimagesize($filename);
                    $img_real = realpath($filename);
                    $url_filename = preg_replace('/[^a-z\d_\-\.]+/i','',basename($img_real));
                    if(strlen($url_filename) > 100){
                        $ext = pathinfo($filename);
                        $shortened_filename = substr($ext['filename'], 0, (100-strlen($ext['extension'])-1));
                        $url_filename = $shortened_filename.'.'.$ext['extension'];
                    }
                    $data = "--ELOQUA_BOUNDARY\r\n".
                    "Content-Disposition: form-data; name=\"Filedata\"; filename=\"".$url_filename."\"\r\n".
                    "Content-Type: ".$img_info['mime']."\r\n".
                    "\r\n".
                    file_get_contents($img_real)."\r\n".
                    "--ELOQUA_BOUNDARY--";

                    $response = $elq->post('assets/image/content', $data, true);

                    $updateFolder = new ImageFile();    
                    $updateFolder->setType($response->type);
                    $updateFolder->setId($response->id);
                    $updateFolder->setName($response->name);
                    $updateFolder->setFolderId($folderId);

                    $elq->put("assets/image/".$response->id,$updateFolder);
                    $matchedImages[$match] = 'http://images.consumerproducts.philips.com'.urldecode($response->fullImageUrl);
                }
                    
                $file = str_replace($match, $matchedImages[$match], $file);
            }
        }
        return $file;
    }

    private function pageTitle($file)
    {
        $res = preg_match("/<title>(.*?)<\/title>/siU", $file, $title_matches);
        if (!$res) 
            return null; 

        // Clean up title: remove EOL's, excessive whitespace and replace special characters.
        $title = preg_replace('/\s+/', ' ', $title_matches[1]);
        $title = trim($title);
        $title = htmlspecialchars_decode($title);
        return $title;
    }
}