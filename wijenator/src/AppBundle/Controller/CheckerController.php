<?php
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\Debug\ErrorHandler;

use AppBundle\Entity\RegionMapping;
use AppBundle\Service\EloquaRequest;
use AppBundle\Helper\b2bFilter;
use AppBundle\Entity\CountryDomains;
use AppBundle\Entity\ConsistencyInfo;
use AppBundle\Entity\GlobalSplits;
use AppBundle\Helper\Contact;
use AppBundle\Helper\Deployment;

class CheckerController extends Controller
{
    private $fieldMergeSyntaxes = null;

    //======================================================================
    // B2C CAMPAIGN VALIDATION
    //======================================================================

    /**
     * @param Request $request
     * @param int $campaignId
     * @param array $renderInfo Contains info on rendering the form/search for the campaign
     * @return Response
     */
    public function b2cAction(Request $request, $campaignId, $renderInfo)
    {
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));

        $campaignResults = new \stdClass();

        $draftErrors = $elq->get('assets/campaign/'.$campaignId.'/active/validationErrors');
        if($draftErrors != null){
            foreach($draftErrors as $draft){
                if($draft->property != "startAt" && $draft->property != "endAt"){
                    $campaignResults->failed = "There are draft errors on the campaign. Please fix these before using the Boomenator.";
                    $renderInfo['manualCheckForm'] = null;
                    $renderInfo['results'] = $campaignResults;
                    return $this->render('tools/b2cCampaign.html.twig', $renderInfo);
                }
            }
        }

        $campaign = $elq->get('assets/campaign/'.$campaignId.'?depth=complete');
        $campaignName = self::b2cNamingCheckAction($campaign->name, 'CA');

        if($campaignName->type == "PLCP" || $campaignName->type == "CLCP"){
            $campaignType = "LCP";
        }elseif($campaignName->type == "ADHOC"){
            $campaignType = "ADHOC";
        }

        $campaignResults->name = $campaign->name;
        $campaignResults->nameErrors = $campaignName->errors;
        $campaignResults->failed = null;
        $campaignResults->eloqua = "https://secure.p01.eloqua.com/Main.aspx#campaigns&id=".$campaignId;

        if(!isset($campaignType) || $campaignName->country === null){
            $campaignResults->failed = "Campaign name is missing country or campaign type (adhoc/lcp). The tool will not work without those in the naming.";
            $renderInfo['manualCheckForm'] = null;
            $renderInfo['results'] = $campaignResults;
            return $this->render('tools/b2cCampaign.html.twig', $renderInfo);
        }

        //-----------------------------------------------------
        // Validate campaign settings
        //-----------------------------------------------------
        $campaignResults->settings = array();

        //type field
        if($campaign->campaignType != $campaignName->type){
            $campaignResults->settings[] = "Campaign type setting doesn't match campaign naming convention.";
        }

        //region (country) field
        if($campaign->region != $campaignName->country){
            $campaignResults->settings[] = "Campaign region setting doesn't match campaign naming convention.";
        }

        //product (BG) field
        if($campaign->product != $campaignName->bg){
            $campaignResults->settings[] = "Campaign product setting doesn't match campaign naming convention.";
        }

        //custom fields
        foreach($campaign->fieldValues as $campaignField){
            //category field
            if($campaignField->id == 11){
                if($campaignField->value != $campaignName->cat){
                    $campaignResults->settings[] = "Campaign category setting doesn't match campaign naming convention.";
                }
            }

            //sub type field
            if($campaignField->id == 16){
                if(strtolower($campaignField->value) != strtolower($campaignName->subtype)){
                    $campaignResults->settings[] = "Campaign sub type setting doesn't match campaign naming convention.";
                }
            }
        }

        //contacts can enter more than once check, only for PLCP
        if($campaignType == "LCP"){
            if($campaign->isMemberAllowedReEntry == "true"){
                $campaignResults->settings[] = "Contacts are not allowed to enter campaigns more than once for PLCP/CLCP. Please disregard the error for birthday campaigns.";
            }
        }

        //-----------------------------------------------------
        // Campaign steps
        //-----------------------------------------------------

        $campaignResults->segments = array();
        $campaignResults->emails = array();
        $campaignResults->sharedFilter = array();
        $campaignResults->consistency = null;

        $notReceivedEmailRule = false;
        $doiCheck = false;

        $regionMap = $this->getDoctrine()->getRepository('AppBundle:RegionMapping')->findOneBy(array('country' => $campaignName->country));
        if($regionMap === null){
            $campaignResults->failed = "This country's details have not been included in the Boomenator yet.<br>Please send an email to Bram with the following information:<ul><li>Country code (".$campaignName->country.")</li><li>Region (EMEA/APAC/AMER)</li><li>Timezone (for example: (UTC) Casablanca)</li></ul>";
            $renderInfo['manualCheckForm'] = null;
            $renderInfo['results'] = $campaignResults;
            return $this->render('tools/b2cCampaign.html.twig', $renderInfo);
        }
        $region = $regionMap->getRegion();

        $userEmail = $elq->get('system/user/current')->emailAddress;

        if($campaignType == "LCP"){
            $consistency = $this->getDoctrine()->getRepository('AppBundle:ConsistencyInfo')->findOneBy(array('country' => $campaignName->country, 'lcp' => 1));
        }else{
            $consistency = $this->getDoctrine()->getRepository('AppBundle:ConsistencyInfo')->findOneBy(array('country' => $campaignName->country, 'adhoc' => 1));
        }
        $consistencyProgram = 0;
        $consistencyList = false;
        $consistencyAddList = false;
        $consistencyRemoveList = false;
        //Data on received in past # days filters
        $receivedEmailFilters = array(144301 => array('offset' => 24, 'timePeriod' => 'hour'), 141224 => array('offset' => 2, 'timePeriod' => 'day'), 141678 => array('offset' => 3, 'timePeriod' => 'day'), 150999 => array('offset' => 4, 'timePeriod' => 'day'), 151163 => array('offset' => 5, 'timePeriod' => 'day'), 122032 => array('offset' => 6, 'timePeriod' => 'day'));

        if(!isset($campaign->elements)){return new Response("Eloqua isn't returning results. Please try resetting your Eloqua password, and updating it in the tool as well afterwards.");}

        foreach($campaign->elements as $step){
            switch($step->type){
                case "CampaignSegment":
                    $segment = new \stdClass();
                    $segment->settings = array();
                    $segname = $elq->get('assets/contact/segment/'.$step->segmentId)->name;
                    if((strpos(strtolower($segname), 'returnpath') !== false || strpos(strtolower($segname), 'oracle seedlist') !== false || strpos(strtolower($segname), 'marketer') !== false || strpos(strtolower($segname), 'marketeer') !== false || strpos(strtolower($segname), 'proofing') !== false)){
                        if($step->isRecurring == "true"){
                            //proofing/returnpath/etc should always be add once.
                            $segment->settings[] = "Segment is set to 'add members regularly'.";
                        }
                    }elseif($campaignType == "LCP" && $step->isRecurring == "false"){
                        //LCP not adding members regularly
                        $segment->settings[] = "Segment is not set to 'add members regularly'.";
                    }elseif($campaignType == "LCP" && $step->isRecurring == "true" && $step->reEvaluationFrequency == 3600){
                        //LCP recalculation period not set to once a day 
                        $segment->settings[] = "Please make sure that once an hour is needed. If it's not based on consumer input (e.g. after a form submission) it should be set to once a day to make sure we don't have too big of a performance impact.";
                    }elseif($campaignType == "LCP" && $step->isRecurring == "true" && $step->reEvaluationFrequency != 86400){
                        //LCP recalculation period not set to once a day 
                        $segment->settings[] = "Segment re-evaluation frequency isn't set to 1 day.";
                    }elseif($campaignType == "ADHOC" && $step->isRecurring == "true"){
                        //ADHOC adding members regularly
                        $segment->settings[] = "Segment is set to 'add members regularly'.";
                    }  

                    $segment->stepName = $step->name;
                    $segment->name = null;
                    $segment->nameErrors = array();
                    $segment->filters = array();
                    list($segment->name, $segment->nameErrors, $segment->filters, $segment->eloqua, $segment->negativeEngagement, $segment->listCountryExclusion, $segment->trStrikeIron) = self::b2cSegmentCheckAction($request, $step->segmentId, $campaignName->country, ($campaignName->bg.$campaignName->cat));

                    $campaignResults->segments[] = $segment;
                break;
                case "CampaignEmail":
                    $email = new \stdClass();
                    $email->settings = array();
                    $email->stepName = $step->name;
                    $email->name = null;
                    $email->nameErrors = array();
                    $email->errors = array();
                    $email->links = array();

                    //Check mandatory email settings
                    if($step->includeListUnsubscribeHeader != "true"){
                        $email->settings[] = "Email setting 'Include list-unsubscribe header' is not enabled.";
                    }
                    if($step->isAllowingResend != "false"){
                        $email->settings[] = "Email setting 'Allow emails to be re-sent to past recipients' is enabled.";
                    }
                    if(isset($step->isAllowingSentToMasterExclude)){
                        if($step->isAllowingSentToMasterExclude != "false"){
                            $email->settings[] = "Email setting 'Send email to master exclude members' is enabled.";
                        }
                    }else{
                        $email->settings[] = "Cannot validate 'Send email to master exclude members' email setting, please check manually.";
                    }
                    if(isset($step->isAllowingSentToUnsubscribe)){
                        if($step->isAllowingSentToUnsubscribe != "false"){
                            $email->settings[] = "Email setting 'Send email to unsubscribed members' is enabled.";
                        }
                    }else{
                        $email->settings[] = "Cannot validate 'Send email to unsubscribed members' email setting, please check manually.";
                    }

                    //Check scheduling
                    if(isset($step->schedule)){
                        //Validate timezone setting
                        if(!in_array($step->schedule->displayTimeZoneId,$regionMap->getTimezoneIds())){
                            $timezones = implode(", ",$regionMap->getTimezoneNames());
                            $email->settings[] = "The timezone selected for scheduling is incorrect. Please select any of the following timezones: ".$timezones.".";
                        }

                        //Validate scheduling times
                        $startTimes = array(28800,115200,201600,288000,374400,460800,547200); //timestamps for 8am (relative to beginning of the week for Eloqua)
                        $schedulingCorrect = true;

                        $startTimesAdhocDach = array(28800,547200); 
                        $schedulingCorrectAdhocDach = true;

                        $startTimesLcpDach = array(115200,201600,288000,374400,460800);
                        $schedulingCorrectLcpDach = true;

                        if(count($step->schedule->elements) == 7 && $campaign->region != "DE"){
                            for($i=0;$i<7;$i++){
                                //loop through all 7 steps. If one of the steps is incorrect, flag the scheduling as incorrect
                                if($step->schedule->elements[$i]->relativeStart != $startTimes[$i] || !($step->schedule->elements[$i]->duration >= 43140 && $step->schedule->elements[$i]->duration <= 43200)){
                                    //Durations capture settings from 8am to 7:59PM and 8AM to 8PM
                                    $schedulingCorrect = false;
                                }
                            }
                        }elseif(count($step->schedule->elements) == 2 && $campaign->region == "DE" && $campaignType == "ADHOC"){
                            for($i=0;$i<2;$i++){
                                if($step->schedule->elements[$i]->relativeStart != $startTimesAdhocDach[$i] || !($step->schedule->elements[$i]->duration >= 43140 && $step->schedule->elements[$i]->duration <= 43200)){
                                    $schedulingCorrectAdhocDach = false;
                                }
                            }
                        }elseif(count($step->schedule->elements) == 5 && $campaign->region == "DE" && $campaignType == "LCP"){
                            for($i=0;$i<5;$i++){
                                if($step->schedule->elements[$i]->relativeStart != $startTimesLcpDach[$i] || !($step->schedule->elements[$i]->duration >= 43140 && $step->schedule->elements[$i]->duration <= 43200)){
                                    $schedulingCorrectLcpDach = false;
                                }
                            } 
                        }else{
                            $schedulingCorrect = false;
                            $schedulingCorrectAdhocDach = false;
                            $schedulingCorrectLcpDach = false;
                        }
                        if(!$schedulingCorrect && $campaign->region != "DE"){
                            $email->settings[] = "Email scheduling is set incorrectly. Should be from 8AM to 8PM, with all 7 days checked, and the correct timezone selected.";
                        }

                        if(!$schedulingCorrectAdhocDach && $campaign->region == "DE" && $campaignType == "ADHOC"){
                            $email->settings[] = "Email scheduling is set incorrectly. Should be from 8AM to 8PM, with only weekend checked for ADHOC DACH campaigns, and the correct timezone selected.";
                        }

                        if(!$schedulingCorrectLcpDach && $campaign->region == "DE" && $campaignType == "LCP"){
                            $email->settings[] = "Email scheduling is set incorrectly. Should be from 8AM to 8PM, with only weekdays checked for LCP DACH campaigns, and the correct timezone selected.";
                        }
                    }else{
                        $email->settings[] = "Email scheduling isn't set.";
                    }

                    //Check break into smaller batches for ADHOC
                    if($campaignType == "ADHOC" && $step->sendTimePeriod != "sendEmailsOverANumberOfHours"){
                        $email->settings[] = "Break into smaller batches is not enabled.";
                    }elseif($campaignType == "ADHOC" && !($step->sendTimeLength >= 8 && $step->sendTimeLength <= 10)){
                        $email->settings[] = "Break into smaller batches is set incorrectly. Should be set up with an 8~10 hour spread.";
                    }

                    $routingTypes = array('unsubscribe', 'masterExclude', 'bounceback', 'emailAddressError', 'globalExclude', 'emailResent', 'emailSendingError', 'notEligible','noEmailAddress');
                    if($campaignType == "ADHOC" && isset($step->outputTerminals)){
                        foreach($step->outputTerminals as $output){
                            if(in_array($output->terminalType, $routingTypes)){
                                $email->settings[] = "Routing is enabled incorrectly. Should not be enabled for ADHOC campaigns.";
                                break;
                            }
                        }
                    }elseif($campaignType == "LCP" && isset($step->outputTerminals)){
                        $checkArray = array();
                        foreach($step->outputTerminals as $output){
                            $checkArray[] = $output->terminalType;
                        }
                        if(array_intersect($routingTypes, $checkArray) != $routingTypes) {
                            $email->settings[] = "Routing is not set correctly. Should always be enabled for PLCP campaigns.";
                        }
                    }elseif($campaignType == "LCP" && !isset($step->outputTerminals)){
                        $email->settings[] = "Routing is not set correctly. Should always be enabled for PLCP campaigns.";
                    }

                    list($email->name, $email->eloqua, $email->nameErrors, $email->errors, $email->links, $email->images, $email->sizepx, $email->ptags, $email->dcs, $email->dupOmniture, $email->outlookImages, $email->comments, $email->styles, $email->dccTags, $email->bgGradients, $email->brokenFieldMerge, $email->doubleQuotes, $email->doubleStyleTags) = self::emailCheckAction($request, $step->emailId, 'b2c', $region, $userEmail, $campaignName->country);

                    $campaignResults->emails[] = $email;
                break;
                case "CampaignContactFilterMembershipRule":
                    if($step->filterId == 139013){
                        //PLCP double optin check SF 
                        $doiCheck = true;
                        $plcpValid = true;

                        if($step->evaluateNoAfter != 2246400){
                            //26 days evaluation period
                            $campaignResults->sharedFilter[] = "PLCP double opt-in check filter has an incorrect evaluation period. Should be set to 26 days.";
                        }

                        $filter = $elq->get('assets/contact/filter/'.$step->filterId);
                        if(!isset($filter->criteria[0]->type) || $filter->criteria[0]->type != "ContactFieldCriterion" || $filter->criteria[0]->fieldId != 100228 || $filter->criteria[0]->condition->type != "TextSetCondition" || $filter->criteria[0]->condition->optionListId != 11779 || $filter->criteria[0]->condition->operator != "in" || count($filter->criteria) != 1){
                                $plcpValid = false;
                        }
                        if(!$plcpValid){
                                $campaignResults->sharedFilter[] = "PLCP double opt-in check filter has been changed, please urgently raise this with an SME"; 
                        } 
                    }elseif(array_key_exists($step->filterId, $receivedEmailFilters)){
                        //ADHOC # day rule SF
                        $notReceivedEmailRule = true;

                        $receivedEmailFilter = $receivedEmailFilters[$step->filterId];
                        $dayCount = strval($receivedEmailFilter['offset']);
                        if($receivedEmailFilter['offset'] == 24){
                            $dayCount = "1";
                        }elseif($receivedEmailFilter['offset'] == 6){
                            $dayCount = "week";
                        }

                        $filter = $elq->get('assets/contact/filter/'.$step->filterId);

                        if(count($filter->criteria) > 1 || $filter->criteria[0]->type != "EmailSentCriterion" || !isset($filter->criteria[0]->activityRestriction) || $filter->criteria[0]->activityRestriction->type != "ZeroCondition" || !isset($filter->criteria[0]->timeRestriction) || $filter->criteria[0]->timeRestriction->type != "DateValueCondition" || $filter->criteria[0]->timeRestriction->operator != "withinLast" || $filter->criteria[0]->timeRestriction->value->type != "RelativeDate" || $filter->criteria[0]->timeRestriction->value->offset != $receivedEmailFilter["offset"] || $filter->criteria[0]->timeRestriction->value->timePeriod != $receivedEmailFilter["timePeriod"]){
                            $campaignResults->sharedFilter[] = "The \"".$filter->name."\" filter has been edited. Please urgently raise with an SME.";
                        }
                        
                        if($step->evaluateNoAfter != 0){
                            $campaignResults->sharedFilter[] = "Not received email in past ".$dayCount." day filter has an evaluation period set. Should not have any evaluation period";
                        }

                        $campaignResults->sharedFilter[] = "Not received email in past ".$dayCount." day filter was used.";
                    }elseif($step->filterId == 104991){
                        //Old not received email filter
                        $campaignResults->sharedFilter[] = "The old 7 day email sent filter was used. Please use 'Not received email in the last week'.";
                    }elseif(!empty($this->getDoctrine()->getRepository('AppBundle:GlobalSplits')->findOneBy(array('filterId' => $step->filterId, 'type' => 'b2c')))){
                        //Global split filter validation
                        $globalValid = true;
                        $globalSplit = $this->getDoctrine()->getRepository('AppBundle:GlobalSplits')->findOneBy(array('filterId' => $step->filterId, 'type' => 'b2c'));
                        $wildcards = $globalSplit->getWildcards();
                        $doesntEnd = $globalSplit->getDoesNotEndWith();

                        $filter = $elq->get('assets/contact/filter/'.$step->filterId);

                        foreach($filter->criteria as $criteria){
                            if($criteria->type != "ContactFieldCriterion" || !isset($criteria->condition) || $criteria->condition->type != "TextValueCondition" || !isset($criteria->fieldId) || $criteria->fieldId != 100032){
                                $globalValid = false;
                                break;
                            }

                            $regexValue = str_replace('*','\*',str_replace('?','\?',$criteria->condition->value));
                            if($criteria->condition->operator == "matchesWildcardPattern"){
                                if($wildcards == ""){
                                    $globalValid = false;
                                    break;
                                }
                                $pattern = '/(\|\|)?'.$regexValue.'/i';
                                $wildcards = preg_replace($pattern,'',$wildcards);
                            }elseif($criteria->condition->operator == "doesNotEndWith"){
                                if($doesntEnd == ""){
                                    $globalValid = false;
                                    break;
                                }
                                $pattern = '/(\|\|)?'.$regexValue.'/i';
                                $doesntEnd = preg_replace($pattern,'',$doesntEnd);
                            }else{
                                $globalValid = false;
                                break;
                            }
                        }

                        if($wildcards != "" || $doesntEnd != "" || $filter->statement != $globalSplit->getStatement()){
                            $globalValid = false;
                        } 
                        
                        $LastUpdatedName = $filter->updatedBy;

                        if($LastUpdatedName == "179"){
                            $LastUpdatedNotSme = true;
                        }else{
                            $LastUpdatedNotSme = false;
                        }

                        if(!$LastUpdatedNotSme){
                            $campaignResults->sharedFilter[] = "The global split filter <b>\"".$filter->name."\"</b> has been last updated by a non-SME user. Please validate the filter with an SME";
                        }

                        if(!$globalValid){
                            $campaignResults->sharedFilter[] = "The global split filter \"".$filter->name."\" has been edited. Please urgently raise this with an SME";
                        }
                    }else{
                        //Voucher validation check
                        $filter = $elq->get('assets/contact/filter/'.$step->filterId);
                        if(isset($filter->criteria[0]->customObjectId) && $filter->criteria[0]->customObjectId == 464){
                            $campaignResults->settings[] = "Voucher bucket set in shared filter is: <strong>".$filter->criteria[0]->fieldConditions[0]->condition->value."</strong>";
                            if($filter->criteria[0]->type == "LinkedAccountCustomObjectCriterion"){
                                $campaignResults->settings[] = "Voucher check was set to Linked Account in Custom Object instead of Linked Contact in Custom Object";
                            }
                            if($filter->criteria[0]->fieldConditions[0]->condition->operator != "equal" || $filter->criteria[0]->fieldConditions[0]->fieldId != 7050){
                                $campaignResults->settings[] = "Voucher check isn't set to Bucket equals 'bucketname'";
                            }
                        }else{
                            //Gender contact step validation
                            foreach($filter->criteria as $criteriaStep){
                                if($criteriaStep->type == "ContactFieldCriterion"){
                                    if($criteriaStep->fieldId == 100192){
                                        if($criteriaStep->condition->type != "TextValueCondition" || ($criteriaStep->condition->operator != "equal" && $criteriaStep->condition->operator != "notEqual") || ($criteriaStep->condition->value != 1 && $criteriaStep->condition->value != 2)){
                                            $campaignResults->settings[] = "The shared filter \"".$filter->name."\" in step \"".$step->name."\" has Gender field being used incorrectly. Can only be set to Exactly or Not Exactly to Male or Female.";
                                        }
                                    }
                                }
                            }
                        }
                    }
                break;
                case "CampaignCloudAction":
                    //Form submit step validation 
                    $cloudActionError = false;
                 
                    if(strpos(strtolower($step->name), 'form') !== false) { 
                        if(isset($step->outputTerminals)){
                            foreach($step->outputTerminals as $output){
                                if($output->terminalType == "error") {
                                        $cloudActionError = true;
                                }  
                            }  
                        } 
                        if(!$cloudActionError) {
                            $campaignResults->settings[] = "Routing is not enabled for the form submit step.";
                        } 
                    }
                break;
                case "CampaignContactListMembershipRule":
                    if($consistency !== null){
                        if($step->listId == $consistency->getSharedList()){
                            $consistencyList = true;
                        }
                    }
                break;
                case "CanvasAddToProgramAction":
                    if($consistency !== null){
                        if($step->programId == $consistency->getProgram()){
                            $consistencyProgram++;
                        }
                    }
                break;
                case "CampaignAddToContactListAction":
                    if($consistency !== null){
                        $consistencyAddList = true;
                    }
                break;
                case "CampaignRemoveFromContactListAction":
                    if($consistency !== null){
                        $consistencyRemoveList = true;
                    }
                break;
            }
        }
        //Set error messages for missing filters
        if($campaignType == "LCP" && !$doiCheck){
            $campaignResults->sharedFilter[] = "'PLCP double opt-in check' filter is missing from the campaign.";
        }
        $excludedFromEmailFilter = array('DE', 'AT', 'CH', 'US', 'CA');
        $excludeEMEALocales = array('BE', 'BG', 'CZ', 'DK', 'EE', 'ES', 'FI', 'FR', 'GB', 'GR', 'HR', 'HU', 'IE', 'IL', 'IR', 'IT', 'LT', 'LV', 'NO', 'PL', 'PT', 'RO', 'RW', 'SA', 'SE', 'SI', 'SK', 'TR', 'UA', 'ZA', 'EG', 'KE', 'MA', 'GH', 'AE', 'KW', 'BH', 'JO', 'LB', 'OM', 'RS', 'NG');

        if($campaignType == "ADHOC" && !$notReceivedEmailRule && in_array($campaign->region, $excludeEMEALocales)){
            $campaignResults->sharedFilter[] = "'Not received email last 3 days' filter is missing from the campaign.";
        }

        if($campaignType == "ADHOC" && !$notReceivedEmailRule && !in_array($campaign->region, $excludedFromEmailFilter) && !in_array($campaign->region, $excludeEMEALocales)){
            $campaignResults->sharedFilter[] = "'Not received email last week' filter is missing from the campaign.";
        }elseif($campaignType == "ADHOC" && in_array($campaign->region, $excludedFromEmailFilter)){
            if($notReceivedEmailRule){
                $campaignResults->sharedFilter[] = "'Not received email last week' filter was added to the campaign. Should not be added for DE, AT, CH, US and CA.";
            }else{
                $campaignResults->sharedFilter[] = "'Not received email last week' filter was correctly omitted from the campaign.";
            }
        }

        if($consistency !== null && $campaignType == "LCP" && (!$consistencyList || $consistencyProgram != 2 || !$consistencyAddList || !$consistencyRemoveList)){
            $campaignResults->consistency = 'The required steps for an LCP in a country where OnePH is running are missing.';
        }elseif($consistency !== null && $campaignType != "LCP" && ($consistencyProgram != 2 || !$consistencyAddList || !$consistencyRemoveList)){
            $campaignResults->consistency = 'The required steps for an ADHOC in a country where OnePH is running are missing.';
        }  

        $renderInfo['results'] = $campaignResults;

        $manualCheckResults = [];
        $manualChecks = array(
            "Segmentation is as briefed" => "1",
            "Marketeer segment added" => "2",
            "Campaign start date is correct" => "3",
            "Campaign end date is correct" => "4",
            "If a voucher is used; shared filter was added to check for assigned voucher" => "5",
            "Checked if all links work" => "6",
            "Link tracking is enabled for all links (settings > manage links)" => "7",
            "Buttons have correct width set for Outlook" => "8",
            "Emails are responsive/look good on mobile" => "9",
            "Tags that users in DCC use to indicate dynamic content replaced with actual dynamic content [e.g. first name]" => "10",
            "Emails checked via Litmus. Check the major email clients (Outook desktop 15, Android, iOS, Gmail)" => "11",
            "If changes were made on code level (including images); brand approved and checked against all clients in Litmus" => "12",
            "If your segment targets consumers past 12 month engagement, please make sure StrikeIron validation is added to the campaign" => "13",
            "If campaign is part of a OnePH locale, shared list was added to the corresponding OnePH program by the SME" => "14"
        );

        if($campaignName->sector == "HC"){
            $healthManualChecks = array(
                "If part of the campaign is gated, shared filter to check for HIPAA email group subscription is in place" => "15",
                "For gated email: Check if correct footer is used, should contain mailto: link to privacy" => "16",
                "For gated email: check if the url's are working" => "16"
            );
            $manualChecks = array_merge($manualChecks,$healthManualChecks);
        }
        if($campaignName->country == "DE" || $campaignName->country == "AT" || $campaignName->country == "CH" || $campaignName->country == "US"){
            $countryManualChecks = array(
                "Email(s) approved by requester" => "17"
            );
            $manualChecks = array_merge($manualChecks,$countryManualChecks);
        }
        if($campaignName->bg == "HW" && $campaignName->cat == "MCC"){
            $mccManualChecks = array(
                "Please make sure the segment for your MCC campaign is approved by the respective SME" => "18"
            );
            $manualChecks = array_merge($manualChecks,$mccManualChecks);
        }

        $manualCheckForm = $this->get('form.factory')->createNamedBuilder('ManualCheck', 'Symfony\Component\Form\Extension\Core\Type\FormType', $manualCheckResults)
            ->add('manualChecks', ChoiceType::class, array('choices' => $manualChecks, 'expanded' => true, 'required' => true, 'multiple' => true))
            ->getForm();
        $manualCheckForm->handleRequest($request);
        $renderInfo['manualCheckForm'] = $manualCheckForm->createView();

        return $this->render('tools\b2cCampaign.html.twig', $renderInfo);
    }

    /**
     * @param Request $request
     * @param int $segmentId
     * @param string $country
     * @param string $bgcat
     * @return array
     */
    public function b2cSegmentCheckAction(Request $request, $segmentId, $country, $bgcat)
    {
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));
        $segment = $elq->get('assets/contact/segment/'.$segmentId);

        $seedlist = false;

        $segmentResults = new \stdClass();
        if (strpos(strtolower($segment->name), 'returnpath') === false && strpos(strtolower($segment->name), 'oracle seedlist') === false && strpos(strtolower($segment->name), 'marketer') === false && strpos(strtolower($segment->name), 'marketeer') === false && strpos(strtolower($segment->name), 'proofing') === false){
            $segmentResults->nameErrors = self::b2cNamingCheckAction($segment->name, 'SE')->errors;
        } else {
            $segmentResults->nameErrors = array();
            $seedlist = true;
        }

        $segmentResults->eloqua = "https://secure.p01.eloqua.com/Main.aspx#segments&id=".$segmentId;

        $segmentResults->filters = array();

        $exclusionFilterIds = array();

        $anyExclusionFilter = false;

        $negativeEngagement = null;

        $negativeSharedList = false;

        $negativeSharedListIncluded = false;

        $hasList = false;

        $listExclusionFilterSet = false;

        $trStrikeIron = false;

        //check if there is a shared list
        foreach($segment->elements as $segmentFilter){
            if(isset($segmentFilter->list) && $segmentFilter->isIncluded == "true"){
               
                if($segmentFilter->list->id == 10226 || $segmentFilter->list->id == 10191){
                    $hasList = false;
                } else {
                    $hasList = true;
                }
            }
            if(isset($segmentFilter->list)){
                //6m = 168092
                //12m = 150116
                if(($segmentFilter->list->id == 10226 || $segmentFilter->list->id == 10191) && $segmentFilter->isIncluded == "false"){
                    $negativeSharedList = true;
                }
                
                if(($segmentFilter->list->id == 10226 || $segmentFilter->list->id == 10191) && $segmentFilter->isIncluded == "true"){
                    $negativeSharedListIncluded = true;
                }

            }
        }
        
       foreach($segment->elements as $segmentFilter){
            $validation = array();

            if(isset($segmentFilter->filter)){
                if($segmentFilter->isIncluded == "true" && !$seedlist){
                    //Check for country & email group subscription

                    $countrySet = false;
                    $emailGroupSet = false;

                    foreach($segmentFilter->filter->criteria as $step){
                        switch($step->type){
                            case "ContactFieldCriterion":
                                if($step->fieldId == 100012){
                                    //Validate country field
                                    $countrySet = true;
                                    if($step->condition->type != "TextValueCondition" || $step->condition->value != $country || $step->condition->operator != "equal"){
                                        $validation[] = "Country field is set incorrectly. Should be set to is exactly ".$country.".";
                                    }
                                }
                            break;
                            case "SubscriptionCriterion":
                                if(isset($step->emailGroupIds)){
                                    $emailGroupSet = true;
                                    if(isset($step->timeRestriction)){
                                        //Validate Subscribed to Groups step. No timerestriction means "as of now".
                                        $validation[] = "Subscribed to Groups step incorrectly configured. Should be set to: as of now.";
                                    }
                                    if(count($step->emailGroupIds)>1){
                                        //More than 1 email group was set.
                                        $validation[] = "Subscribed to Groups step incorrectly configured. Multiple email groups have been added.";
                                    }else{
                                        if($bgcat == "HWMCC"){
                                            if($step->emailGroupIds[0] != 475){
                                                //Email group HW - MCC should have been used
                                                $validation[] = "Subscribed to Groups step incorrectly configured. Email group 'HW - MCC' has to be used.";
                                            }
                                        }else{
                                            if($step->emailGroupIds[0] != 474){
                                                //Email group General Newsletter should have been used
                                                $validation[] = "Subscribed to Groups step incorrectly configured. Email group 'General Newsletter' has to be used.";
                                            }
                                        }
                                    }
                                }
                            break;
                            case "LinkedAccountCustomObjectCriterion":
                                $validation[] = "Has Linked Account in Custom Object was used. Please make sure to use Has linked Contact in Custom Object.";
                            break;
                        }
                    }
                    //Set field missing messages
                    if(!$countrySet){ $validation[] = "Country field is missing."; }
                    if(!$emailGroupSet){ $validation[] = "Subscribed to Groups field is missing."; }
                }
                if($segmentFilter->isIncluded == "false" && !$seedlist){
                    //Check for negative engagement criteria

                    //Set all fields unavailable
                    $countrySet = false;
                    $sharedListCountryExclusion = false;
                    $doiSet = false;
                    $optoutSet = false;
                    $unsubscribedSet = false;
                    $hardbounceSet = false;
                    $emailOpenSet = false;
                    $emailClickSet = false;
                    $formSubmitSet = false;
                    $visitedLandingpageSet = false;
                    $visitedWebsiteSet = false;
                    //Prepare step ID variables
                    $baseStepIds = array();
                    $activityStepIds = array();
                    //Prepare activity timeperiods array
                    $timePeriods = array();
                    $strikeIronSet = false;

                    $stepChanged = false;

                    $anyExclusionFilter = true;

                    $stepCount = count($segmentFilter->filter->criteria);

                    foreach($segmentFilter->filter->criteria as $step){
                        switch($step->type){
                            case "ContactFieldCriterion":
                                if($step->fieldId == 100012){
                                    //Validate country field
                                    $countrySet = true;
                                    if($stepCount == 1 && ($step->condition->type != "TextValueCondition" || $step->condition->value != $country || $step->condition->operator != "notEqual")){
                                        $validation[] = "Country field is set incorrectly. Should be set to is not exactly ".$country.".";
                                    }
                                    if($stepCount > 1 && ($step->condition->type != "TextValueCondition" || $step->condition->value != $country || $step->condition->operator != "equal")){
                                        $validation[] = "Country field is set incorrectly. Should be set to is exactly ".$country.".";
                                    }
                                    //check if the field for shared list country exclusion is there
                                    if($step->condition->type == "TextValueCondition" && $step->condition->value == $country && $step->condition->operator == "notEqual"){
                                        $sharedListCountryExclusion = true;
                                    }
                                }elseif($step->fieldId == 100228){
                                    //Validate double optin field
                                    $doiSet = true;
                                    $baseStepIds[] = $step->id;
                                    if($step->condition->type != "TextSetCondition" || $step->condition->optionListId != 11779 || $step->condition->operator != "notIn"){
                                        //Not checking against picklist, checking the wrong picklist, or using the wrong operator
                                        $stepChanged = true;
                                    }
                                }elseif($step->fieldId == 100250){
                                    //Validate OPTED_OUT_TIMESTAMP field
                                    $optoutSet = true;
                                    $activityStepIds[] = $step->id;
                                    if($step->condition->type != "DateValueCondition" || $step->condition->operator != "notWithinLast" || $step->condition->value->timePeriod != "month" || ($step->condition->value->offset != 6 && $step->condition->value->offset != 12)){
                                        $stepChanged = true;
                                    }
                                    $timePeriods[] = $step->condition->value->offset;
                                }elseif($step->fieldId == 100341){
                                    //Strikeiron validation
                                    if($step->condition->type == "TextValueCondition" && $step->condition->operator == "equal" && $step->condition->value == 2){
                                        $strikeIronSet = true;
                                    }
                                }
                            break;
                            case "UnsubscriptionCriterion":
                                if(!isset($step->emailGroupIds)){
                                    $unsubscribedSet = true;
                                    $baseStepIds[] = $step->id;
                                    if(isset($step->timeRestriction)){
                                        //Validate globally unsubscribed step. No timerestriction means "as of now".
                                        $stepChanged = true;
                                    }
                                }
                            break;
                            case "HardBouncebackCriterion":
                                if($step->hasStatus == "True"){
                                    $hardbounceSet = true;
                                    $baseStepIds[] = $step->id;
                                    if(isset($step->timeRestriction)){
                                        //Validate hardbounced step. No timerestriction means "as of now".
                                        $stepChanged = true;
                                    }
                                }
                            break;
                            case "EmailOpenCriterion":
                                $emailOpenSet = true;
                                if ($step->activityRestriction->type == "ZeroCondition") {
                                    $activityStepIds[] = $step->id;
                                    if(!self::validateActivityFieldAction($step)){
                                        $stepChanged = true;
                                    }
                                    $timePeriods[] = $step->timeRestriction->value->offset;
                                }else{
                                    $stepChanged = true;
                                }
                            break;
                            case "EmailClickThroughCriterion":
                                $emailClickSet = true;
                                if ($step->activityRestriction->type == "ZeroCondition") {
                                    $activityStepIds[] = $step->id;
                                    if(!self::validateActivityFieldAction($step)){
                                        $stepChanged = true;
                                    }
                                    $timePeriods[] = $step->timeRestriction->value->offset;
                                }else{
                                    $stepChanged = true;
                                }
                            break;
                            case "FormSubmitCriterion":
                                $formSubmitSet = true;
                                if ($step->activityRestriction->type == "ZeroCondition") {
                                    $activityStepIds[] = $step->id;
                                    if(!self::validateActivityFieldAction($step)){
                                        $stepChanged = true;
                                    }
                                    $timePeriods[] = $step->timeRestriction->value->offset;
                                }else{
                                    $stepChanged = true;
                                }
                            break;
                            case "VisitedLandingPageCriterion":
                                $visitedLandingpageSet = true;
                                if ($step->activityRestriction->type == "ZeroCondition") {
                                    $activityStepIds[] = $step->id;
                                    if(!self::validateActivityFieldAction($step)){
                                        $stepChanged = true;
                                    }
                                    $timePeriods[] = $step->timeRestriction->value->offset;
                                }else{
                                    $stepChanged = true;
                                }
                            break;
                            case "VisitedWebsiteCriterion":
                                $visitedWebsiteSet = true;
                                if ($step->activityRestriction->type == "ZeroCondition") {
                                    $activityStepIds[] = $step->id;
                                    if(!self::validateActivityFieldAction($step) || $step->VisitedWebsiteType != "VisitedPage" || count($step->trackedUrlIds) > 0){
                                        $stepChanged = true;
                                    }
                                    $timePeriods[] = $step->timeRestriction->value->offset;
                                }else{
                                    $stepChanged = true;
                                }
                            break;
                            case "LinkedAccountCustomObjectCriterion":
                                $validation[] = "Has Linked Account in Custom Object was used. Please make sure to use Has linked Contact in Custom Object.";
                            break;
                        }
                    }

                    if($country == "TR" && $countrySet == true && $strikeIronSet == true && strpos($segmentFilter->filter->statement, "OR") === false && count($segmentFilter->filter->criteria) == 2){
                        $trStrikeIron = true;
                    }

                    if(count($segmentFilter->filter->criteria) == 1 && $sharedListCountryExclusion){
                        $listExclusionFilterSet = true;
                    }
                }

                //Gender steps validation
                $genderIncorrect = false;
                foreach($segmentFilter->filter->criteria as $step){
                    if($step->type == "ContactFieldCriterion"){
                        if($step->fieldId == 100192){
                            if($step->condition->type != "TextValueCondition" || ($step->condition->operator != "equal" && $step->condition->operator != "notEqual") || ($step->condition->value != 1 && $step->condition->value != 2)){
                                $genderIncorrect = true;
                            }
                        }
                    }
                }
                if($genderIncorrect){ $validation[] = "Gender field is used incorrectly. Can only be set to Exactly or Not Exactly to Male or Female."; }

                //old product CDO validation
                $oldCdo = false;
                foreach($segmentFilter->filter->criteria as $step){
                    if($step->type == "LinkedCustomObjectCriterion"){
                        if($step->customObjectId == 350 || $step->customObjectId == 349){
                            $oldCdo = true;
                        }
                    }
                }
                if($oldCdo){ $validation[] = "One of the old Registered Product CDO's was used. Please only use <b>Registered Products CDO 2015 to 2017</b>."; }

                //old CIR CDO validation
                $oldCirCdo = false;
                foreach($segmentFilter->filter->criteria as $step){
                    if($step->type == "LinkedCustomObjectCriterion"){
                        if($step->customObjectId == 352){
                            $oldCirCdo = true;
                        }
                    }
                }
                if($oldCirCdo){ $validation[] = "The deprecated Consumer Integrations RCDH CDO was used, please update your segment with the new <b>Consumer Integrations RCDH 2020</b> CDO."; }

            }

            //Add filter information to output
            $filter = new \stdClass();
            if(isset($segmentFilter->filter)){
                $filter->name = $segmentFilter->filter->name;
            }elseif(isset($segmentFilter->list)){
                $filter->name = $segmentFilter->list->name;
            }
            $filter->validation = $validation;
            $segmentResults->filters[$segmentFilter->id] = $filter;
        }

        $trStrikeIronResult = null;

        if($country == "TR" && $trStrikeIron == false && !$seedlist){
            $trStrikeIronResult = "Strike Iron exclusion has not been added, or is set up incorrect. Should be Country is Exactly Turkey AND StrikeIronEmailValidation is Exactly Invalid.";
        }
        
        $negativeEngagementResult = null;

        if(!$negativeSharedList && !$seedlist){
            $negativeEngagementResult = "The negative engagement exclusion list is missing from the segment.";
        }

        if($negativeSharedListIncluded && !$seedlist){
            $negativeEngagementResult = "The negative engagement exclusion list is added as as inclusion. Should be set as an exclusion.";
        }

        $listCountryExclusion = null;

        if($hasList && !$listExclusionFilterSet & !$seedlist){
            $listCountryExclusion = "Segment has a shared list, but it's missing a proper exclusion for other countries. Should be an excluded filter with only: Country not equal to the country of this campaign.";
        }

        return array($segment->name, $segmentResults->nameErrors, $segmentResults->filters, $segmentResults->eloqua, $negativeEngagementResult, $listCountryExclusion, $trStrikeIronResult);
    }

    /**
     * @param \stdClass $step
     * @return bool
     */
    public function validateActivityFieldAction($step)
    {
        if($step->timeRestriction->type != "DateValueCondition" || $step->timeRestriction->operator != "withinLast" || $step->timeRestriction->value->timePeriod != "month" || ($step->timeRestriction->value->offset != 6 && $step->timeRestriction->value->offset != 12)){
            return false;
        }else{
            return true;
        }
    }

    /**
     * @param string $statementString
     * @param string $countryStep
     * @param array $baseSteps
     * @param array $activitySteps
     * @return bool
     */
    public function validateFilterOrder(string $statementString, array $baseSteps, array $activitySteps){
        $countryValid = false;
        //get nested array based on statement string from Eloqua
        $statement = self::getStatementArray($statementString);
        if($statement['logic'] == 'or'){
            foreach($statement['values'] as $key => $val){
                if(!is_array($val) && in_array($val, $baseSteps)){
                    $baseSteps = array_diff($baseSteps, [$val]);
                }else{
                    if(!is_array($val)){
                        return false;
                    }
                    if($val['logic'] != 'and'){
                        return false;
                    }
                    foreach($val['values'] as $key1 => $val1){
                        if(!is_array($val1) && in_array($val1, $activitySteps)){
                            $activitySteps = array_diff($activitySteps, [$val1]);
                        }
                    }
                }
            }
            if(count($baseSteps) == 0 && count($activitySteps) == 0){
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $statement
     * @return array
     */
    public function getStatementArray($statement){
        $newStatement = array();
        $newStatement['def'] = $statement;
        $newStatement['values'] = array();

        preg_match_all('/\(([^()]|(?R))*\)/', $statement, $nestedGroups);

        foreach($nestedGroups[0] as $nested){
            //remove brackets
            $nested = substr($nested, 1, -1);
            //get values for nested group
            array_push($newStatement['values'], self::getStatementArray($nested));
            //remove group from parent statement
            $statement = str_replace($nested, '', $statement);
        }
        //Get logic for this level AND-only, OR-only, both
        if(strpos($statement, 'AND') !== false && strpos($statement, 'OR') !== false){
            $newStatement['logic'] = 'both';
        }elseif(strpos($statement, 'AND') !== false){
            $newStatement['logic'] = 'and';
        }else{
            $newStatement['logic'] = 'or';
        }

        //Add non-nested values
        preg_match_all('/(\d+)/', $statement, $values);
        $newStatement['values'] = array_merge($newStatement['values'], $values[0]);

        return $newStatement;
    }

    /**
     * @param string $name
     * @param string $assetType
     * @return \stdClass
     */
    public function b2cNamingCheckAction($name, $assetType, $external = null)
    {    
        $sectors = ['CL','LI','HC','CRS','GBL','TEST'];
        $countries = ['MULTI','WW','TEST','AF','AX','AL','DZ','AS','AD','AO','AI','AQ','AG','AR','AM','AW','AU','AT','AZ','BS','BH','BD','BB','BY','BE','BZ','BJ','BM','BT','BO','BA','BW','BV','BR','IO','BN','BG','BF','BI','KH','CM','CA','CV','KY','CF','TD','CL','CN','CX','CC','CO','KM','CG','CD','CK','CR','CI','HR','CU','CY','CZ','DK','DJ','DM','DO','EC','EG','SV','GQ','ER','EE','ET','FK','FO','FJ','FI','FR','GF','PF','TF','GA','GM','GE','DE','GH','GI','GR','GL','GD','GP','GU','GT','GN','GW','GY','HT','HM','HN','HK','HU','IS','IN','ID','IR','IQ','IE','IL','IT','JM','JP','JO','KZ','KE','KI','KP','KR','KW','KG','LA','LV','LB','LS','LR','LY','LI','LT','LU','MO','MK','MG','MW','MY','MV','ML','MT','MH','MQ','MR','MU','YT','MX','FM','MD','MC','MN','MS','MA','MZ','MM','MP','NA','NR','NP','NL','AN','NC','NZ','NI','NE','NG','NU','NF','NO','OM','PK','PW','PS','PA','PG','PY','PE','PH','PN','PL','PT','PR','QA','RE','RO','RS','RU','RW','KN','LC','WS','SM','ST','SA','SN','CS','SC','SL','SG','SK','SI','SB','SO','ZA','GS','ES','LK','SH','PM','VC','SD','SR','SJ','SZ','SE','CH','SY','TW','TJ','TZ','TH','TL','TG','TK','TO','TT','TN','TR','TM','TC','TV','UG','UA','AE','GB','US','UM','UY','UZ','VU','VA','VE','VN','VG','VI','WF','EH','YE','ZM','ZW'];
        $bgs = ['CRB','AUT','CLU','CO','DA','GI','HW','PC','PH','PHS','TPV','SRC'];
        $categories = ['CRC','AIR','AL','AM','AMM','BTY','CLU','DL','ES','FC','GC','HC','HP','KA','MCC','MG','OHC','PM','RGC','TV','SLT'];
        $assets = ['CA','EM','LP','FO','SE','EG','ET','DC','IM','RE','SF','SL'];
        $campaignTypes =  ['ADHOC','PLCP','CLCP'];
        $campaignSubTypes = ['acquisition','ambassador','birthday','conversion','cross','cross sell','inactive','multitouch','nurture','r&r','service','survey','test drivers','upsell','welcome'];
        $bgCatMatches = array("CO" => array("ES","RGC"), "DA" => array("AIR","FC","GC","KA"), "HW" => array("MCC","OHC","PM"), "PC" => array("BTY","MG"), "PHS" => array("DL"), "CLU" => array("CLU"), "AUT" => array("AL","AM"), "SRC" => array("SLT"), "GI" => array("AMM","HP"), "TPV" => array("TV"));
        
        $results = new \stdClass();
        $results->errors = array();
        $results->sector = null;
        $results->country = null;
        $results->bg = null;
        $results->cat = null;
        $results->type = null;
        $results->subtype = null;

        if(preg_match('/(\s\s+)/', $name)){
            $results->errors[] = "The name contains a double space, please fix this as kayako will make a period out of the double space.";
        }
        
        preg_match('/^(?P<sector>CL|LI|HC|CRS|GBL|TEST)?\s*(?P<country>[A-Z]{2}|MULTI|TEST)?\s*(?P<date>\d{8})?\s*(?P<bgcat>[A-Z]{4,6})?\s*(?P<asset>[A-Z]{2})?\s*(?P<type>[A-Z]{4,5})?\s*(?P<subtype>(Cross sell|Test drivers|[A-Za-z&]+))?\s*(?P<name>.*?)?\s*(?P<ticket>[DCC]-\d{3}-\d{5})?$/', $name, $parts);

        if(array_key_exists('sector',$parts)){
            if(!in_array($parts['sector'], $sectors)){
                $results->errors[] = 'Sector is incorrect.';
            }
            $results->sector = $parts['sector'];
        }else{
            $results->errors[] = 'Sector is missing.';
        }

        if(array_key_exists('country',$parts)){
            if(!in_array($parts['country'], $countries)){
                $results->errors[] = 'Country is incorrect.';
            }else{
                $results->country = $parts['country'];
            }
        }else{
            $results->errors[] = 'Country is missing.';
        }

        if(array_key_exists('date',$parts)){
            if(!in_array($parts['date'], $countries)){
                if( strlen($parts['date']) == 8 ){  
                    $dateError = 0;
                    $year = substr($parts['date'], 0, 4);
                    $month = substr($parts['date'], 4, 2);
                    $day = substr($parts['date'], 6);
                    if($year > date("Y") || $month > 12 || $day > 31){
                        $results->errors[] = 'Date is incorrect.';
                    }
                }else{
                    $results->errors[] = 'Date is incomplete.';
                }
            }
        }else{
            $results->errors[] = 'Date is missing.';
        }

        if(array_key_exists('bgcat',$parts)){
            $bg = null;
            $cat = null;
            foreach($bgs as $businessGroup){
                if(substr($parts['bgcat'], 0, 2) == $businessGroup || substr($parts['bgcat'], 0, 3) == $businessGroup){
                    $bg = $businessGroup;
                    $cat = str_replace($bg, '', $parts['bgcat']);
                    break;
                }
            }
            if($bg === null){
                $results->errors[] = 'Business group is missing or incorrect.';
            }
            $results->bg = $bg;

            if($cat !== null){
                if(!in_array($cat, $categories)){
                    $results->errors[] = 'Category is incorrect.';
                    $cat = null;
                }
            }else{
                $results->errors[] = 'Category is missing.';
            }
            $results->cat = $cat;

            if($bg !== null && $cat !== null && $bg != "CRB"){
                if(!in_array($cat, $bgCatMatches[$bg]) && $cat != "CRC"){
                    $results->errors[] = 'The category used is not part of the BG used.';
                }
            }
        }else{
            $results->errors[] = 'BG and Category are missing.';
        }

        if(array_key_exists('asset',$parts)){
            if(!in_array($parts['asset'], $assets) || $parts['asset'] != $assetType){
                $results->errors[] = 'Asset is incorrect.';
            }
        }else{
            $results->errors[] = 'Asset is missing.';
        }

        if(array_key_exists('type',$parts)){
            if(!in_array($parts['type'], $campaignTypes)){
                $results->errors[] = 'Campaign type is incorrect.';
            }else{
                $results->type = $parts['type'];
            }
        }else{
            $results->errors[] = 'Campaign type is missing.';
        }

        if(array_key_exists('subtype',$parts)){
            if(!in_array(strtolower($parts['subtype']), $campaignSubTypes)){
                $results->errors[] = 'Campaign subtype is incorrect.';
            }else{
                $results->subtype = $parts['subtype'];
            }
        }else{
            $results->errors[] = 'Campaign subtype is missing.';
        }

        if(!array_key_exists('name',$parts)){
            $results->errors[] = "Name is missing";
        }

        if(!array_key_exists('ticket',$parts)){
            $results->errors[] = "Ticket is missing";
        }

        if($external){
            return new JsonResponse($results);
        }else{
            return $results;
        }
    }

    //======================================================================
    // B2B CAMPAIGN VALIDATION
    //======================================================================

    public function b2bAction(Request $request, $campaignId, $renderInfo)
    {
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));

        $campaignResults = new \stdClass();

        $draftErrors = $elq->get('assets/campaign/'.$campaignId.'/active/validationErrors');
        if($draftErrors != null){
            foreach($draftErrors as $draft){
                if($draft->property != "startAt" && $draft->property != "endAt"){
                    $campaignResults->failed = "There are draft errors on the campaign. Please fix these before using the Boomenator.";
                    $renderInfo['manualCheckForm'] = null;
                    $renderInfo['results'] = $campaignResults;
                    return $this->render('tools/b2bCampaign.html.twig', $renderInfo);
                }
            }
        }

        $campaign = $elq->get('assets/campaign/'.$campaignId.'?depth=complete');
        $campaignName = self::b2bNamingCheckAction($campaign->name, 'campaign');

        if($campaignName->campaigncategory == 'AO'){
            $lcp = true;
        }elseif($campaignName->campaigncategory == 'AH'){
            $lcp = false;
        }

        $campaignResults->name = $campaign->name;
        $campaignResults->nameErrors = $campaignName->errors;
        $campaignResults->failed = null;
        $campaignResults->eloqua = "https://secure.p01.eloqua.com/Main.aspx#campaigns&id=".$campaignId;

        if(!isset($lcp)){
            $campaignResults->failed = "The campaign category (AO/AH) part is missing from the naming. Without this value the tool cannot correctly validate the campaign.";
            $renderInfo['manualCheckForm'] = null;
            $renderInfo['results'] = $campaignResults;
            return $this->render('tools/b2bCampaign.html.twig', $renderInfo);
        }

        //-----------------------------------------------------
        // Validate campaign settings
        //-----------------------------------------------------
        $campaignResults->settings = array();

        //contacts can enter more than once check
        if($campaign->isMemberAllowedReEntry == "true"){
            $campaignResults->settings[] = "Contacts are allowed to enter more than once in the campaign settings.";
        }
         //SFDC campaign ID
         if($campaign->crmId == ""){
            $campaignResults->settings[] = "CRM Campaign ID must be filled with the SFDC Campaign ID";
        }

        $campaignTypeArray = array('E-Mailshot' => 'E-mailshot', 'eNewsletter' => 'eNewsletter', 'Registration campaign' => 'Registration', 'Event campaign' => 'Event', 'Welcome campaign' => 'Welcome', 'Awareness campaign' => 'Awareness', 'Re-engagement campaign' => 'Reengagement', 'Nurture campaign' => 'Nurture', 'Social' => 'Social', 'Other' => 'Other');

        //campaign type field
        if($campaignName->campaigntype != $campaignTypeArray[$campaign->campaignType]){
            $campaignResults->settings[] = "Campaign type setting doesn't match campaign naming convention.";
        }

        //region field
        if(($campaign->region != "WW(Global)" && $campaign->region != $campaignName->country) || ($campaign->region == "WW(Global)" && $campaignName->country != "WW")){
            $campaignResults->settings[] = "Campaign region setting doesn't match campaign naming convention.";
        }

        //custom fields
        foreach($campaign->fieldValues as $campaignField){
            //BG field
            if($campaignField->id == 9){
                if(($campaignField->value == "S&RC" && $campaignName->bg != "SRC") || ($campaignField->value == "MA" && $campaignName->bg != "MA") || ($campaignField->value == "TC" && $campaignName->bg != "TC") || ($campaignField->value == "HSDP, HTS and EM" && $campaignName->bg != "HTS") || ($campaignField->value == "Cross-BG" && $campaignName->bg != "CBG") || ($campaignField->value != "S&RC" && $campaignField->value != "MA" && $campaignField->value != "TC" && $campaignField->value != "HSDP, HTS and EM" && $campaignField->value != "Cross-BG" && $campaignField->value != $campaignName->bg)){
                    $campaignResults->settings[] = "Campaign BG setting doesn't match campaign naming convention.";
                }
            }

            //BU field
            if($campaignField->id == 11){
                if(strpos($campaignField->value, '_') === false){
                    $campaignResults->settings[] = "Campaign BU setting uses one of the old values. Please only use the values with an underscore '_'.";
                }
            }
        }

        //-----------------------------------------------------
        // Campaign steps
        //-----------------------------------------------------

        $campaignResults->segments = array();
        $campaignResults->emails = array();
        $campaignResults->sharedFilter = array();

        $fiveDayRule = false;

        $regionMap = $this->getDoctrine()->getRepository('AppBundle:RegionMapping')->findOneBy(array('type' => 'B2B'));
        $region = $regionMap->getRegion();
        $userEmail = $elq->get('system/user/current')->emailAddress;

        if(!isset($campaign->elements)){return new Response("Eloqua isn't returning results. Please try resetting your Eloqua password, and updating it in the tool as well afterwards.");}
        
        foreach($campaign->elements as $step){
            switch($step->type){
                case "CampaignSegment":
                    $segment = new \stdClass();
                    $segment->settings = array();
                    if($step->name != "Broadcasting - Do not delete"){
                        if($step->isRecurring == "true" && !$lcp){
                            //adding members regularly
                            $segment->settings[] = "Segment is set to 'add members regularly'.";
                        }elseif($step->isRecurring == "false" && $lcp){
                            //adding members regularly
                            $segment->settings[] = "Segment is set to 'add members once'.";                        
                        }
                    }else{
                        if($step->isRecurring == "true"){
                            //adding members regularly
                            $segment->settings[] = "Segment is set to 'add members regularly'.";
                        }
                    }

                    $segment->stepName = $step->name;
                    $segment->name = null;
                    $segment->nameErrors = array();
                    $segment->filters = array();
                    list($segment->name, $segment->nameErrors, $segment->errors, $segment->eloqua) = self::b2bSegmentCheckAction($request, $step->segmentId);

                    $campaignResults->segments[] = $segment;
                break;
                case "CampaignEmail":
                    $email = new \stdClass();
                    $email->settings = array();
                    $email->stepName = $step->name;
                    $email->name = null;
                    $email->nameErrors = array();
                    $email->errors = array();
                    $email->links = array();

                    //Check mandatory email settings
                    if($step->includeListUnsubscribeHeader != "true"){
                        $email->settings[] = "Email setting 'Include list-unsubscribe header' is not enabled.";
                    }
                    if($step->isAllowingResend != "false"){
                        $email->settings[] = "Email setting 'Allow emails to be re-sent to past recipients' is enabled.";
                    }
                    if(isset($step->isAllowingSentToMasterExclude)){
                        if($step->isAllowingSentToMasterExclude != "false"){
                            $email->settings[] = "Email setting 'Send email to master exclude members' is enabled.";
                        }
                    }
                    if(isset($step->isAllowingSentToUnsubscribe)){
                        if($step->isAllowingSentToUnsubscribe != "false"){
                            $email->settings[] = "Email setting 'Send email to unsubscribed members' is enabled.";
                        }
                    }else{
                        $email->settings[] = "Cannot validate 'Send email to unsubscribed members' email setting, please check manually.";
                    }

                    //Check scheduling
                    if(isset($step->schedule)){
                        //Validate scheduling times
                        $startTimes = array(28800,115200,201600,288000,374400,460800,547200); //timestamps for 8am (relative to beginning of the week for Eloqua)
                        $schedulingCorrect = true;
                        for($i=0;$i<7;$i++){
                            //loop through all 7 steps. If one of the steps is incorrect, flag the scheduling as incorrect
                            if($step->schedule->elements[$i]->relativeStart != $startTimes[$i] || !($step->schedule->elements[$i]->duration >= 43140 && $step->schedule->elements[$i]->duration <= 43200)){
                                //Durations capture settings from 8am to 7:59PM and 8AM to 8PM
                                $schedulingCorrect = false;
                            }
                        }
                        if(!$schedulingCorrect){
                            $email->settings[] = "Email scheduling is set incorrectly. Should be from 8AM to 8PM, with all 7 days checked.";
                        }
                    }else{
                        $email->settings[] = "Email scheduling isn't set.";
                    }

                    //Check break into smaller batches for ADHOC
                    if(!$lcp){
                        if($step->sendTimePeriod != "sendEmailsOverANumberOfHours"){
                            $email->settings[] = "Break into smaller batches is not enabled.";
                        }elseif($step->sendTimeLength != 2){
                            $email->settings[] = "Break into smaller batches is set incorrectly. Should be set up with a 2 hour spread.";
                        }
                    }else{
                        if($step->sendTimePeriod == "sendEmailsOverANumberOfHours"){
                            $email->settings[] = "Break into smaller batches is enabled.";
                        }
                    }

                    $routingTypes = array('unsubscribe', 'masterExclude', 'bounceback', 'emailAddressError', 'globalExclude', 'emailResent', 'emailSendingError', 'notEligible','noEmailAddress');
                    if(!$lcp && isset($step->outputTerminals)){
                        $checkArray = array();
                        foreach($step->outputTerminals as $output){
                            $checkArray[] = $output->terminalType;
                        }
                        if(array_intersect($routingTypes, $checkArray) == $routingTypes) {
                            $email->settings[] = "Routing is not set correctly. Should always be disabled.";
                        }
                    }elseif($lcp && isset($step->outputTerminals)){
                        $checkArray = array();
                        foreach($step->outputTerminals as $output){
                            $checkArray[] = $output->terminalType;
                        }
                        if(array_intersect($routingTypes, $checkArray) != $routingTypes) {
                            $email->settings[] = "Routing is not set correctly. Should always be enabled for lifecycle campaigns.";
                        }
                    }elseif($lcp && !isset($step->outputTerminals)){
                        $email->settings[] = "Routing is not set correctly. Should always be enabled for lifecycle campaigns.";
                    }

                    list($email->name, $email->eloqua, $email->nameErrors, $email->errors, $email->links, $email->images, $email->sizepx, $email->ptags, $email->dcs, $email->dupOmniture, $email->outlookImages, $email->comments, $email->styles, $email->dccTags, $email->bgGradients, $email->brokenFieldMerge, $email->doubleQuotes, $email->doubleStyleTags) = self::emailCheckAction($request, $step->emailId, 'b2b', $region, $userEmail, $campaignName->country);

                    $campaignResults->emails[] = $email;
                break;
                case "CampaignContactFilterMembershipRule":
                    if($step->filterId == 100229){
                        $filter = $elq->get('assets/contact/filter/100229');
                        if(count($filter->criteria) > 1 || $filter->criteria[0]->type != "EmailSentCriterion" || !isset($filter->criteria[0]->activityRestriction) || $filter->criteria[0]->activityRestriction->type != "NumericValueCondition" || $filter->criteria[0]->activityRestriction->operator != "notLess" || $filter->criteria[0]->activityRestriction->value != 1 || !isset($filter->criteria[0]->timeRestriction) || $filter->criteria[0]->timeRestriction->type != "DateValueCondition" || $filter->criteria[0]->timeRestriction->operator != "withinLast" || $filter->criteria[0]->timeRestriction->value->type != "RelativeDate" || $filter->criteria[0]->timeRestriction->value->offset != 5 || $filter->criteria[0]->timeRestriction->value->timePeriod != "day"){
                            $campaignResults->sharedFilter[] = "The \"".$filter->name."\" filter has been edited. Please urgently raise with an SME.";
                        }

                        $fiveDayRule = true;
                        if($step->evaluateNoAfter != 0){
                            $campaignResults->sharedFilter = "SYSTEM - SENT EMAIL IN THE LAST 5 DAYS filter has an evaluation period set. Should not have any evaluation period";
                        }
                    }elseif(!empty($this->getDoctrine()->getRepository('AppBundle:GlobalSplits')->findOneBy(array('filterId' => $step->filterId, 'type' => 'b2b')))){
                        //Global split filter validation
                        $globalValid = true;
                        $globalSplit = $this->getDoctrine()->getRepository('AppBundle:GlobalSplits')->findOneBy(array('filterId' => $step->filterId, 'type' => 'b2b'));
                        $wildcards = $globalSplit->getWildcards();
                        $doesntEnd = $globalSplit->getDoesNotEndWith();

                        $filter = $elq->get('assets/contact/filter/'.$step->filterId);

                        foreach($filter->criteria as $criteria){
                            if($criteria->type != "ContactFieldCriterion" || !isset($criteria->condition) || $criteria->condition->type != "TextValueCondition" || !isset($criteria->fieldId) || $criteria->fieldId != 100032){
                                $globalValid = false;
                                break;
                            }

                            $regexValue = str_replace('*','\*',str_replace('?','\?',$criteria->condition->value));
                            if($criteria->condition->operator == "matchesWildcardPattern"){
                                if($wildcards == ""){
                                    $globalValid = false;
                                    break;
                                }
                                $pattern = '/(\|\|)?'.$regexValue.'/i';
                                $wildcards = preg_replace($pattern,'',$wildcards);
                            }elseif($criteria->condition->operator == "doesNotEndWith"){
                                if($doesntEnd == ""){
                                    $globalValid = false;
                                    break;
                                }
                                $pattern = '/(\|\|)?'.$regexValue.'/i';
                                $doesntEnd = preg_replace($pattern,'',$doesntEnd);
                            }else{
                                $globalValid = false;
                                break;
                            }
                        }

                        if($wildcards != "" || $doesntEnd != "" || $filter->statement != $globalSplit->getStatement()){
                            $globalValid = false;
                        }

                        if(!$globalValid){
                            $campaignResults->sharedFilter[] = "The global split filter \"".$filter->name."\" has been edited. Please urgently raise this with an SME";
                        }
                    }
                break;
            }
        }
        //Set error message for missing filter
        if(!$fiveDayRule && !$lcp){
            $campaignResults->sharedFilter[] = "'SYSTEM - SENT EMAIL IN THE LAST 5 DAYS' filter is missing from the campaign.";
        }elseif($fiveDayRule && $lcp){
            $campaignResults->sharedFilter[] = "'SYSTEM - SENT EMAIL IN THE LAST 5 DAYS' filter was added while this is an always on campaign.";
        }

        $renderInfo['results'] = $campaignResults;

        $manualCheckResults = [];
        $manualChecks = array(
            "Campaign start date is correct" => "1",
            "Campaign end date is correct" => "2",
            "Checked if all links work" => "3",
            "Link tracking is enabled for all links (settings > manage links)" => "4",
            "Buttons have correct width set for outlook" => "5",
            "Emails are responsive/look good on mobile" => "6",
            "Tags that users in DCC use to indicate dynamic content replaced with actual dynamic content [e.g. first name]" => "7",
            "Emails checked via litmus. Check the major email clients (Outook desktop 15, Android, iOS, Gmail)" => "8",
            "If changes were made on code level (including images); brand approved and checked against all clients in Litmus" => "9",
            "Target audience created and approved by LM" => "10",
            "If there is a form related to this campaign; reporting steps should be added on the campaign canvas" => "11",
            "BG, BIU and Adcode are filled according to the ticket" => "12",
            "Local Subscription Center page is included in the canvas [ Landing Page ( reporting only ) ]" => "13",
            "‘Send email to unsubscribed members’ is checked for opt out emails ( ONLY IF APPROVED BY PRIVACY )" => "14" 
        );
        if($campaignName->country == "DE" || $campaignName->country == "AT" || $campaignName->country == "CH" || $campaignName->country == "US"){
            $countryManualChecks = array(
                "Email(s) approved by requester" => "15"
            );
            $manualChecks = array_merge($manualChecks,$countryManualChecks);
        }
        $manualCheckForm = $this->get('form.factory')->createNamedBuilder('ManualCheck', 'Symfony\Component\Form\Extension\Core\Type\FormType', $manualCheckResults)
            ->add('manualChecks', ChoiceType::class, array('choices' => $manualChecks, 'expanded' => true, 'required' => true, 'multiple' => true))
            ->getForm();
        $manualCheckForm->handleRequest($request);
        $renderInfo['manualCheckForm'] = $manualCheckForm->createView();

        return $this->render('tools\b2bCampaign.html.twig', $renderInfo);
    }

    /**
     * @param Request $request
     * @param int $segmentId
     * @return array
     */
    public function b2bSegmentCheckAction(Request $request, $segmentId)
    {
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));
        $segment = $elq->get('assets/contact/segment/'.$segmentId);

        $segmentResults = new \stdClass();
        $segmentResults->nameErrors = array();

        if(strpos(strtolower($segment->name), 'returnpath') === false && strpos(strtolower($segment->name), 'stakeholder segment') === false && $segment->createdAt > 1548979200){
            $segmentResults->nameErrors = self::b2bNamingCheckAction($segment->name, 'segment')->errors;
        }

        $segmentResults->eloqua = "https://secure.p01.eloqua.com/Main.aspx#segments&id=".$segmentId;

        $segmentResults->errors = array();

        $filterList = array();

        //Add all standard filters for B2B segments
        $filter1 = new b2bFilter();
        $filter1->setType("ContactFilter");
        $filter1->setId(119076);
        $filter1->setName("1.EXCLUDE PATIENT / CONSUMER");

        $filter2 = new b2bFilter();
        $filter2->setType("ContactFilter");
        $filter2->setId(104302);
        $filter2->setName("2.Exclude Global Unsubscribers");

        $filter3 = new b2bFilter();
        $filter3->setType("ContactFilter");
        $filter3->setId(104303);
        $filter3->setName("3.Exclude Hardbouncers");

        $filter4 = new b2bFilter();
        $filter4->setType("ContactFilter");
        $filter4->setId(104304);
        $filter4->setName("4.Exclude Philips.com");

        $filter5 = new b2bFilter();
        $filter5->setType("ContactFilter");
        $filter5->setId(102646);
        $filter5->setName("5.Exclude Privacy requirements");

        $filter6 = new b2bFilter();
        $filter6->setType("ContactFilter");
        $filter6->setId(100382);
        $filter6->setName("6.Exclude Embargoed Countries");

        $filter7 = new b2bFilter();
        $filter7->setType("ContactFilter");
        $filter7->setId(106111);
        $filter7->setName("7.Exclude 00Qd DKPEN Form session");

        /*$filter8 = new b2bFilter();
        $filter8->setType("ContactFilter");
        $filter8->setId(113893);
        $filter8->setName("8.EXCLUDE GOVERNMENT");*/

        $filter9 = new b2bFilter();
        $filter9->setType("ContactList");
        $filter9->setId(3278);
        $filter9->setName("9.OHC_2016_Global Exclude");

        $filter10 = new b2bFilter();
        $filter10->setType("ContactFilter");
        $filter10->setId(121544);
        $filter10->setName("10.EXCLUDE Competitor");

        $filter11 = new b2bFilter();
        $filter11->setType("ContactFilter");
        $filter11->setId(114146);
        $filter11->setName("11.Exclude Non-Engaged Contacts - 12 Months");

        $filter12 = new b2bFilter();
        $filter12->setType("ContactFilter");
        $filter12->setId(144905);
        $filter12->setName("12.Exclude Blank Country");

        $filter13 = new b2bFilter();
        $filter13->setType("ContactList");
        $filter13->setId(7161);
        $filter13->setName("13.EQ contact with work email address scrubbed from SFDC contact record");

        $filter14 = new b2bFilter();
        $filter14->setType("ContactList");
        $filter14->setId(9195);
        $filter14->setName("14.AVENT Professionals");

        $filter15 = new b2bFilter();
        $filter15->setType("ContactFilter");
        $filter15->setId(171961);
        $filter15->setName("15.Market Exclusion");

        $filter16 = new b2bFilter();
        $filter16->setType("ContactFilter");
        $filter16->setId(172725);
        $filter16->setName("16.Exclude Dental/Dentist Titles 1");

        $filter17 = new b2bFilter();
        $filter17->setType("ContactList");
        $filter17->setId(11642);
        $filter17->setName("17.NAM Executives");

        /*$filter18 = new b2bFilter();
        $filter18->setType("ContactFilter");
        $filter18->setId(192179);
        $filter18->setName("18.Double Opt-in Check for DACH");*/

    array_push($filterList, $filter1, $filter2, $filter3, $filter4, $filter5, $filter6, $filter7, /*$filter8,*/ $filter9, $filter10, $filter11, $filter12, $filter13, $filter14, $filter15, $filter16, $filter17/*, $filter18*/);        
        
        if(strpos(strtolower($segment->name), 'returnpath') === false && strpos(strtolower($segment->name), 'stakeholder segment') === false){
            foreach($segment->elements as $segmentFilter){
                foreach($filterList as $key => $filter){
                    if (isset($segmentFilter->filter)){
                        if($filter->getId() == $segmentFilter->filter->id && $filter->getType() == $segmentFilter->filter->type){
                            if($segmentFilter->isIncluded == "true"){
                                $filterList[$key]->setStatus('notexcluded');
                            }else{
                                $filterList[$key]->setStatus('correct');
                            }
                            switch($segmentFilter->filter->id){
                                case 119076:
                                    $currentFilter = "1.EXCLUDE PATIENT / CONSUMER";
                                    $correct = true;
                                    $matches = array('patient' => 'patient', 'Patient / Consumer' => 'Patient / Consumer', 'Consumer' => 'Consumer', 'Patient/Consumer' => 'Patient/Consumer');
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type == "ContactFieldCriterion" && $step->fieldId == 100016 && $step->condition->type == "TextValueCondition" && $step->condition->operator == "equal" && $step->condition->value != "Patient/Consumer"){
                                            if(in_array($step->condition->value,$matches)){
                                                unset($matches[$step->condition->value]);
                                            }else{
                                                $correct = false;
                                            }
                                        }elseif($step->type == "ContactFieldCriterion" && $step->fieldId == 100016 && $step->condition->type == "TextValueCondition" && $step->condition->operator == "contains" && $step->condition->value == "Patient/Consumer"){
                                            if(in_array($step->condition->value,$matches)){
                                                unset($matches[$step->condition->value]);
                                            }else{
                                                $correct = false;
                                            }
                                        }else{
                                            $correct = false;
                                        }
                                    }
                                    if($segmentFilter->filter->statement != '91257 OR 91258 OR 91259 OR 252261'){
                                        $correct = false;
                                    }
                                    if(!$correct || count($matches) > 0){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 104302:
                                    $currentFilter = "2.Exclude Global Unsubscribers";
                                    $correct = true;
                                    $sfdcOne = false;
                                    $unsub = false;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type == "ContactFieldCriterion"){
                                            if($step->fieldId == 100043 && $step->condition->type == "TextValueCondition" && $step->condition->operator == "equal" && $step->condition->value == 1){
                                                $sfdcOne = true;
                                            }
                                        }elseif($step->type == "UnsubscriptionCriterion"){
                                            if(!isset($step->timeRestriction)){
                                                $unsub = true;
                                            }
                                        }else{
                                            $correct = false;
                                        }
                                    }
                                    if($segmentFilter->filter->statement != "(289177 OR 289178)"){
                                        $correct = false;
                                    }
                                    if(!$correct || !$sfdcOne || !$unsub){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 104303:
                                    $currentFilter = "3.Exclude Hardbouncers";
                                    $correct = true;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type == "ContactFieldCriterion"){
                                            if($step->condition->type != "TextValueCondition" && $step->condition->operator != "equal" && $step->condition->value != "2" && $step->fieldId != 100243){
                                        $correct = false;
                                            }
                                        }elseif($step->type != "HardBouncebackCriterion" || $step->hasStatus != "True" || isset($step->timeRestriction)){
                                            $correct = false;
                                        } 
                                }

                                    if($segmentFilter->filter->statement != '27328 OR 338521'){
                                        $correct = false;
                                    }
                                    if(!$correct){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 104304:
                                    $currentFilter = "4.Exclude Philips.com";
                                    $correct = true;
                                    $emailDoman = false;
                                    $emailContains = false;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type != "ContactFieldCriterion"){
                                            $correct = false;
                                        }else{
                                            if($step->condition->type == "TextValueCondition" && $step->condition->operator == "equal" && $step->condition->value == "philips.com" && $step->fieldId == 100198){
                                                $emailDomain = true;
                                            }elseif($step->condition->type == "TextValueCondition" && $step->condition->operator == "contains" && $step->condition->value == "@philips." && $step->fieldId == 100001){
                                                $emailContains = true;
                                            }
                                        }
                                        
                                    }
                                    if($segmentFilter->filter->statement != '27329 OR 233993'){
                                        $correct = false;
                                    }
                                    if(!$correct || !$emailDomain || !$emailContains){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 102646:
                                    $currentFilter = "5.Exclude Privacy requirements";
                                    $correct = true;
                                    $state = false;
                                    $stateBlank = false;
                                    $canada = false;
                                    $france = false;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type != "ContactFieldCriterion"){
                                            $correct = false;
                                        }else{
                                            if($step->condition->type == "TextSetCondition" && $step->condition->operator == "in" && $step->condition->quickListString == "CA-QC,Quebec" && $step->fieldId == 100010){
                                                $state = true;
                                            }elseif($step->condition->type == "TextValueCondition" && $step->condition->operator == "blank" && $step->fieldId == 100010){
                                                $stateBlank = true;
                                            }elseif($step->condition->type == "TextValueCondition" && $step->condition->operator == "equal" && $step->fieldId == 100012){
                                                if($step->condition->value == "FR"){
                                                    $france = true;
                                                }elseif($step->condition->value == "CA"){
                                                    $canada = true;
                                                }else{
                                                    $correct = false;
                                                }
                                            }
                                        }
                                        
                                    }
                                    if($segmentFilter->filter->statement != '223747 OR (223748 AND 223749) OR 307485'){
                                        $correct = false;
                                    }
                                    if(!$correct || !$state || !$stateBlank || !$canada || !$france){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 100382:
                                    $currentFilter = "6.Exclude Embargoed Countries";
                                    $correct = true;
                                    $countries = array("CU" => "CU","SY" => "SY","IR" => "IR","KP" => "KP","SD" => "SD","SS" => "SS");
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type != "ContactFieldCriterion"){
                                            $correct = false;
                                        }else{
                                            if($step->condition->type != "TextValueCondition" || $step->condition->operator != "equal" || $step->fieldId != 100012 || !in_array($step->condition->value, $countries)){
                                                $correct = false;
                                            }else{
                                                unset($countries[$step->condition->value]);
                                            }
                                        }
                                        
                                    }
                                    if($segmentFilter->filter->statement != '1508 OR 1509 OR 318325 OR 318326 OR 318327 OR 318328'){
                                        $correct = false;
                                    }
                                    if(!$correct || count($countries) != 0){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 106111:
                                    $currentFilter = "7.Exclude 00Qd DKPEN Form session";
                                    $correct = true;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type != "ContactFieldCriterion"){
                                            $correct = false;
                                        }else{
                                            if($step->condition->type != "TextSetCondition" || $step->condition->operator != "in" || $step->condition->optionListId != 2812 || $step->condition->quickListString != "" || $step->fieldId != 100001){
                                                $correct = false;
                                            }
                                        }
                                        
                                    }
                                    if($segmentFilter->filter->statement != '70872'){
                                        $correct = false;
                                    }
                                    if(!$correct){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                /*case 113893:
                                    $currentFilter = "8.EXCLUDE GOVERNMENT";
                                    $correct = true;
                                    $cdo = false;
                                    $us = false;
                                    $gov = false;
                                    $mil = false;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type == "ContactFieldCriterion"){
                                            if($step->condition->type == "TextValueCondition" && $step->condition->operator == "endsWith" && $step->condition->value == ".mil" && $step->fieldId == 100001){
                                                $mil = true;
                                            }elseif($step->condition->type == "TextValueCondition" && $step->condition->operator == "endsWith" && $step->condition->value == ".gov" && $step->fieldId == 100001){
                                                $gov = true;
                                            }elseif($step->condition->type == "TextValueCondition" && $step->condition->operator == "equal" && $step->condition->value == "US" && $step->fieldId == 100012){
                                                $us = true;
                                            }else{
                                                $correct = false;
                                            }
                                        }elseif($step->type == "LinkedCustomObjectCriterion"){
                                            if($step->customObjectId != 6 || count($step->fieldConditions) > 1){
                                                $correct = false;
                                            }else{
                                                if($step->fieldConditions[0]->type == "FieldCondition" && $step->fieldConditions[0]->fieldId == 74 && $step->fieldConditions[0]->condition->type == "TextValueCondition" && $step->fieldConditions[0]->condition->operator == "equal" && $step->fieldConditions[0]->condition->value == "HSND-003726.W1NAGovernmentWebsiteContactus-form-7-_9-_5"){
                                                    $cdo = true;
                                                }else{
                                                    $correct = false;
                                                }
                                            }
                                        }else{
                                            $correct = false;
                                        }
                                        
                                    }
                                    if($segmentFilter->filter->statement != '73780 OR ((239050 OR 239051) AND 239052)'){
                                        $correct = false;
                                    }
                                    if(!$correct || !$cdo || !$us || !$gov || !$mil){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;*/
                                case 99308:
                                    $currentFilter = "10.EXCLUDE Competitor";
                                    $correct = true;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type != "ContactFieldCriterion"){
                                            $correct = false;
                                        }else{
                                            if($step->condition->type != "TextSetCondition" || $step->condition->operator != "in" || $step->condition->quickListString != "capgemini.com,ge.com,hitachimed.com,istatease.com,phillipskenya.com,siemens.com,test.com,nmqdigital.com,roche.com,leicabiosystems.com,hamamatsu.com,sectra.com,3dhistech,com,infinitt.com,motic.com,olympus.com" || $step->fieldId != 100171){
                                                $correct = false;
                                            }
                                        }                                        
                                    }
                                    if($segmentFilter->filter->statement != '99308'){
                                        $correct = false;
                                    }
                                    if(!$correct){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 114146:
                                    $currentFilter = "11.Exclude Non-Engaged Contacts - 12 Months";
                                    $correct = true;
                                    $emailOpen = false;
                                    $emailClick = false;
                                    $formSubmit = false;
                                    $visitedLandingpage = false;
                                    $visitedWebsite = false;
                                    $dateCreated = false;
                                    $campaigns = array("TY-RU" => "TY-RU", "TY-FR" => "TY-FR", "TY-WW" => "TY-WW", "TY-RU2" => "TY-RU2");
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type == "EmailOpenCriterion"){
                                            if(!isset($step->activityRestriction) || $step->activityRestriction->type != "ZeroCondition" || !isset($step->timeRestriction) || $step->timeRestriction->type != "DateValueCondition" || $step->timeRestriction->operator != "withinLast" || $step->timeRestriction->value->type != "RelativeDate" || $step->timeRestriction->value->offset != 12 || $step->timeRestriction->value->timePeriod != "month"){
                                                $correct = false;
                                            }else{
                                                $emailOpen = true;
                                            }
                                        }elseif($step->type == "EmailClickThroughCriterion")   {
                                            if(!isset($step->activityRestriction) || $step->activityRestriction->type != "ZeroCondition" || !isset($step->timeRestriction) || $step->timeRestriction->type != "DateValueCondition" || $step->timeRestriction->operator != "withinLast" || $step->timeRestriction->value->type != "RelativeDate" || $step->timeRestriction->value->offset != 12 || $step->timeRestriction->value->timePeriod != "month"){
                                                $correct = false;
                                            }else{
                                                $emailClick = true;
                                            }
                                        }elseif($step->type == "FormSubmitCriterion")                            {
                                            if(!isset($step->activityRestriction) || $step->activityRestriction->type != "ZeroCondition" || !isset($step->timeRestriction) || $step->timeRestriction->type != "DateValueCondition" || $step->timeRestriction->operator != "withinLast" || $step->timeRestriction->value->type != "RelativeDate" || $step->timeRestriction->value->offset != 12 || $step->timeRestriction->value->timePeriod != "month"){
                                                $correct = false;
                                            }else{
                                                $formSubmit = true;
                                            }
                                        }elseif($step->type == "VisitedLandingPageCriterion"){
                                            if(!isset($step->activityRestriction) || $step->activityRestriction->type != "ZeroCondition" || !isset($step->timeRestriction) || $step->timeRestriction->type != "DateValueCondition" || $step->timeRestriction->operator != "withinLast" || $step->timeRestriction->value->type != "RelativeDate" || $step->timeRestriction->value->offset != 12 || $step->timeRestriction->value->timePeriod != "month"){
                                                $correct = false;
                                            }else{
                                                $visitedLandingpage = true;
                                            }
                                        }elseif($step->type == "VisitedWebsiteCriterion"){
                                            if(!isset($step->activityRestriction) || $step->activityRestriction->type != "ZeroCondition" || !isset($step->timeRestriction) || $step->timeRestriction->type != "DateValueCondition" || $step->timeRestriction->operator != "withinLast" || $step->timeRestriction->value->type != "RelativeDate" || $step->timeRestriction->value->offset != 12 || $step->timeRestriction->value->timePeriod != "month" || $step->VisitedWebsiteType != "VisitedPage" || count($step->trackedUrlIds) != 0){
                                                $correct = false;
                                            }else{
                                                $visitedWebsite = true;
                                            }
                                        }elseif($step->type == "ContactFieldCriterion"){
                                            if($step->fieldId != 100026 || $step->condition->type != "DateValueCondition" || $step->condition->operator != "notWithinLast" || $step->condition->value->type != "RelativeDate" || $step->condition->value->offset != 12 || $step->condition->value->timePeriod != "month"){
                                                $correct = false;
                                            }else{
                                                $dateCreated = true;
                                            }
                                        }elseif($step->type == "EmailSentCriterion"){
                                            if(!isset($step->activityRestriction) || $step->activityRestriction->type != "ZeroCondition" || !isset($step->timeRestriction) || $step->timeRestriction->type != "DateValueCondition" || $step->timeRestriction->operator != "withinLast" || $step->timeRestriction->value->type != "RelativeDate" || $step->timeRestriction->value->offset != 12 || $step->timeRestriction->value->timePeriod != "month" || count($step->campaignIds) == 0){
                                                $correct = false;
                                            }else{
                                                switch($step->campaignIds[0]){
                                                    case 2663:
                                                        unset($campaigns["TY-RU2"]);
                                                    break;
                                                    case 1720:
                                                        unset($campaigns["TY-WW"]);
                                                    break;
                                                    case 1140:
                                                        unset($campaigns["TY-RU"]);
                                                    break;
                                                    case 1161:
                                                        unset($campaigns["TY-FR"]);
                                                    break;
                                                }
                                            }
                                        }else{
                                            $correct = false;
                                        }
                                    }
                                    if($segmentFilter->filter->statement != '74440 AND 211403 AND 211402 AND 191650 AND 101724 AND 74436 AND 74437 AND 74438 AND 74439 AND 74441'){
                                        $correct = false;
                                    }
                                    if(!$correct || !$emailOpen || !$emailClick || !$formSubmit || !$visitedLandingpage || !$visitedWebsite || !$dateCreated || count($campaigns) != 0){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 144905:
                                    $currentFilter = "12.Exclude Blank Country";
                                    $correct = true;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type != "ContactFieldCriterion"){
                                            $correct = false;
                                        }else{
                                            if($step->condition->type != "TextValueCondition" || $step->condition->operator != "blank" || $step->fieldId != 100012){
                                                $correct = false;
                                            }
                                        }
                                        
                                    }
                                    if($segmentFilter->filter->statement != '192682'){
                                        $correct = false;
                                    }
                                    if(!$correct){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 171961:
                                    $currentFilter = "15.Market Exclusion";
                                    $correct = true;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type != "ContactFieldCriterion"){
                                            $correct = false;
                                        }else{
                                            if($step->condition->type != "TextSetCondition" || $step->condition->operator != "in" || $step->condition->optionListId != 355 || $step->condition->quickListString != "" || $step->fieldId != 100012){
                                                $correct = false;
                                            }
                                        }
                                        
                                    }
                                    if($segmentFilter->filter->statement != '335156'){
                                        $correct = false;
                                    }
                                    if(!$correct){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                case 172725:
                                    $currentFilter = "16.Exclude Dental/Dentist Titles 1";
                                    $correct = true;
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type == "ContactFieldCriterion"){
                                            if($step->condition->type != "TextValueCondition" || $step->condition->operator != "contains" || $step->fieldId != 100016 || ($step->condition->value != "dentist" && $step->condition->value != "dental")){
                                                $correct = false;
                                            }
                                        }elseif($step->type == "LinkedCustomObjectCriterion"){
                                            if($step->customObjectId != 226 || count($step->fieldConditions) != 1 || $step->fieldConditions[0]->fieldId != 5099 || $step->fieldConditions[0]->condition->type != "TextValueCondition" || $step->fieldConditions[0]->condition->operator != "notBlank"){
                                                $correct = false;
                                            }
                                        }else{
                                            $correct = false;
                                        }
                                    }
                                    if($segmentFilter->filter->statement != '260006 OR 260007 OR 279186'){
                                        $correct = false;
                                    }
                                    if(!$correct){
                                        $filterList[$key]->setErrors($currentFilter.' has been altered. Please urgently raise this with Lead Management Team.');
                                    }
                                break;
                                
                               /* case 192179:
                                    $currentFilter = "18.Double Opt-in check for DACH";
                                    $correct = true;
                                    
                                    foreach($segmentFilter->filter->criteria as $step){
                                        if($step->type == "FormSubmitCriterion"){
                                            if($step->activityRestriction->type != "ZeroCondition" || $step->formIds->id != "303620" || $step->timeRestriction->operator != "withinLast" || ($step->value->type != "RelativeDate" && $step->value->offset != 12 && $step->value->month != "month" )){
                                                $correct = false;
                                            }
                                        }
                                    }

                                break;*/
                            }  
                        }
                    } elseif (isset($segmentFilter->list)){
                        if($filter->getId() == $segmentFilter->list->id && $filter->getType() == $segmentFilter->list->type){
                            if($segmentFilter->isIncluded == "true"){
                                $filterList[$key]->setStatus('notexcluded');
                            }else{
                                $filterList[$key]->setStatus('correct');
                            }
                        }
                    }
                }
            }


            foreach($filterList as $filter){
                //filter 17 only if segment was created from february 1st 2019 onwards
                if ($filter->getStatus() == "notfound"){
                    if($filter->getName() != "17.NAM Executives" || ($filter->getName() == "17.NAM Executives" && $segment->createdAt > 1548979200)){
                        $segmentResults->errors[] = $filter->getName()." is missing.";
                    }
                } elseif ($filter->getStatus() == "notexcluded"){
                    if($filter->getName() != "17.NAM Executives" || ($filter->getName() == "17.NAM Executives" && $segment->createdAt > 1548979200)){
                        $segmentResults->errors[] = $filter->getName()." is set to include instead of exclude.";
                    }
                }
                foreach($filter->getErrors() as $error){
                    $segmentResults->errors[] = $error;                    
                }
            }
        }

        return array($segment->name, $segmentResults->nameErrors, $segmentResults->errors, $segmentResults->eloqua);
    }

    /**
     * @param Request $request
     * @param int $emailId
     * @param string $type
     * @param \AppBundle\Entity\RegionSettings $region
     * @param string $userEmail
     * @param string $country
     * @return array
     */
    public function emailCheckAction(Request $request, $emailId, $type, $region, $userEmail, $country, $external = null, $rr = null)
    {
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));
        
        $emailResults = new \stdClass();

        $email = $elq->get('assets/email/'.$emailId);

        $proofExclusion = array("bram.van.den.boomen@philips.com", "ozan.kayikci@philips.com", "berkan.cinar@philips.com");

        if(!in_array(strtolower($userEmail),$proofExclusion)){

            $userEmail = strtolower($userEmail);

            $contactLookup = $elq->get("data/contacts?search='".$userEmail."'&depth=partial");
            $newContact = new Contact;
            $newContact->setEmailAddress($userEmail);
            $newContact->setIsSubscribed('true');

            if(count($contactLookup->elements) != 1 || $contactLookup->elements[0]->name != $userEmail){
                //if the contact doesnt exist - create
                $contact = $elq->post('data/contact', $newContact);
            }elseif($contactLookup->elements[0]->isSubscribed != "true"){
                //if the contact is not subscribed - subscribe
                $newContact->setId($contactLookup->elements[0]->id);
                $contact = $elq->put('data/contact/'.$contactLookup->elements[0]->id, $newContact);
            }else{
                $contact = $contactLookup->elements[0];
            }

            $deployment = new Deployment;
            $deployment->setContactId($contact->id);
            $depEmail = new \stdClass();
            $depEmail->id = $email->id;
            $depEmail->type = "Email";
            $depEmail->name = $email->name;
            $deployment->setEmail($depEmail);
            
            $proof = $elq->post("/assets/email/deployment", $deployment);
        }

        if($type == 'b2c'){
            $nameResults = self::b2cNamingCheckAction($email->name, 'EM');
        }else{
            $nameResults = self::b2bNamingCheckAction($email->name, 'email');
        }
        $emailResults->nameErrors = $nameResults->errors;

        $emailResults->emailErrors = array();
        $emailResults->eloqua = "https://secure.p01.eloqua.com/Main.aspx#emails&id=".$emailId;

        $html = $email->htmlContent->html;

        //Validate email settings based on region
        if($email->bounceBackEmail != $region->getBounceBackEmail()){
            $emailResults->emailErrors[] = "Bounceback email has been set to <span class=\"red\">'".$email->bounceBackEmail."'</span>. Should be '".$region->getBounceBackEmail()."'.";
        }

        if($email->virtualMTAId != $region->getVirtualMtaId()){
            $virtualMta = "";
            if($type == 'b2b'){
                $virtualMta = "bounce@2.forms.healthcare.philips.com";
            }else{
                switch($region->getVirtualMtaId()){
                    case 3:
                        $virtualMta = "bounce@emea.consumerproducts.philips.com";
                    break;
                    case 4:
                        $virtualMta = "bounce@apac.consumerproducts.philips.com";
                    break;
                    case 5:
                        $virtualMta = "bounce@consumerproducts.philips.com";
                    break;
                }
            }
            $emailResults->emailErrors[] = "Virtual MTAs has been set incorrectly. Should be '".$virtualMta."'.";
        }

        if($email->replyToEmail != $region->getReplyToEmail()){
            $emailResults->emailErrors[] = "Reply to email has been set to <span class=\"red\">'".$email->replyToEmail."'</span>. Should be '".$region->getReplyToEmail()."'.";
        }

        if($email->replyToName != $region->getReplyToName()){
            $emailResults->emailErrors[] = "Reply to name has been set to <span class=\"red\">'".$email->replyToName."'</span>. Should be '".$region->getReplyToName()."'.";
        }

        if($email->senderEmail != $region->getSenderEmail()){
            $emailResults->emailErrors[] = "From email has been set to <span class=\"red\">'".$email->senderEmail."'</span>. Should be '".$region->getSenderEmail()."'.";
        }

        if($email->senderName != $region->getSenderName()){
            $emailResults->emailErrors[] = "From name has been set to <span class=\"red\">'".$email->senderName."'</span>. Should be '".$region->getSenderName()."'.";
        }

        if($type == 'b2c'){
            //Validate email settings with pre-defined values
            if($nameResults->cat == "MCC" || $nameResults->cat == "OHC"){
                $requiredEmailGroup = "HW - ".$nameResults->cat;
            }else{
                $requiredEmailGroup = "General Newsletter";             
            }

            if(($email->emailGroupId != 474 && $nameResults->cat != "MCC" && $nameResults->cat != "OHC") || ($email->emailGroupId != 475 && $nameResults->cat == "MCC") || ($email->emailGroupId != 476 && $nameResults->cat == "OHC")){
                $emailResults->emailErrors[] = "Email group has been set incorrectly. Should be '".$requiredEmailGroup."'.";
            }
        }

        if(($type == 'b2c' && $email->emailHeaderId != 340) || ($type == 'b2b' && $email->emailHeaderId != 7)){ //NO HEADER ID
            $emailResults->emailErrors[] = "Email header has been set incorrectly. Should be 'NO HEADER'.";
        }

        if(($type == 'b2c' && $email->emailFooterId != 386) || ($type == 'b2b' && $email->emailFooterId != 10)){ //NO FOOTER ID
            $emailResults->emailErrors[] = "Email footer has been set incorrectly. Should be 'NO FOOTER'.";
        }

        if($email->sendPlainTextOnly != "false"){
            $emailResults->emailErrors[] = "Send plain text email is enabled. Should be disabled.";
        }

        //Validate subjectline
        $subjectError = false;
        if($email->subject == ""){
            $emailResults->emailErrors[] = "Email subjectline setting is missing. Please fill in the subject.";
            $subjectError = true;
        }

        preg_match('/<title( name=\"title\")?>\n*(?P<subject>.+?)\n*<\/title>/is', $html, $subject);
        $subject['subject'] = trim($subject['subject']);
        if(!array_key_exists('subject',$subject) || $subject['subject'] == "" || $subject['subject'] == "Your subject line"){
            $emailResults->emailErrors[] = "The subjectline has not been set in the html &lt;title&gt;. Please add the subjectline to the &lt;title&gt;.";
            $subjectError = true;
        }

        if(htmlspecialchars_decode($subject['subject']) != htmlspecialchars_decode($email->subject) && !$subjectError){
            if(preg_match_all('/<span elqid="\d+".*?<\/span>/is', $subject['subject'], $dcMatches)){
                foreach($dcMatches[0] as $dcMatch){
                    preg_match('/elqid="(?P<id>\d+)"/',$dcMatch,$dcId);
                    $dc = $elq->get('assets/DynamicContent/'.$dcId['id']);
                    $subject['subject'] = str_replace($dcMatch,$dc->name,$subject['subject']);

                    //find non <span> tags in subjectline dynamic content
                    if(preg_match('/<(?!\/?span).*?>/',$dc->defaultContentSection->contentHtml,$tagsFound)){
                        $emailResults->emailErrors[] = "The subjectline dynamic content rule \"Default Rule\" contains an HTML tag other than span. Please remove as this will cause the subjectline to be invisible on iOS.";
                    }
                    foreach($dc->rules as $rule){
                        if(preg_match('/<(?!\/?span).*?>/',$rule->contentSection->contentHtml,$tagsFound)){
                            $emailResults->emailErrors[] = "The subjectline dynamic content rule \"".$rule->contentSection->name."\" contains an HTML tag other than span. Please remove as this will cause the subjectline to be invisible on iOS.";
                        }
                    }
                }
            }
            $subject['subject'] = preg_replace('/<span class=eloquaemail>(?<fmName>.+?)<\/span>/','$1',$subject['subject']);

            //remove nbsp
            $string = htmlentities($subject['subject'], null, 'utf-8');
            $subject['subject'] = str_replace("&nbsp;", " ", $string);
            $subject['subject'] = html_entity_decode($subject['subject']);

            $string = htmlentities(preg_replace('/<\/?eloqua[^>]*>/',"",$email->subject), null, 'utf-8');
            $email->subject = str_replace("&nbsp;", " ", $string);
            $email->subject = html_entity_decode($email->subject);
            
            if(htmlspecialchars_decode($subject['subject']) != htmlspecialchars_decode($email->subject)){
                $emailResults->emailErrors[] = "The subjectline in the email setting and the &lt;title&gt; tags don't match. Please ensure that the same subjectline is available in both.";                
            }
        }

        if(!$subjectError){
            $emailResults->emailErrors[] = "The following subjectline is used: <b>".$email->subject."</b>.";
        }

        //Validate preheader
        preg_match('/class="c-preHeader"[^>]+>.*?(<!--.*?-->)?[\n\r]?\s*?(?P<preheader>.+?)\s*?<\/div>/is', $html, $preheader);
        if(!isset($preheader['preheader']) || $preheader['preheader'] == "" || $preheader['preheader'] == "Your pre-header here"){
            $emailResults->emailErrors[] = "The preheader has not been set. Please enter the pre-header.";
        }else{
            $emailResults->emailErrors[] = "The following preheader is used: <b>".$preheader['preheader']."</b>.";
        }

        if($type == 'b2c'){
            //country specific validations
            if($country == "US"){
                //Check for US emails permission on US campaigns
                $permissionSet = false;
                $permissions = $elq->get('assets/email/'.$emailId.'/security/grants');
                foreach($permissions->elements as $permission){
                    if($permission->recipient->id == 94 && $permission->permissions[0] == "Retrieve" && count($permission->permissions) == 1){
                        $permissionSet = true;
                    }
                }
                if(!$permissionSet){
                    $emailResults->emailErrors[] = "'US emails' permission isn't set correctly. The security group 'US emails' should be added, and only given view rights.";
                }
            }elseif($country == "DE" || $country == "AT" || $country == "CH"){
                $dcAvailable = false;
                foreach($email->dynamicContents as $key => $dc){
                    if($dc->id == 1325 || $dc->id == 1326){
                        $dcAvailable = true;
                    }
                }
                if(!$dcAvailable){
                    if($country == "CH"){
                        $emailResults->emailErrors[] = "German ".$country." emails should have either \"DACH German Formal Salutation\" or \"DACH German Semi-Formal Salutation\". If your email is French, please ignore this error.";
                    }else{
                        $emailResults->emailErrors[] = $country." emails should have either \"DACH German Formal Salutation\" or \"DACH German Semi-Formal Salutation\".";
                    }
                }
            }
        }elseif($type == 'b2b'){
            $profileManagementEN = preg_match('/https?:\/\/www\.healthsystems\.philips\.com\/profile_management[^_]/is', $html);
            $privacyNoticeNormalEN = preg_match('/https?:\/\/www\.philips\.com\/a-w\/privacy-notice\.html/is', $html);

            $profileManagementES = preg_match('/https?:\/\/www\.healthsystems\.philips\.com\/profile_management_es/is', $html);
            $privacyNoticeNormalES = preg_match('/https?:\/\/www\.centralamerica\.philips\.com\/a-w\/aviso-de-privacidad\.html/is', $html);

            $profileManagementDE = preg_match('/https?:\/\/www\.healthsystems\.philips\.com\/profile_management_de/is', $html);
            $privacyNoticeNormalDE = preg_match('/https?:\/\/www\.philips\.de\/a-w\/datenschutzbestimmungen\.html/is', $html);

            if($nameResults->country == "WW" && $nameResults->language == "en" && (!$profileManagementEN || !$privacyNoticeNormalEN)){
                $emailResults->emailErrors[] = "The global footer was not used while the email is WW. Please make sure the correct footer is used.";
            }elseif($nameResults->country == "WW" && $nameResults->language == "es" && (!$profileManagementES || !$privacyNoticeNormalES)){
                $emailResults->emailErrors[] = "The global footer was not used while the email is WW. Please make sure the correct footer is used.";
            }elseif($nameResults->country == "WW" && $nameResults->language == "de" && (!$profileManagementDE || !$privacyNoticeNormalDE)){
                $emailResults->emailErrors[] = "The global footer was not used while the email is WW. Please make sure the correct footer is used.";
            }elseif($nameResults->country != "WW" && ($profileManagementEN || $privacyNoticeNormalEN || $profileManagementES || $privacyNoticeNormalES || $profileManagementDE || $privacyNoticeNormalDE)){
                $emailResults->emailErrors[] = "The global footer was used while the email is not WW. Please make sure the correct footer is used.";
            }

            if($nameResults->country != null || $nameResults->country == "WW"){
                if(!preg_match("/<\/center>[\n\r\s]+<style data\-ignore\-inlining>@media \(prefers\-color\-scheme: dark\) \{ #_t::before \{content:url\(\'https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?rd&d=<span class=eloquaemail>EmailAddress<\/span>\'\);\}\} @media \(prefers\-color\-scheme: light\) \{ #_t::before \{content:url\(\'https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?rl&d=<span class=eloquaemail>EmailAddress<\/span>\'\);\}\} @media print\{ #_t \{background\-image:url\(\'https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?p&d=<span class=eloquaemail>EmailAddress<\/span>\'\);\}\} div\.OutlookMessageHeader \{background\-image:url\(\'https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?f&d=<span class=eloquaemail>EmailAddress<\/span>\'\)\} table\.moz\-email\-headers\-table \{background\-image:url\(\'https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?f&d=<span class=eloquaemail>EmailAddress<\/span>\'\)\} blockquote #_t \{background\-image:url\(\'https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?f&d=<span class=eloquaemail>EmailAddress<\/span>\'\)\}[\n\r\s]+ #MailContainerBody #_t \{background\-image:url\(\'https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?f&d=<span class=eloquaemail>EmailAddress<\/span>\'\)\}<\/style><div id=\"_t\"><\/div>[\n\r\s]+<img src=\"https:\/\/[a-z\d]+\.emltrk\.com\/v2\/[a-z\d]+\?d=~~eloqua\.\.type\-\-emailfield\.\.syntax\-\-EmailAddress\.\.innerText\-\-EmailAddress~~\" width=\"1\" height=\"1\" border=\"0\" alt=\"\" \/>[\n\r\s]+<\/body>/",$html)){
                    $emailResults->emailErrors[] = "The Litmus tracking is missing or not added correctly, please make sure to add it between the closing  <b>&lt;/center&gt;</b> and <b>&lt;/body&gt;</b> tags.";
                }
            }
        }

        $emailResults->dccTags = array();

        //Check for nonexistent fieldmerges
        $emailResults->brokenFieldMerge = self::fieldMergeValidation($request, $html);

        //Check if px or other text has been used in width/height attributes
        if(preg_match_all('/(\{\=\{.*?\}\=\}|\{\*\{.*?\}\*\}|\{\[\{.*?\}\]\})/s',$html,$dccTagMatches)){
            foreach($dccTagMatches[0] as $dccTag){
                $emailResults->dccTags[] = $dccTag;
            }
        }

        $emailResults->bgGradient = array();

        //Check if px or other text has been used in width/height attributes
        if(preg_match_all('/<.*?style="(.*?gradient.*?)".*?>/',$html,$bgGradientMatches)){
            $i = 0;
            foreach($bgGradientMatches[1] as $bgGradient){
                if(!preg_match('/background-color:\s*#/',$bgGradient)){
                    $emailResults->bgGradient[] = $bgGradientMatches[0][$i];
                }
                $i++;
            }
        }

        $emailResults->sizepx = array();

        //Check if px or other text has been used in width/height attributes
        if(preg_match_all('/(width|height)=\"\d+(px|PX)\"/',$html,$pxAttributes)){
            foreach($pxAttributes[0] as $sizepx){
                $emailResults->sizepx[] = $sizepx;
            }
        }

        $emailResults->doubleQuotes = array();

        //Check if px or other text has been used in width/height attributes
        if(preg_match_all('/<[^(!DOCTYPE)].*?[^(alt=)]"\s*".*?>/',$html,$doubleQuoteMatches)){
            foreach($doubleQuoteMatches[0] as $doubleQuote){
                $emailResults->doubleQuotes[] = $doubleQuote;
            }
        }

        $emailResults->doubleStyleTags = array();

        //Check if px or other text has been used in width/height attributes
        if(preg_match_all('/<.*?style="[^>]*?style=".*?>/',$html,$doubleStyleTags)){
            foreach($doubleStyleTags[0] as $doubleStyleTag){
                $emailResults->doubleStyleTags[] = $doubleStyleTag;
            }
        }

        $emailResults->pTags = array();

        //Check if <p> is opened in another <p>
        if(preg_match_all('/(<p .*?>|<p>)(?P<content>.*?)<\/p>/s',$html,$matches)){
            foreach($matches[0] as $key => $match){
                if(preg_match('/(<p .*?>|<p>)/s',$matches['content'][$key])){
                    $emailResults->pTags[] = $match;
                }
            }
        }

        $emailResults->outlookImages = array();

        //Check for old way of working outlook images
        if(preg_match_all('/<.*?class=\".*?outlookImage.*?\".*?>/', $html, $outlookImages)){
            foreach($outlookImages[0] as $outlookimg){
                $emailResults->outlookImages[] = $outlookimg;
            }
        }

        $emailResults->comments = array();

        //Check comments were closed incorrectly
        if(preg_match_all('/<!--.*?--!>/',$html, $commentMatches)){
            foreach($commentMatches[0] as $comment){
                $emailResults->comments[] = $comment;
            }
        }

        $emailResults->doubleStyle = array();

        //Check if styles contain ;; or ##
        if(preg_match_all('/style=".*?(;;|\#\#).*?"/',$html, $styleMatches)){
            foreach($styleMatches[0] as $style){
                $emailResults->doubleStyle[] = $style;
            }
        }
        if(preg_match_all('/<style.*?>(.*?)<\/style>/s',$html, $styleMatches)){
            foreach($styleMatches[1] as $style){
                if(preg_match_all('/^.*(;;|\#\#).*$/m',$style, $rowMatches)){
                    foreach($rowMatches[0] as $match){
                        $emailResults->doubleStyle[] = $match;
                    }
                }
            }
        }

        list($emailResults->imageErrors, $rrImage) = self::imageValidation($html, $rr);
        if($rr && !$rrImage){
            $emailResults->emailErrors[] = "There was no dynamic R&R image found.";
        }

        $countryDomain = ($type == 'b2c')?$this->getDoctrine()->getRepository('AppBundle:CountryDomains')->findOneBy(array('country' => $country))->getDomain():null;
        $omnitures = array();

        list($emailResults->linkErrors, $emailResults->emailErrors[], $emailResults->emailErrors[], $omnitures, $rrCount) = self::linkValidation($html, $type, $omnitures, $country, $countryDomain, $rr);

        if($rr && $rrCount < 2){
            $emailResults->emailErrors[] = "Only one R&R link was found. Should have at least two on the image as well as the CTA.";
        }

        list($emailResults->dcErrors, $omnitures) = self::dynamicContentValidation($request, $html, $type, $omnitures, $country, $countryDomain, $rr);

        $duplicates=array();
        $processed=array();
        foreach($omnitures as $i) {
           if(in_array($i,$processed)) {
               $duplicates[]=$i;
           } else {
               $processed[]=$i;
           }
        }

        $emailResults->duplicateOmniture = array_unique($duplicates);

        if($type == 'b2c'){
            //Find fieldmerge for voucher and validate setting types
            foreach($email->fieldMerges as $key => $fieldMerge){
                //filter out standard field merges
                $standardMerges = array("11","12","13","69","296");
                if(in_array($fieldMerge->id,$standardMerges)){
                    continue;
                }
                if(isset($fieldMerge->customObjectId) && $fieldMerge->customObjectId == 464){
                    $emailResults->emailErrors[] = "Voucher bucket set in email is: <strong>".$fieldMerge->fieldConditions[0]->condition->value."</strong>";
                    if($fieldMerge->customObjectFieldId != 7049 || $fieldMerge->customObjectSort != "lastModified" || $fieldMerge->fieldConditions[0]->type != "FieldCondition" || $fieldMerge->fieldConditions[0]->fieldId != 7050 || $fieldMerge->fieldConditions[0]->condition->type != "TextValueCondition" || $fieldMerge->fieldConditions[0]->condition->operator != "equal"){
                        $emailResults->emailErrors[] = "The vouchercode field merge is not set correctly. Should be set to the following values:<ul><li>VoucherCodeNew</li><li>VoucherCode</li><li>Last Modified</li><li>Bucket</li><li>exactly</li><li>bucketname</li></ul>";
                    }
                }
            }
        }

        //Check if current year fieldmerge is there
        if(($type == 'b2c' && $country !== 'TW' && !preg_match('/\<span.*?class=\"eloquaemail\".*?>.*?Current\_Year1.*?\<\/span\>/mis', $html)) || ($type == 'b2b' && !preg_match('/\<span.*?class=\"eloquaemail\".*?>.*?CurrentYear1.*?\<\/span\>/mis', $html))){
            $emailResults->emailErrors[] = "The current year fieldmerge is missing/incorrect.";
        }

        if($external){
            return new JsonResponse(array($email->name, $emailResults->eloqua, $emailResults->nameErrors, $emailResults->emailErrors, $emailResults->linkErrors, $emailResults->imageErrors, $emailResults->sizepx, $emailResults->pTags, $emailResults->dcErrors, $emailResults->duplicateOmniture, $emailResults->outlookImages, $emailResults->comments, $emailResults->doubleStyle, $emailResults->dccTags, $emailResults->bgGradient, $emailResults->brokenFieldMerge, $emailResults->doubleQuotes, $emailResults->doubleStyleTags, $nameResults));
        }else{
            return array($email->name, $emailResults->eloqua, $emailResults->nameErrors, $emailResults->emailErrors, $emailResults->linkErrors, $emailResults->imageErrors, $emailResults->sizepx, $emailResults->pTags, $emailResults->dcErrors, $emailResults->duplicateOmniture, $emailResults->outlookImages, $emailResults->comments, $emailResults->doubleStyle, $emailResults->dccTags, $emailResults->bgGradient, $emailResults->brokenFieldMerge, $emailResults->doubleQuotes, $emailResults->doubleStyleTags);
        }
    }

    /**
     * @param string $name
     * @return \stdClass
     */
    public function b2bNamingCheckAction($name, $assetType, $external = null)
    {
        $bgs = ['CBG','DI','EB','HI','HTS','HW','IGT','CTO','MA','TC','PHM','SRC','US'];
        $languages = ['af','ar','bg','ca','zh','zf','hr','cs','da','nl','en','et','fi','fr','de','el','he','hu','is','id','it','ja','ko','lv','lt','ms','no','pl','pt','ro','ru','sr','sh','sk','sl','es','sv','tw','th','tr','uk','dual'];
        $countries = ['WW','AF','AX','AL','DZ','AS','AD','AO','AI','AQ','AG','AR','AM','AW','AU','AT','AZ','BS','BH','BD','BB','BY','BE','BZ','BJ','BM','BT','BO','BA','BW','BV','BR','IO','BN','BG','BF','BI','KH','CM','CA','CV','KY','CF','TD','CL','CN','CX','CC','CO','KM','CG','CD','CK','CR','CI','HR','CU','CY','CZ','DK','DJ','DM','DO','EC','EG','SV','GQ','ER','EE','ET','FK','FO','FJ','FI','FR','GF','PF','TF','GA','GM','GE','DE','GH','GI','GR','GL','GD','GP','GU','GT','GN','GW','GY','HT','HM','HN','HK','HU','IS','IN','ID','IR','IQ','IE','IL','IT','JM','JP','JO','KZ','KE','KI','KP','KR','KW','KG','LA','LV','LB','LS','LR','LY','LI','LT','LU','MO','MK','MG','MW','MY','MV','ML','MT','MH','MQ','MR','MU','YT','MX','FM','MD','MC','MN','MS','MA','MZ','MM','MP','NA','NR','NP','NL','AN','NC','NZ','NI','NE','NG','NU','NF','NO','OM','PK','PW','PS','PA','PG','PY','PE','PH','PN','PL','PT','PR','QA','RE','RO','RU','RW','KN','LC','WS','SM','ST','SA','SN','CS','SC','SL','SG','SK','SI','SB','SO','ZA','GS','ES','LK','SH','PM','VC','SD','SR','SJ','SZ','SE','CH','SY','TW','TJ','TZ','TH','TL','TG','TK','TO','TT','TN','TR','TM','TC','TV','UG','UA','AE','GB','US','UM','UY','UZ','VU','VA','VE','VN','VG','VI','WF','EH','YE','ZM','ZW'];
        $channels = ['Email','Web','Social'];
        $campaigntypes = ['E-mailshot','eNewsletter','Registration','Event','Welcome','Awareness','Reengagement','Nurture','Social','Other'];
        $campaigncategories = ['AO','AH'];

        $results = new \stdClass();
        $results->errors = array();
        $results->country = null;
        $results->language = null;
        $results->bg = null;
        $results->campaigntype = null;
        $results->campaigncategory = null;

        if(preg_match('/(\s\s+)/', $name)){
            $results->errors[] = "The name contains a double space, please fix this as kayako will make a period out of the double space.";
        }

        preg_match('/^(?P<year>\d{2})?\/(?P<month>\d{2})?_?(?P<bg>[A-Za-z]{2,4})?_?(?P<name>[A-Za-z\d]+)?_?(?P<language>dual|[A-Za-z]{2})?_?(?P<country>[A-Za-z]{2})?_?(?P<channel>Email|Web|Social)?_?(?P<campaigntype>[A-Za-z\-]+)?_?(?P<campaigncategory>[A-Za-z]{2})?_?(?P<sfdcid>\d{4}[A-Za-z]\d{6}[A-Za-z\d]{4})?_?(?P<ticket>CHG\d{7}|[A-Za-z]{3}\-\d{3}\-\d{5})?(_.+)?$/', $name, $parts);

        if(array_key_exists('year',$parts)){
            if(strlen($parts['year']) != 2 || $parts['year'] > date("y")){
                $results->errors[] = 'Year is incorrect.';
            }
        }else{
            $results->errors[] = 'Year is missing.';
        }

        if(array_key_exists('month',$parts)){
            if(strlen($parts['month']) != 2 || $parts['month'] > 12){
                $results->errors[] = 'Month is incorrect.';
            }
        }else{
            $results->errors[] = 'Month is missing.';
        }

        if(array_key_exists('bg',$parts)){
            if(in_array($parts['bg'], $bgs)){
                $results->bg = $parts['bg'];
            }else{
                $results->errors[] = 'BG is incorrect.';
            }
        }else{
            $results->errors[] = 'BG is missing.';
        }

        if(!array_key_exists('name',$parts)){
            $results->errors[] = 'Campaign name is missing.';
        }

        if(array_key_exists('language',$parts)){
            if(in_array($parts['language'], $languages)){
                $results->language = $parts['language'];
            }else{
                $results->errors[] = 'Language is incorrect.';
            }
        }else{
            $results->errors[] = 'Language is missing.';
        }

        if(array_key_exists('country',$parts)){
            if(in_array($parts['country'], $countries)){
                $results->country = $parts['country'];
            }else{
                $results->errors[] = 'Country is incorrect.';
            }
        }else{
            $results->errors[] = 'Country is missing.';
        }

        if(array_key_exists('channel',$parts)){
            if(!in_array($parts['channel'], $channels)){
                $results->errors[] = 'Channel is incorrect.';
            }
        }else{
            $results->errors[] = 'Channel is missing.';
        }

        if(array_key_exists('campaigntype',$parts)){
            if(in_array($parts['campaigntype'], $campaigntypes)){
                $results->campaigntype = $parts['campaigntype'];
            }else{
                $results->errors[] = 'Campaign type is incorrect.';
            }
        }else{
            $results->errors[] = 'Campaign type is missing.';
        }

        if(array_key_exists('campaigncategory',$parts) && $parts['campaigncategory'] != ""){
            if($assetType != 'segment'){
                if(in_array($parts['campaigncategory'], $campaigncategories)){
                    $results->campaigncategory = $parts['campaigncategory'];
                }else{
                    $results->errors[] = 'Campaign category is incorrect.';
                }
            }else{
                $results->errors[] = 'Campaign category has been added to the naming, please remove.';
            }
        }else{
            if($assetType != 'segment'){
                $results->errors[] = 'Campaign type is missing.';
            }
        }

        if(array_key_exists('sfdcid',$parts) && $parts['sfdcid'] != ""){
            $results->errors[] = 'SFDC campaign ID has been added to the naming, please remove.';
        }

        if(!array_key_exists('ticket',$parts)){
            $results->errors[] = 'Kayako ticket ID is missing or incorrect.';
        }

        if($external){
            return new JsonResponse($results);
        }else{
            return $results;
        }
    }

    /**
     * @param Request $request
     * @param string $html
     * @param string $type
     * @param string $countryDomain
     * @return array
     */
    public function dynamicContentValidation(Request $request, $html, $type, $omnitures, $country, $countryDomain, $rr = null){
        $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));

        $dcErrors = array();

        preg_match_all('/<span.*?elqid="(?P<dcId>\d+)".*?><\/span>/i', $html, $dcIds);

        foreach($dcIds['dcId'] as $id){
            $elqDc = $elq->get('assets/DynamicContent/'.$id);

            $dc = new \stdClass();
            $dc->name = $elqDc->name;
            $dc->steps = array();

            //default step
            if($elqDc->defaultContentSection->contentHtml != ""){
                $step = new \stdClass();
                $step->name = "Default";
                list($step->imageErrors, $step->rrImage) = self::imageValidation($elqDc->defaultContentSection->contentHtml, $rr);
                list($step->linkErrors, $viewError, $unsubError, $omnitures, $rrCount) = self::linkValidation($elqDc->defaultContentSection->contentHtml, $type, $omnitures, $country, $countryDomain, $rr);
                $step->brokenFieldMerge = self::fieldMergeValidation($request, $elqDc->defaultContentSection->contentHtml);
                if(count($step->imageErrors) > 0 || count($step->linkErrors) > 0 || $step->brokenFieldMerge){
                    $dc->steps[] = $step;
                }
            }

            //additional steps
            foreach($elqDc->rules as $dcStep){
                $step = new \stdClass();
                $step->name = $dcStep->contentSection->name;
                list($step->imageErrors, $step->rrImage) = self::imageValidation($dcStep->contentSection->contentHtml, $rr);
                list($step->linkErrors, $viewError, $unsubError, $omnitures, $rrCount) = self::linkValidation($dcStep->contentSection->contentHtml, $type, $omnitures, $country, $countryDomain, $rr);
                $step->brokenFieldMerge = self::fieldMergeValidation($request, $dcStep->contentSection->contentHtml);
                if(count($step->imageErrors) > 0 || count($step->linkErrors) > 0 || $step->brokenFieldMerge){
                    $dc->steps[] = $step;
                }
            }

            if(count($dc->steps) > 0){
                $dcErrors[] = $dc;
            }
        }

        return array($dcErrors, $omnitures);
    }

    /**
     * @param Request $request
     * @param string $html
     * @return bool
     */
    public function fieldMergeValidation(Request $request, $html){
        preg_match_all('/<span[^>]*?(unselectable="on"|class=eloquaemail)[^>]*>(\s|\\r|\\n)(?P<fieldMerge>.*?)(\s|\\r|\\n)<\/span>/is', $html, $fieldMerges);

        $brokenFieldMerge = false;
        foreach($fieldMerges['fieldMerge'] as $key => $fieldMerge){
            if($this->fieldMergeSyntaxes === null){
                $elq = new EloquaRequest($this->getDoctrine(),$request->getSession()->get('selectedCredential'));
                
                $i=1;
                $this->fieldMergeSyntaxes = array();
                $data = $elq->get('assets/fieldMerges?depth=partial&page='.$i);
                while (count($data->elements) > 0) {
                    foreach ($data->elements as $fm) {
                        $this->fieldMergeSyntaxes[] = $fm->syntax;
                    }
                    $i++;
                    $data = $elq->get('assets/fieldMerges?depth=partial&page='.$i);
                }
            }
            if(!in_array($fieldMerge, $this->fieldMergeSyntaxes)){
                $brokenFieldMerge = true;
            }
        }

        return $brokenFieldMerge;
    }

    /**
     * @param string $html
     * @return array
     */
    public function imageValidation($html, $rr = null){
        $imageErrors = array();
        $rrImage = false;

        preg_match_all('/(src=|background\-image\:url\(|background=)[\'\"](?P<url>(http|\/\/)?.*?)(\"|\'\))/is', $html, $images);

        foreach($images['url'] as $key => $url){
            $image = new \stdClass();
            $image->url = $url;
            $image->status = null;

            if(strpos($url, 'http://') === 0 || strpos($url, 'https://') === 0 || strpos($url, '//') === 0){
                $valid = true;
                $ext = strtolower(pathinfo($url, PATHINFO_EXTENSION));

                if(strpos($url, 'imagethumbnails') !== false){
                    $image->status = "The thumbnail URL was used, please use the full image URL.";
                }elseif(strpos($url, 'returnpath') === false && strpos($url, 'Blank_1x1.png') === false && !preg_match("/_spacer\d+x\d+\.png/",$url) && strpos($url, 'epxprwec.emltrk.com' === false)){
                    if(strpos($url, 'http://images.consumerproducts.philips.com') === 0){
                        $url = str_replace('http://images.consumerproducts.philips.com','https://secure.p01.eloqua.com',$url);
                    }else{
                        $url = str_replace('http://origin.www.images.2.forms.healthcare.philips.com','https://static.eloqua.com',$url);
                    }
                    if(strpos($url, '////')){
                        str_replace('////','//',$url);
                    }
                    $imageType = exif_imagetype($url);
                    $actualExt = "";
                    switch($imageType){
                        case 1:
                            if($ext != "gif"){
                                $valid = false;
                            }
                            $actualExt = "gif";
                        break;
                        case 2:
                            if($ext != "jpg" && $ext != "jpeg"){
                                $valid = false;
                            }
                            $actualExt = "jpg";
                        break;
                        case 3:
                            if($ext != "png"){
                                $valid = false;
                            }
                            $actualExt = "png";
                        break;
                    }                
                    if(!$valid){
                        $image->status = "The image extension doesn't match the image type (will cause issues in Internet Explorer). The file is a ".$actualExt." but was renamed to ".$ext.".";
                    }
                }
            }elseif(preg_match('/~~eloqua\.\.type--emailfield\.\.syntax--Product_Reg_IMS1\.\.innerText--Product_Reg_IMS1~~\?wid=\d+&hei=\d+/i', $url) && $rr){
                $rrImage = true;
                $image->status = "R&R dynamic image is used.";
            }elseif($url == ""){
                $image->status = "There is an image/background without any url in it. Please search for src=\"\", background-image:url('') and background=\"\". Any of these 3 could be empty.";
            }else{
                $image->status = "The image doesn't have a valid url.";
            }
            if($image->status !== null){
                $imageErrors[] = $image;
            }
        }

        return array($imageErrors, $rrImage);
    }

    /**
     * @param string $html
     * @param string $type
     * @param string $countryDomain
     * @return array
     */
    public function linkValidation($html, $type, $omnitures, $country, $countryDomain, $rr = null){
        $linkErrors = array();
        $viewOnlineError = null;
        $viewOnlineValid = false;
        $unsubError = null;
        $unsubValid = false;
        $rrCount = 0;

        preg_match_all('/<a.*?href="(?P<url>[^"]*)"[^>]*>(?P<text>.*?)<\/a>/is', $html, $links);

        foreach($links['url'] as $key => $url){
            $link = new \stdClass();
            $link->text = trim(strip_tags($links['text'][$key]));
            $link->url = $url;
            $link->status = null;
            $link->author = null;
            $link->eloquaEditors = null;
            $link->omniture = null;
            $link->tracking = null;
            $link->countryDomain = null;
            if($url == ""){
                $link->status = 'empty';
            }elseif((preg_match('/http:\/\/app\.consumerproducts\.philips\.com\/e\/es\?s=~~eloqua\.\.type--emailfield\.\.syntax--siteid\.\.encodeFor--url~~&e=~~eloqua\.\.type--emailfield\.\.syntax--elqemailsaveguid\.\.encodeFor--url~~&elqTrackId=[a-z\d]+/',$url) && $type == "b2c") || preg_match('/http:\/\/now\.eloqua\.com\/es\.asp\?s=~~eloqua\.\.type--emailfield\.\.syntax--siteid\.\.innerText--siteid\.\.encodeFor--url~~&e=~~eloqua\.\.type--emailfield\.\.syntax--elqEmailSaveGUID\.\.innerText--elqEmailSaveGUID\.\.encodeFor--url~~&elqTrackId=[a-z\d]+/',$url)){
                $viewOnlineValid = true;
                $link->status = 'valid';
            }elseif((preg_match('/http:\/\/app\.consumerproducts\.philips\.com\/e\/u\?s=~~eloqua\.\.type--emailfield\.\.syntax--siteid\.\.encodeFor--url~~/',$url) && $type == "b2c") || (preg_match('/http:\/\/origin\.www\.app\.2\.forms\.healthcare\.philips\.com\/e\/u\.aspx\?s=~~eloqua\.\.type--emailfield\.\.syntax--siteid\.\.innerText--siteid\.\.encodeFor--url~~&country=~~eloqua\.\.type--emailfield\.\.syntax--Country\.\.innerText--Country\.\.encodeFor--url~~&lang=~~eloqua\.\.type--emailfield\.\.syntax--Language1\.\.innerText--Language1\.\.encodeFor--url~~/',$url) && $type == "b2b")){
                $unsubValid = true;
                $link->status = 'valid';
            }elseif(preg_match('/https?:\/\/(www\.)?author\.philips\.com/',$url)){
                $link->author = "AEM Author page used, please use live link.";
            }elseif(preg_match('/https?:\/\/pod01\.eloquaeditors\.com/',$url)){
                $link->eloquaEditors = "Pod01 Eloqua Editors link is used";
            }elseif(preg_match('/mailto/',$url)){
                if(!preg_match('/^mailto:(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))([\?\&][^\&]+)*$/', $url)){
                    $link->status = 'mailto';
                }else{
                    $link->status = 'valid';
                }
            }elseif(preg_match('/http:\/\/app\.consumerproducts\.philips\.com\/e\/f2\.aspx\?elqFormName=ratings(one|two)&elqSiteID=1065054172&y=2019&baseURL=https:\/\/www\.philips\.co\/c-w\/bvcrmcontainer&bvaction=rr_submit_review&bvcampaignid=.*?&bvproductId=~~eloqua\.\.type--emailfield\.\.syntax--Product_Reg_CTN11\.\.innerText--Product_Reg_CTN11\.\.encodeFor--url~~.*?/i', $url, $match) && $rr){

                $link->status = 'rr'.$match[1];
                $rrCount++;
            }else{
                $omnitureRegex = ($type == 'b2c')?'/[&\?](origin=3_[a-z\-]+_[a-z\-]+_[a-z\d\-]+_[a-z\d\-]*_[a-z\d\-]+_[a-z\d\-]+_[a-z\d\-]*_[a-z\d\-]*_[a-z\d\-]*_g)/':'/[&\?](origin=3_[a-z\-]+_[a-z\-]+_[a-z\d\-]+_[a-z\d\-]*_[a-z\d\-]+_[a-z\d\-]+_[a-z\d\-]*_[a-z\d\-]*_[a-z\d\-]*)/';
                if(preg_match('/.*\.pdf/',parse_url($url, PHP_URL_PATH))){
                    $omnitureFound = preg_match($omnitureRegex, $url, $matches);
                    if($omnitureFound){
                        $link->omniture = "Omniture tracking was added to a PDF link. Please remove.";
                    }
                }elseif(preg_match('/\.philips\.[a-z]{2,4}(\.[a-z]{2,4})?/',parse_url($url, PHP_URL_HOST)) || (preg_match('/\.eloqua\.com\/e\/f2/',$url) && preg_match('/https?:\/\/(www\.)?philips\./', $url))){
                    $omnitureFound = preg_match($omnitureRegex, $url, $matches);
                    if(!$omnitureFound){
                        $link->omniture = "Omniture tracking is missing or has an incorrect format.";
                    }else{
                        $omnitures[] = $matches[1];
                        if($type == 'b2b' && $country == 'WW'){
                            $omnitureCountry = 'global';
                        }else{
                            $omnitureCountry = strtolower($country);
                        }
                        if(!preg_match('/[&\?]origin=3_'. $omnitureCountry .'/', $url)) {
                            $link->omniture = "The country parameter on omniture is incorrect.";
                        }
                    }
                    
                    //Only run next validation if a non-Eloqua Philips domain is used. Only to be used for B2C
                    if($type == "b2c" && strpos(strtolower($url), "personalhealth.philips.com") === false && strpos(strtolower($url), "app.consumerproducts.philips.com") === false && strpos(strtolower($url), "healthsystems.philips.com") === false && strpos(strtolower($url), "2.events.healthcare.philips.com") === false && strpos(strtolower($url), "2.forms.healthcare.philips.com") === false && strpos(strtolower($url), "securehealth.philips.com") === false && strpos(strtolower($url), "healthtechproducts.philips.com") === false){
                        //Check if the correct domain was used based on the country
                        if(strpos(strtolower($url), $countryDomain) === false){
                            $link->countryDomain = "The link is using a domain for a different country. Should use: <i>".$countryDomain."</i>";
                        }
                    }
                }else{
                    if(preg_match($omnitureRegex, $url)){
                        $link->omniture = "Omniture tracking has been added on a non-philips website. Please remove.";
                    }
                }
                if(!preg_match('/[&\?]elqTrack=true/i', $url)){
                    $link->tracking = "Eloqua tracking (elqTrack=true) is missing from the link.";
                }
                $link->status = self::urlValidation($url, $country);
            }
            $linkErrors[] = $link;
        }

        if(!$viewOnlineValid){
            $viewOnlineError = "The view online link is missing or incorrect.";
        }
        if(!$unsubValid){
            $unsubError = "The unsubscribe link is missing or incorrect.";
        }

        return array($linkErrors, $viewOnlineError, $unsubError, $omnitures, $rrCount);
    }

    /**
     * @param string $url
     * @return string
     */
    public function urlValidation($url, $country){
        if(strpos($url, 'pod01.eloquaeditors.com') !== false){
          return 'error';
        }
        if(strpos($url, 'mailto:') !== false){
          return 'ok';
        }
        
        $ch = curl_init($url);
        $ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36';
        curl_setopt($ch, CURLOPT_USERAGENT, $ua);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT,10);
        $output = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $redirectUrl = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
        curl_close($ch);

        $redirect = false;
        
        switch ($httpcode) {
            case "301": 
                $redirect = true;
            break;
            case "302": 
                $redirect = true;
            break;
            case "307": 
                $redirect = true;
            break;
            case "308": 
                $redirect = true;
            break;
            case "400": 
                return 'error';
            break;
            case "401": 
                return 'error';
            break;
            case "403": 
                return 'error';
            break;
            case "404": 
                return 'error';
            break;
            case "500": 
                return 'error';
            break;
            default: 
                return 'ok';
            break;
        }

        if($redirect){
            if(strpos($url,'http://') == 0 && strpos($redirectUrl,'https://') == 0 && substr($url,7) == substr($redirectUrl,8)){
                return 'httpredirect';
            }else{
                if($country == "WW" && strpos($url, 'philips.com/healthcare') !== false){
                    return 'ok';
                }else{
                    return 'redirect';
                }
            }
        }
    }
}