<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Credentials;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CredentialsController extends Controller
{
    /**
     * @Route("/credentials", name="credentials")
     */
    public function setCredentialsAction(Request $request)
    {
        $credentials = new Credentials();
        $response = '';

        $db = $this->getDoctrine()->getManager();

        $form = $this->createFormBuilder($credentials, array('attr' => array('class' => 'credentialsForm')))
            ->add('instance', TextType::class,array('label'=>'Instance Name'))
            ->add('username', TextType::class)
            ->add('password', PasswordType::class)
            ->add('save', SubmitType::class, array('label' => 'Save Credentials'))
            ->setAction($this->generateUrl('credentials'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $creds = $this->getDoctrine()->getEntityManager()->getRepository('AppBundle:Credentials')->findOneBy(array('instance' => $credentials->getInstance()));
            if(!$creds){
                $db->persist($credentials);
                $response = 'Saved credentials for '.$credentials->getInstance();
            }else{
                $creds->setUsername($credentials->getUsername());
                $creds->setPassword($credentials->getPassword());
                $response = 'Updated credentials for '.$creds->getInstance();
            }
            $db->flush();
        }

        $allCredentials = $this->getDoctrine()
            ->getRepository('AppBundle:Credentials')
            ->findAll();
            
        return $this->render('credentials/setCredentials.html.twig', array(
            'form' => $form->createView(),
            'title' => 'Set Eloqua Credentials',
            'credentials' => $allCredentials,
            'response' => $response
        ));
    }
}