<?php
namespace AppBundle\Helper;

class Email
{
	public $type = "Email";
	public $id;
	public $name;
	public $encodingId;
	public $subject;
	public $folderId;
	public $htmlContent;
	public $replyToName;
	public $replyToEmail;
	public $senderName;
	public $senderEmail;
	public $bounceBackEmail;
	public $isTracked = "true";
	public $emailGroupId;
	public $emailHeaderId;
	public $emailFooterId;
	public $virtualMTAId;

	public function getType($type)
	{
		return $this->type;
	}

	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	public function getId($id)
	{
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	public function getName($name)
	{
		return $this->name;
	}

	public function setEncodingId($encodingId)
	{
		$this->encodingId = $encodingId;

		return $this;
	}

	public function getEncodingId($encodingId)
	{
		return $this->encodingId;
	}

	public function setSubject($subject)
	{
		$this->subject = $subject;

		return $this;
	}

	public function getSubject($subject)
	{
		return $this->subject;
	}

	public function setFolderId($folderId)
	{
		$this->folderId = $folderId;

		return $this;
	}

	public function getFolderId($folderId)
	{
		return $this->folderId;
	}

	public function setHtmlContent($htmlContent)
	{
		$this->htmlContent = $htmlContent;

		return $this;
	}

	public function getHtmlContent($htmlContent)
	{
		return $this->htmlContent;
	}

	public function setReplyToName($replyToName)
	{
		$this->replyToName = $replyToName;

		return $this;
	}

	public function getReplyToName($replyToName)
	{
		return $this->replyToName;
	}

	public function setReplyToEmail($replyToEmail)
	{
		$this->replyToEmail = $replyToEmail;

		return $this;
	}

	public function getReplyToEmail($replyToEmail)
	{
		return $this->replyToEmail;
	}

	public function setSenderName($senderName)
	{
		$this->senderName = $senderName;

		return $this;
	}

	public function getSenderName($senderName)
	{
		return $this->senderName;
	}

	public function setSenderEmail($senderEmail)
	{
		$this->senderEmail = $senderEmail;

		return $this;
	}

	public function getSenderEmail($senderEmail)
	{
		return $this->senderEmail;
	}

	public function setBounceBackEmail($bounceBackEmail)
	{
		$this->bounceBackEmail = $bounceBackEmail;

		return $this;
	}

	public function getBounceBackEmail($bounceBackEmail)
	{
		return $this->bounceBackEmail;
	}

	public function setIsTracked($isTracked)
	{
		$this->isTracked = $isTracked;

		return $this;
	}

	public function getIsTracked($isTracked)
	{
		return $this->isTracked;
	}

	public function setEmailGroupId($emailGroupId)
	{
		$this->emailGroupId = $emailGroupId;

		return $this;
	}

	public function getEmailGroupId($emailGroupId)
	{
		return $this->emailGroupId;
	}

	public function setEmailHeaderId($emailHeaderId)
	{
		$this->emailHeaderId = $emailHeaderId;

		return $this;
	}

	public function getEmailHeaderId($emailHeaderId)
	{
		return $this->emailHeaderId;
	}

	public function setEmailFooterId($emailFooterId)
	{
		$this->emailFooterId = $emailFooterId;

		return $this;
	}

	public function getEmailFooterId($emailFooterId)
	{
		return $this->emailFooterId;
	}

	public function setVirtualMTAId($virtualMTAId)
	{
		$this->virtualMTAId = $virtualMTAId;

		return $this;
	}

	public function getVirtualMTAId($virtualMTAId)
	{
		return $this->virtualMTAId;
	}
}