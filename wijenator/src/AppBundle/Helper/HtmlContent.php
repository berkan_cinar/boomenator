<?php
namespace AppBundle\Helper;

class HtmlContent
{
	public $html;
	public $type = 'RawHtmlContent';
	public $contentSource = 'upload';

	public function setHtml($html)
	{
		$this->html = $html;

		return $this;
	}
}