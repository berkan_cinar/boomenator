<?php
namespace AppBundle\Helper;

class Contact  
{  
    public $accountName;  
    public $address1;  
    public $address2;  
    public $address3;  
    public $businessPhone;  
    public $city;  
    public $country;  
    public $emailAddress;  
    public $firstName;  
    public $id;  
    public $lastName;  
    public $isSubscribed = "true";  
    public $isBounceback;  
    public $salesPerson;  
	public $title;

    public function setAccountName($accountName)
    {
        $this->accountName = $accountName;

        return $this;
    }

    public function getAccountName($accountName)
    {
        return $this->accountName;
    }

    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress1($address1)
    {
        return $this->address1;
    }

    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getAddress2($address2)
    {
        return $this->address2;
    }

    public function setAddress3($address3)
    {
        $this->address3 = $address3;

        return $this;
    }

    public function getAddress3($address3)
    {
        return $this->address3;
    }

    public function setBusinessPhone($businessPhone)
    {
        $this->businessPhone = $businessPhone;

        return $this;
    }

    public function getBusinessPhone($businessPhone)
    {
        return $this->businessPhone;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function getCity($city)
    {
        return $this->city;
    }

    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    public function getCountry($country)
    {
        return $this->country;
    }

    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    public function getEmailAddress($emailAddress)
    {
        return $this->emailAddress;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getFirstName($firstName)
    {
        return $this->firstName;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId($id)
    {
        return $this->id;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getLastName($lastName)
    {
        return $this->lastName;
    }

    public function setIsSubscribed($isSubscribed)
    {
        $this->isSubscribed = $isSubscribed;

        return $this;
    }

    public function getIsSubscribed($isSubscribed)
    {
        return $this->isSubscribed;
    }

    public function setIsBounceback($isBounceback)
    {
        $this->isBounceback = $isBounceback;

        return $this;
    }

    public function getIsBounceback($isBounceback)
    {
        return $this->isBounceback;
    }

    public function setSalesPerson($salesPerson)
    {
        $this->salesPerson = $salesPerson;

        return $this;
    }

    public function getSalesPerson($salesPerson)
    {
        return $this->salesPerson;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle($title)
    {
        return $this->title;
    }
}