<?php
namespace AppBundle\Helper;

class ImageFile
{
	public $type;
	public $id;
	public $folderId;
	public $name;

	public function setType($type)
	{
		$this->type = $type;

		return $this;
	}

	public function getType($type)
	{
		return $this->type;
	}

	public function setId($id)
	{
		$this->id = $id;

		return $this;
	}

	public function getId($id)
	{
		return $this->id;
	}

	public function setFolderId($folderId)
	{
		$this->folderId = $folderId;

		return $this;
	}

	public function getFolderId($folderId)
	{
		return $this->folderId;
	}

	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	public function getName($name)
	{
		return $this->name;
	}
}