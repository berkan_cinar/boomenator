<?php
namespace AppBundle\Helper;

class ImageFolder
{
	public $type = "Folder";
	public $folderId;
	public $name;

	public function getType($type)
	{
		return $this->type;
	}

	public function setFolderId($folderId)
	{
		$this->folderId = $folderId;

		return $this;
	}

	public function getFolderId($folderId)
	{
		return $this->folderId;
	}

	public function setName($name)
	{
		$this->name = $name;

		return $this;
	}

	public function getName($name)
	{
		return $this->name;
	}
}