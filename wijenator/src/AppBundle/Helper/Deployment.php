<?php
namespace AppBundle\Helper;

class Deployment
{
	public $contactId;
	public $email;
	public $name = "Test Deployment";
	public $type = "EmailTestDeployment";
	
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    public function getContactId()
    {
        return $this->contactId;
    }
	
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }
}