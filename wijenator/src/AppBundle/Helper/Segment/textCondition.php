<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\baseRestriction;

class textCondition extends baseRestriction
{
	public $optionListId = null;
	public $quickListString = null;

	public function __construct($operator,$include,$val1=null,$val2=null)
	{
		$textOperators = array(
			"exactly" => "equal",
			"contains" => "contains",
			"starts" => "startsWith",
			"ends" => "endsWith",
			"wildcard" => "matchesWildcardPattern",
			"blank" => "blank",
			"between" => "between",
			"picklist" => "in",
			"quicklist" => "in",
			"notexactly" => "notequal",
			"notcontains" => "doesNotContain",
			"notstarts" => "doesNotStartWith",
			"notends" => "doesNotEndWith",
			"notblank" => "notBlank",
			"notbetween" => "notBetween",
			"notpicklist" => "notIn",
			"notquicklist" => "notIn"
		);
		$selector = ($include)?$operator:"not".$operator;
		$this->operator = $textOperators[$selector];
		if($operator == "blank"){
			$this->type = "TextValueCondition";

		}elseif($operator == "between"){
			$this->type = "TextRangeCondition";
			$this->start = $val1;
			$this->end = $val2;

		}elseif($operator == "picklist"){
			$this->type = "TextSetCondition";
			$this->optionListId = $val1;

		}elseif($operator == "quicklist"){
			$this->type = "TextSetCondition";
			$this->quickListString = $val1;

		}else{
			$this->type = "TextValueCondition";
			$this->value = $val1;
		}
	}

    public function getOptionListId()
    {
        return $this->optionListId;
    }

    public function getQuickListString()
    {
        return $this->quickListString;
    }
}
?>