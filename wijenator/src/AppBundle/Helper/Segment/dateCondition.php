<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\baseRestriction;

class dateCondition extends baseRestriction
{
	public function __construct($operator,$include,$val1=null,$val2=null)
	{
		$dateOperators = array(
			"before" => "less",
			"after" => "greater",
			"on or before" => "notGreater",
			"on or after" => "notLess",
			"on" => "equal",
			"dynamically equal" => "equal",
			"dynamically before" => "less",
			"dynamically after" => "greater",
			"dynamically on or before" => "notGreater",
			"dynamically on or after" => "notLess",
			"within last" => "withinLast",
			"within next" => "withinNext",
			"blank" => "blank",
			"within" => "between",
			"dynamically within" => "between",
			"noton" => "notEqual",
			"notwithin last" => "notWithinLast",
			"notwithin next" => "notWithinNext",
			"notblank" => "notBlank",
			"notwithin" => "notBetween",
			"notdynamically within" => "notBetween"
		);
		$selector = ($include)?$operator:"not".$operator;
		$this->operator = $dateOperators[$selector];

		if(substr($operator,0,11) == "dynamically" && $operator != "dynamically within"){
			$this->type = "DateValueCondition";
			$this->value = new \stdClass();
			$this->value->type = "NamedDate";
			if(substr($val1,-9) == "(no year)"){
				$this->value->isAnyYear = 'true';
				$this->value->value = substr($val1,0,-9);
			}else{
				$this->value->isAnyYear = 'false';
				$this->value->value = $val1;
			}

		}elseif($operator == "within last" || $operator == "within next"){
			$this->type = "DateValueCondition";
			$this->value = new \stdClass();
			$this->value->type = "RelativeDate";
			$this->value->offset = $val1;
			$this->value->timePeriod = $val2;

		}elseif($operator == "blank"){
			$this->type = "DateValueCondition";

		}elseif($operator == "between"){
			$this->type = "DateRangeCondition";
			$this->start = new \stdClass();
			$this->start->type = "AbsoluteDate";
			$this->start->seconds = $val1;
			$this->end = new \stdClass();
			$this->end->type = "AbsoluteDate";
			$this->end->seconds = $val2;

		}elseif($operator == "dynamically within"){
			$this->type = "DateRangeCondition";
			$this->start = new \stdClass();
			$this->start->type = "NamedDate";
			if(substr($val1,-9) == "(no year)"){
				$this->start->isAnyYear = 'true';
				$this->start->value = substr($val1,0,-9);
			}else{
				$this->start->isAnyYear = 'false';
				$this->start->value = $val1;
			}
			$this->end = new \stdClass();
			$this->end->type = "NamedDate";
			if(substr($val2,-9) == "(no year)"){
				$this->end->isAnyYear = 'true';
				$this->end->value = substr($val2,0,-9);
			}else{
				$this->end->isAnyYear = 'false';
				$this->end->value = $val2;
			}
			
		}else{
			$this->type = "DateValueCondition";
			$this->value = new \stdClass();
			$this->value->type = "AbsoluteDate";
			$this->value->seconds = $val1;
		}
	}
}
?>