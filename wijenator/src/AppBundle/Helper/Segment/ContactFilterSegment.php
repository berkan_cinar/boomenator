<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\ContactFilter;

class ContactFilterSegment //filter parent
{
	public $type = "ContactFilterSegmentElement";
	public $isIncluded = 'true'; //defaults to true (include), set to false to exclude a filter.
	public $filter;

	public function __construct($name,$criteria,$statement,$isIncluded=null)
	{
		$this->filter = new ContactFilter($name,$criteria,$statement);
		if($isIncluded !== null){
			$this->isIncluded = $isIncluded;
		}
	}

    public function getType()
    {
        return $this->type;
    }

    public function setIsIncluded($isIncluded)
    {
        $this->isIncluded = $isIncluded;

        return $this;
    }

    public function getIsIncluded()
    {
        return $this->isIncluded;
    }
	
    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }

    public function getFilter()
    {
        return $this->filter;
    }
}
?>