<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\Segment;

class Criterion
{
    public $id;
    public $type;
    public $timeRestriction = null;
    public $activityRestriction = null;

    public function __construct()
	{
		$this->id = Segment::getTempId();
	}

    public function setTimeRelative($operator,$diff,$period)
    {
        $this->timeRestriction = new timeRestriction($operator,$diff,$period);
    }

    public function setTimeAbsolute($operator,$date1,$date2=null)
    {
        $this->timeRestriction = new timeRestriction($operator,$date1,$date2);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTimeRestriction()
    {
        return $this->timeRestriction;
    }

    public function getActivityRestriction()
    {
        return $this->activityRestriction;
    }
}
?>