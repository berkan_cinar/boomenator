<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\baseRestriction;

class numberCondition extends baseRestriction
{
	public $optionListId = null;
	public $quickListString = null;

	public function __construct($operator,$include,$val1=null,$val2=null)
	{
		$textOperators = array(
			"exactly" => "equal",
			"greater than" => "greater",
			"less than" => "less",
			"at least" => "notLess",
			"at most" => "notGreater",
			"blank" => "blank",
			"between" => "between",
			"notexactly" => "notequal",
			"notblank" => "notBlank",
			"notbetween" => "notBetween",
		);
		$selector = ($include)?$operator:"not".$operator;
		$this->operator = $textOperators[$selector];
		if($operator == "blank"){
			$this->type = "NumberValueCondition";

		}elseif($operator == "between"){
			$this->type = "NumberRangeCondition";
			$this->start = $val1;
			$this->end = $val2;

		}else{
			$this->type = "NumberValueCondition";
			$this->value = $val1;
		}
	}

    public function getOptionListId()
    {
        return $this->optionListId;
    }

    public function getQuickListString()
    {
        return $this->quickListString;
    }
}
?>