<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\fieldCondition;

class cdoFieldCondition  extends fieldCondition //CDO field set
{
	public function __construct($fieldId)
	{
		parent::__construct($fieldId);
		$this->type = "FieldCondition";
	}
}
?>