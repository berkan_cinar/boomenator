<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\Criterion;

class LinkedCustomObjectCriterion extends Criterion//Any linked CDO criteria
{
	public $customObjectId;
	public $fieldConditions; //takes array of fieldconditions

	public function __construct($customObjectId,$fieldConditions)
	{
		parent::__construct();
		$this->type = "LinkedCustomObjectCriterion";
		$this->customObjectId = $customObjectId;
		$this->fieldConditions = $fieldConditions;
	}

    public function getCustomObjectId()
    {
        return $this->customObjectId;
    }

    public function getFieldConditions()
    {
        return $this->fieldConditions;
    }
}
?>