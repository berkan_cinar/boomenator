<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\Criterion;

class SubscriptionCriterion extends Criterion
{
	public $emailGroupIds; //array of email groups
	
	public function __construct()
	{
		parent::__construct();
		$this->type = "SubscriptionCriterion";
	}	
	
	public function setEmailGroups(array $emailgroups)
	{
		$this->emailGroupIds = $emailgroups;
	}
}
?>