<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\baseRestriction;

class timeRestriction extends baseRestriction
{
	public function __construct($operator,$val1,$val2=null)
	{
		$operators = array(
			"on" => "equal",
			"before" => "less",
			"after" => "greater",
			"within" => "between",
			"within last" => "withinLast",
			"not within last" => "notWithinLast"
		);
		$this->operator = $operators[$operator];
		if($operator == "within last" || $operator == "not within last"){
			$this->type = "DateValueCondition";
			$this->value = new \stdClass();
			$this->value->type = "RelativeDate";
			$this->value->offset = $val1;
			$this->value->timePeriod = $val2;

		}elseif($operator == "within"){
			$this->type = "DateRangeCondition";
			$this->start = new \stdClass();
			$this->start->type = "AbsoluteDate";
			$this->start->seconds = $val1;
			$this->end = new \stdClass();
			$this->end->type = "AbsoluteDate";
			$this->end->seconds = $val2;

		}else{
			$this->type = "DateValueCondition";
			$this->value = new \stdClass();
			$this->value->type = "AbsoluteDate";
			$this->value->seconds = $val1;
		}
	}
}
?>