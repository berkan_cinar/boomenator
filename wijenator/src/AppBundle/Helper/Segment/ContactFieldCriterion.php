<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\fieldCondition;

class ContactFieldCriterion extends fieldCondition //Any contactfield criteria
{
	public function __construct($fieldId)
	{
		parent::__construct($fieldId);
		$this->type = "ContactFieldCriterion";
	}
}
?>