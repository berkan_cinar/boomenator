<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\Criterion;

class UnsubscriptionCriterion extends Criterion //Globally Unsubscribed criteria
{
	public function __construct()
	{
		parent::__construct();
		$this->type = "UnsubscriptionCriterion";
	}	
	//If no time restriction is set, eloqua validates it as "As of now";
}
?>