<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\Criterion;
use AppBundle\Helper\Segment\textCondition;
use AppBundle\Helper\Segment\numberCondition;
use AppBundle\Helper\Segment\dateCondition;

class fieldCondition extends Criterion
{
	public $condition;
	public $fieldId;

	public function __construct($fieldId)
	{
		parent::__construct();
		$this->fieldId = $fieldId;
	}

	public function setTextCondition($operator,$include,$val1,$val2=null)
	{
		$this->condition = new textCondition($operator,$include,$val1,$val2);
	}

	public function setBlankText($include)
	{
		$this->condition = new textCondition('blank', $include);
	}

	public function setNumberCondition($operator,$include,$val1,$val2=null)
	{
		$this->condition = new numberCondition($operator,$include,$val1,$val2);
	}

	public function setBlankNumber($include)
	{
		$this->condition = new numberCondition('blank', $include);
	}

	public function setRelativeDateCondition($operator,$include,$diff,$period)
	{
		$this->condition = new dateCondition($operator,$include,$diff,$period);
	}

	public function setAbsoluteDateCondition($operator,$include,$date1,$date2=null)
	{
		$this->condition = new dateCondition($operator,$include,$date1,$date2);
	}

	public function setBlankDate($include)
	{
		$this->condition = new dateCondition('blank', $include);
	}

    public function getCondition()
    {
        return $this->condition;
    }

    public function getFieldId()
    {
        return $this->fieldId;
    }
}
?>