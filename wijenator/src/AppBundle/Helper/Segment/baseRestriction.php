<?php
namespace AppBundle\Helper\Segment;

class baseRestriction
{
	public $type;
	public $operator = null;
	public $value = null;
	public $start = null;
	public $end = null;
    
    public function getType()
    {
        return $this->type;
    }

    public function getOperator()
    {
        return $this->operator;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getEnd()
    {
        return $this->end;
    }
}
?>