<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\Criterion;

class HardBouncebackCriterion extends Criterion //Hard Bounceback criteria
{
	public $hasStatus = 'false'; //set to true if you need to add all hardbounced consumers for exclusion for example

	public function __construct($hasStatus=null)
	{
		parent::__construct();
		$this->type = "HardBouncebackCriterion";
		if($hasStatus !== null){
			$this->hasStatus = $hasStatus;
		}
	}	

    public function getHasStatus()
    {
        return $this->hasStatus;
    }
	//If no time restriction is set, eloqua validates it as "As of now";
}
?>