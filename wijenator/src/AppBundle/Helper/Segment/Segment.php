<?php
namespace AppBundle\Helper\Segment;

class Segment //full segment
{
	public $type = "ContactSegment";
	public $id = null;
	public $name;
	public $folderId;
	public $elements;
	private static $tempId = -500100;

	public function __construct($name,$folderId,$elements)
	{
		$this->name = $name;
		$this->folderId = $folderId;
		$this->elements = $elements;
	}

    public function getType()
    {
        return $this->type;
    }
	
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getFolderId()
    {
        return $this->folderid;
    }

    public function getElements()
    {
        return $this->elements;
    }
    
	public static function getTempId()
	{
		return --self::$tempId;
	}
}
?>