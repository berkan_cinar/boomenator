<?php
namespace AppBundle\Helper\Segment;

class ContactFilter //filter with criteria
{
	public $type = "ContactFilter";
	public $name;
	public $criteria;
	public $statement;

	public function __construct($name,$criteria,$statement)
	{
		$this->name = $name;
		$this->criteria = $criteria;
		$this->statement = $statement;
	}

    public function getType()
    {
        return $this->type;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getStatement()
    {
        return $this->statement;
    }
}
?>