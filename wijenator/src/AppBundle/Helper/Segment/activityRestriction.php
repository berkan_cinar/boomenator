<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\baseRestriction;

class activityRestriction extends baseRestriction
{
	public function __construct($operator,$val1=null,$val2=null)
	{
		$operators = array(
			"exactly" => "equal",
			"between" => "between",
			"at least" => "notless"
		);
		if($operator != "noactivity"){
			$this->type = "NumericValueCondition";
			$this->operator = $operators[$operator];
			if($operator != "between"){
				$this->value = $val1;
			}else{
				$this->start = $val1;
				$this->end = $val2;
			}
		}else{
			$this->type = "ZeroCondition";
		}
	}
}
?>