<?php
namespace AppBundle\Helper\Segment;

use AppBundle\Helper\Segment\Criterion;
use AppBundle\Helper\Segment\activityRestriction;

class ActivityCriterion extends Criterion //any activity Criteria
{
	public $VisitedWebsiteType = null;
	public $emailIds = null;
	public $campaignIds = null;
	public $formIds = null;
	public $trackedUrlIds = null;

	public function __construct($type)
	{
		parent::__construct();
		$availableTypes = array(
			"sent" => "EmailSentCriterion",
			"open" => "EmailOpenCriterion",
			"click" => "EmailClickThroughCriterion",
			"landingpage" => "VisitedLandingPageCriterion",
			"form" => "FormSubmitCriterion",
			"website" => "VisitedWebsiteCriterion"
		);
		$this->type = $availableTypes[$type];
		if($type == "website"){
			$this->VisitedWebsiteType = "VisitedPage";
			$this->trackedUrlIds = array();
		}
	}

	public function setActivity($operator,$val1,$val2=null)
	{
		$this->activityRestriction = new activityRestriction($operator,$val1,$val2=null);
	}
	public function setNonActivity()
	{
		$this->activityRestriction = new activityRestriction('noactivity');
	}

    public function getVisitedWebsiteType()
    {
        return $this->visitedWebsiteType;
    }
	
    public function setEmailIds($emailIds)
    {
        $this->emailIds = $emailIds;

        return $this;
    }

    public function getEmailIds()
    {
        return $this->emailIds;
    }
	
    public function setCampaignIds($campaignIds)
    {
        $this->campaignIds = $campaignIds;

        return $this;
    }

    public function getCampaignIds()
    {
        return $this->campaignIds;
    }
	
    public function setFormIds($formIds)
    {
        $this->formIds = $formIds;

        return $this;
    }

    public function getFormIds()
    {
        return $this->formIds;
    }
	
    public function setTrackedUrlIds($trackedUrlIds)
    {
        $this->trackedUrlIds = $trackedUrlIds;

        return $this;
    }

    public function getTrackedUrlIds()
    {
        return $this->trackedUrlIds;
    }
}
?>