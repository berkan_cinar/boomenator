<?php
namespace AppBundle\Helper;

class b2bFilter
{
	public $id;
	public $type;
	public $name;
    public $status = "notfound";
    public $filterErrors = array();
	
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }
	
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setErrors($filterErrors)
    {
        $this->filterErrors[] = $filterErrors;

        return $this;
    }

    public function getErrors()
    {
        return $this->filterErrors;
    }
}