<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="upload_settings")
 */
class UploadSettings
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $emailGroupId;

    /**
     * @ORM\Column(type="integer")
     */
    private $emailHeaderId;

    /**
     * @ORM\Column(type="integer")
     */
    private $emailFooterId;

    /**
     * @ORM\Column(type="integer")
     */
    private $imageFolder;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return UploadSettings
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set emailGroupId
     *
     * @param integer $emailGroupId
     *
     * @return UploadSettings
     */
    public function setEmailGroupId($emailGroupId)
    {
        $this->emailGroupId = $emailGroupId;

        return $this;
    }

    /**
     * Get emailGroupId
     *
     * @return integer
     */
    public function getEmailGroupId()
    {
        return $this->emailGroupId;
    }

    /**
     * Set emailHeaderId
     *
     * @param integer $emailHeaderId
     *
     * @return UploadSettings
     */
    public function setEmailHeaderId($emailHeaderId)
    {
        $this->emailHeaderId = $emailHeaderId;

        return $this;
    }

    /**
     * Get emailHeaderId
     *
     * @return integer
     */
    public function getEmailHeaderId()
    {
        return $this->emailHeaderId;
    }

    /**
     * Set emailFooterId
     *
     * @param integer $emailFooterId
     *
     * @return UploadSettings
     */
    public function setEmailFooterId($emailFooterId)
    {
        $this->emailFooterId = $emailFooterId;

        return $this;
    }

    /**
     * Get emailFooterId
     *
     * @return integer
     */
    public function getEmailFooterId()
    {
        return $this->emailFooterId;
    }

    /**
     * Set imageFolder
     *
     * @param integer $imageFolder
     *
     * @return UploadSettings
     */
    public function setImageFolder($imageFolder)
    {
        $this->imageFolder = $imageFolder;

        return $this;
    }

    /**
     * Get imageFolder
     *
     * @return integer
     */
    public function getImageFolder()
    {
        return $this->imageFolder;
    }
}
