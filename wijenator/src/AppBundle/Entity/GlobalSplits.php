<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="global_splits")
 */
class GlobalSplits
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=5)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", length=7)
     */
    private $filterId;

    /**
     * @ORM\Column(type="text")
     */
    private $statement;

    /**
     * @ORM\Column(type="text")
     */
    private $wildcards;

    /**
     * @ORM\Column(type="text")
     */
    private $doesNotEndWith;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return UploadSettings
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return UploadSettings
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filterId
     *
     * @param string $filterId
     *
     * @return UploadSettings
     */
    public function setFilterId($filterId)
    {
        $this->filterId = $filterId;

        return $this;
    }

    /**
     * Get filterId
     *
     * @return string
     */
    public function getFilterId()
    {
        return $this->filterId;
    }

    /**
     * Set statement
     *
     * @param string $statement
     *
     * @return UploadSettings
     */
    public function setStatement($statement)
    {
        $this->statement = $statement;

        return $this;
    }

    /**
     * Get statement
     *
     * @return string
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * Set wildcards
     *
     * @param string $wildcards
     *
     * @return UploadSettings
     */
    public function setWildcards($wildcards)
    {
        $this->wildcards = $wildcards;

        return $this;
    }

    /**
     * Get wildcards
     *
     * @return string
     */
    public function getWildcards()
    {
        return $this->wildcards;
    }

    /**
     * Set doesNotEndWith
     *
     * @param string $doesNotEndWith
     *
     * @return UploadSettings
     */
    public function setDoesNotEndWith($doesNotEndWith)
    {
        $this->doesNotEndWith = $doesNotEndWith;

        return $this;
    }

    /**
     * Get doesNotEndWith
     *
     * @return string
     */
    public function getDoesNotEndWith()
    {
        return $this->doesNotEndWith;
    }
}
