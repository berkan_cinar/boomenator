<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="region_mapping")
 */
class RegionMapping
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=3)
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity="Timezones")
     * @ORM\JoinTable(name="timezone_mapping",
     *      joinColumns={@ORM\JoinColumn(name="region_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="timezone_id", referencedColumnName="id")}
     *      )
     */
    private $timezone;

    /**
     * @ORM\ManyToOne(targetEntity="RegionSettings")
     * @ORM\JoinColumn(name="region", referencedColumnName="id")
     */
    private $region;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return MenuLinks
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return MenuLinks
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

     /**
     * Add timezone
     *
     * @param \AppBundle\Entity\Timezones $timezone
     *
     * @return ExportSettings
     */
    public function addTimezone(\AppBundle\Entity\Timezones $timezone)
    {
        $this->timezone[] = $timezone;

        return $this;
    }

    /**
     * Remove timezone
     *
     * @param \AppBundle\Entity\Timezones $timezone
     */
    public function removeTimezone(\AppBundle\Entity\Timezones $timezone)
    {
        $this->timezone->removeElement($timezone);
    }

    /**
     * Get timezone ids
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimezoneIds()
    {
        $ids = array();
        foreach($this->timezone as $t){
            $ids[] = $t->getId();
        }
        return $ids;
    }  

    /**
     * Get timezone names
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimezoneNames()
    {
        $names = array();
        foreach($this->timezone as $t){
            $names[] = $t->getName();
        }
        return $names;
    } 

    /**
     * Set region
     *
     * @param \AppBundle\Entity\RegionSettings $region
     *
     * @return RegionMapping
     */
    public function setRegion(\AppBundle\Entity\RegionSettings $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \AppBundle\Entity\RegionSettings
     */
    public function getRegion()
    {
        return $this->region;
    } 
}