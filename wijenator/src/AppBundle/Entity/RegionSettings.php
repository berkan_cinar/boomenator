<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="region_settings")
 */
class RegionSettings
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $replyToName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $replyToEmail;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $senderName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $senderEmail;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $bounceBackEmail;

    /**
     * @ORM\Column(type="integer", length=5)
     */
    private $virtualMtaId;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set region
     *
     * @param string $region
     *
     * @return RegionSettings
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set replyToName
     *
     * @param string $replyToName
     *
     * @return RegionSettings
     */
    public function setReplyToName($replyToName)
    {
        $this->replyToName = $replyToName;

        return $this;
    }

    /**
     * Get replyToName
     *
     * @return string
     */
    public function getReplyToName()
    {
        return $this->replyToName;
    }

    /**
     * Set replyToEmail
     *
     * @param string $replyToEmail
     *
     * @return RegionSettings
     */
    public function setReplyToEmail($replyToEmail)
    {
        $this->replyToEmail = $replyToEmail;

        return $this;
    }

    /**
     * Get replyToEmail
     *
     * @return string
     */
    public function getReplyToEmail()
    {
        return $this->replyToEmail;
    }

    /**
     * Set senderName
     *
     * @param string $senderName
     *
     * @return RegionSettings
     */
    public function setSenderName($senderName)
    {
        $this->senderName = $senderName;

        return $this;
    }

    /**
     * Get senderName
     *
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * Set senderEmail
     *
     * @param string $senderEmail
     *
     * @return RegionSettings
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;

        return $this;
    }

    /**
     * Get senderEmail
     *
     * @return string
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * Set bounceBackEmail
     *
     * @param string $bounceBackEmail
     *
     * @return RegionSettings
     */
    public function setBounceBackEmail($bounceBackEmail)
    {
        $this->bounceBackEmail = $bounceBackEmail;

        return $this;
    }

    /**
     * Get bounceBackEmail
     *
     * @return string
     */
    public function getBounceBackEmail()
    {
        return $this->bounceBackEmail;
    }

    /**
     * Set virtualMtaId
     *
     * @param string $virtualMtaId
     *
     * @return RegionSettings
     */
    public function setVirtualMtaId($virtualMtaId)
    {
        $this->virtualMtaId = $virtualMtaId;

        return $this;
    }

    /**
     * Get virtualMtaId
     *
     * @return string
     */
    public function getVirtualMtaId()
    {
        return $this->virtualMtaId;
    }
}