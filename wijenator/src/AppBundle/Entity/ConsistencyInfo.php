<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="consistency_info")
 */
class ConsistencyInfo
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $sharedList;

    /**
     * @ORM\Column(type="integer", length=10)
     */
    private $program;

    /**
     * @ORM\Column(type="boolean")
     */
    private $adhoc;

    /**
     * @ORM\Column(type="boolean")
     */
    private $lcp;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Country
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set sharedList
     *
     * @param string $sharedList
     *
     * @return SharedList
     */
    public function setSharedList($sharedList)
    {
        $this->sharedList = $sharedList;

        return $this;
    }

    /**
     * Get sharedList
     *
     * @return string
     */
    public function getSharedList()
    {
        return $this->sharedList;
    }

    /**
     * Set program
     *
     * @param string $program
     *
     * @return Program
     */
    public function setProgram($program)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return string
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set adhoc
     *
     * @param string $adhoc
     *
     * @return adhoc
     */
    public function setAdhoc($adhoc)
    {
        $this->adhoc = $adhoc;

        return $this;
    }

    /**
     * Get adhoc
     *
     * @return string
     */
    public function getAdhoc()
    {
        return $this->adhoc;
    }

    /**
     * Set lcp
     *
     * @param string $lcp
     *
     * @return lcp
     */
    public function setLcp($lcp)
    {
        $this->lcp = $lcp;

        return $this;
    }

    /**
     * Get lcp
     *
     * @return string
     */
    public function getLcp()
    {
        return $this->lcp;
    }
}
