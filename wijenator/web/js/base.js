/* Form handling */
$('.credForm').change(function(){
	$('.credForm').closest('form').submit();
});

$('.menuCredentialsForm').submit(function(e){
	e.preventDefault();

	$.ajax({
	    type        : 'POST',
	    url         : $('.menuCredentialsForm').attr( 'action' ),
	    data        : $('.menuCredentialsForm').serialize()
	});
});

$('.emailProofForm').submit(function(e){
	e.preventDefault();

	$('.emailStatusIcons').children('span').hide();

	$("input[name='proofEmailSelect[emails][]']:checked").each(function(key, obj){
		var emailID = $(obj).val();
		var emailAddress = $("input[name='proofEmailSelect[emailaddress]']").val();

		var data = {emailID: emailID,
					emailaddress: emailAddress}

		var statusIcon = $('#email'+emailID);
		$(statusIcon).children('span').hide();
		$(statusIcon).children('.loading').show();

		$.ajax({
		    type        : 'POST',
		    url         : $('.emailProofForm').attr( 'action' ),
		    data        : data
		}).done(function(response){
			$(statusIcon).children('.loading').hide();
			if(response == "SUCCESS"){
				$(statusIcon).children('.success').show();
			}else{
				$(statusIcon).children('.failed').attr('title',response).show();
			}
		})
	})
});

$('.dccSegmentForm').submit(function(e) {
	e.preventDefault();
	$('#segmentResult').html('');
	if($('#dccSegmentForm_folder').val() !== null && $('#dccSegmentForm_name').val() != ''){
		$('#dccSegmentForm_submit').prop('disabled', true);
		$('#segmentLoader').removeAttr('style');
		var formData = new FormData();
	    formData.append("folder",$("#dccSegmentForm_folder").val());
	    formData.append("name",$("#dccSegmentForm_name").val());
	    formData.append("campaignType",$("#dccSegmentForm_campaignType").val());
		$.ajax({
			type: "POST",
			data: formData,
			url: $('.dccSegmentForm').attr( 'action' ),
		    cache: false,
		    contentType: false,
		    processData: false
		}).done(function(response) {			
			var results = $.parseJSON(response);
			if(results.id !== undefined){
				$('#segmentResult').html('Segment created with ID: '+results.id+'. <span style="color:red;">Please check in Eloqua if the segment is correct, and if so, start the calculation by pressing the refresh segment button.</span>');
			}else{
				$('#segmentResult').html('<span style="color:red;">There was an error with the creation of the Segment. This could be a temporary system error.</span>');
			}
			$('#dccSegmentForm_submit').prop('disabled', false);
		   	$('#segmentLoader').css('display','none');
		});
	}else{
		$('#segmentResult').html('<span style="color:red;">Please select a folder and set a name for the segment.</span>');
	}
});

$(".dccEmailForm").submit(function(e){
	e.preventDefault();
	$('#emailResponse').html('');

	var validateEmailNames = true;
	$('.emailNameDcc').each(function(i){
		if($(this).val() == ''){
			validateEmailNames = false;
		}
	});

	var emailsFinished = 0;
	var totalEmails = $('.emailNameDcc').length;
	$('#dccEmailForm_submit').prop('disabled', true);
	$('#emailLoader').removeAttr('style');

	if(validateEmailNames && $('#dccEmailForm_folder').val() !== null){
		$('#emailResult').html('');
		var namesArray = [];
		var names = {};
		var emailCount = $('.emailNameDcc').length;
		if(emailCount < 10){
			$('.emailNameDcc').each(function(i){
				names[$(this).attr("name").replace("dccEmailForm[", "").replace("]", "")] = $(this).val();
			});
			namesArray[0] = names;
		}else{
			var j = 4;
			var k = 0;
			$('.emailNameDcc').each(function(i){
				names[$(this).attr("name").replace("dccEmailForm[", "").replace("]", "")] = $(this).val();
				if(i == j){
					namesArray[k] = names;
					names = {};
					if((j+5) < emailCount){
						j = j+5;
					}else{
						j = emailCount-1;
					}
					k++;
				}
			});
		}

		if(emailCount < 10){
			//less than 10 emails, send in 1 batch
		    var formData = new FormData();
		    formData.append("folder",$("#dccEmailForm_folder").val());
		    formData.append("fileNames",$("#dccEmailForm_fileNames").val());
		    formData.append("names",JSON.stringify(namesArray[0]));
		    formData.append("country",$("#dccEmailForm_country").val());
		    formData.append("campaignType",$("#dccEmailForm_campaignType").val());
		    formData.append("campaignName",$("#dccEmailForm_campaignName").val());
		    formData.append("bgcat",$("#dccEmailForm_bgcat").val());

		    $.ajax({
		        url: $(".dccEmailForm").attr( 'action' ),
		        type: 'POST',
		        data: formData,
			    cache: false,
			    contentType: false,
			    processData: false
			}).done(function (response) {
	    		$('#dccEmailForm_submit').prop('disabled', false);
				$('#emailLoader').css('display','none');
	            var results = JSON.parse(response);
	            $.each(results, function(key, result){
		            var message = '"'+result['emailName']+'" has been created with ID: <strong>'+result['emailId']+'</strong>. <a href="https://secure.p01.eloqua.com/Main.aspx?culture=en#emails&id='+result['emailId']+'" target="_blank">Open email in Eloqua</a><br /><br/>';
		            $('#emailResult').append(message);
		        });
		    });	
		}else{
			//10 or more emails - break into batches
		    var i = 0;

		    function complete(){
		    	i++;
		    	if(i >= namesArray.length){
		    		$('#dccEmailForm_submit').prop('disabled', false);
					$('#emailLoader').css('display','none');
		    	}else{
		    		sendRequest(namesArray[i], complete);
		    	}
		    }

		    sendRequest(namesArray[i], complete);
		}
	}else{
		$('#emailResult').html('<span style="color:red;">Please select the folder and enter all email names.</span>');
	}
});

function sendRequest(nameObject, complete){
	var formData = new FormData();
    formData.append("folder",$("#dccEmailForm_folder").val());
    formData.append("fileNames",$("#dccEmailForm_fileNames").val());
    formData.append("names",JSON.stringify(nameObject));
    formData.append("country",$("#dccEmailForm_country").val());
    formData.append("campaignType",$("#dccEmailForm_campaignType").val());
    formData.append("campaignName",$("#dccEmailForm_campaignName").val());
    formData.append("bgcat",$("#dccEmailForm_bgcat").val());

    $.ajax({
        url: $(".dccEmailForm").attr( 'action' ),
        type: 'POST',
        data: formData,
	    cache: false,
	    contentType: false,
	    processData: false,
	    complete: complete
	}).done(function (response) {
        var results = JSON.parse(response);
        $.each(results, function(key, result){
            var message = '"'+result['emailName']+'" has been created with ID: <strong>'+result['emailId']+'</strong>. <a href="https://secure.p01.eloqua.com/Main.aspx?culture=en#emails&id='+result['emailId']+'" target="_blank">Open email in Eloqua</a><br /><br/>';
            $('#emailResult').append(message);
        });
    });
}

$('#generateImage').click(function(e) {
	$('.sf-toolbar, #menubar, .hideScreenshot').hide();
	html2canvas(document.body).then(function(canvas) {
		canvas.toBlob(function(blob) {
		    saveAs(blob, dateFormat(Date(),"yyyy-mm-dd") + " - " + $('#campaignSearch_query').val() + ".png");
		});
		$('.sf-toolbar, #menubar, .hideScreenshot').show();
  	});
});